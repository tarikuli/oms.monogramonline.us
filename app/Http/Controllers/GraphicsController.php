<?php

namespace App\Http\Controllers;

use App\Batch;
use App\BatchRoute;
use App\Customer;
use App\Design;
use App\Item;
use App\Printer;
use App\Rejection;
use App\Section;
use App\Station;
use App\Store;
use App\WasatchDashboard;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Monogram\Helper;
use Monogram\Batching;
use Monogram\FileHelper;
use Monogram\ImageHelper;
use Monogram\SFTPConnection;
use Monogram\Sure3d;
use Monogram\Wasatch;
use Zebra\Zpl\Builder;
use Zebra\Zpl\GdDecoder;
use Zebra\Zpl\Image;


class GraphicsController extends Controller
{
    protected $wasatchDashboard;

    protected $main_dir = '/media/graphics/MAIN/';
    protected $sort_root = '/var/www/oms/public_html/media/graphics/';
    protected $download_dir = '/media/graphics/Sure3d/';

    protected $csv_dir = '/media/Ji-share/5p_batch_csv_export/';
    protected $error_dir = '/media/Ji-share/5p_batch_csv_export/Jobs_Error/';
    protected $finished_dir = '/media/Ji-share/5p_batch_csv_export/Jobs_Finished/';
    public static $manual_dir = '/media/Ji-share/5p_batch_csv_export/MANUAL/';

// protected $sub_dir = '/media/graphics/sublimation/';
//    public static $archive = '/media/graphics/archive/';

//    protected $printers = ['EPSON-1' => 'EPSON-1', 'EPSON-2' => 'EPSON-2','SOFT-1' => 'SOFT-1', 'SOFT-2' => 'SOFT-2', 'SOFT-3' => 'SOFT-3', 'HARD-1' => 'HARD-1', 'HARD-2' => 'HARD-2', 'HARD-3' => 'HARD-3'];
    protected $printers = ['EPSON-1' => 'EPSON-1', 'EPSON-2' => 'EPSON-2', 'EPSON-3' => 'EPSON-3', 'EPSON-4' => 'EPSON-4', 'EPSON-5' => 'EPSON-5', 'EPSON-6' => 'EPSON-6', 'EPSON-7' => 'EPSON-7', 'EPSON-8' => 'EPSON-8', 'EPSON-9' => 'EPSON-9'];

    /**
     * @return array
     */
    public function getPrinters()
    {
        return $this->printers;
    }
    public function __construct()
    {
        $this->wasatchDashboard = new WasatchDashboard;
        if (env("GRAPHICS_ENV") == "devgraphics") {
            $this->main_dir = '/media/devgraphics/MAIN/';
            $this->sort_root = '/var/www/devoms/public_html/media/graphics/';
            $this->download_dir = '/media/devgraphics/Sure3d/';
            $this->csv_dir = '/media/devJi-share/5p_batch_csv_export/';
            $this->error_dir = '/media/devJi-share/5p_batch_csv_export/Jobs_Error/';
            $this->finished_dir = '/media/devJi-share/5p_batch_csv_export/Jobs_Finished/';
        }
    }


    public function index(Request $request)
    {
        if (!file_exists($this->csv_dir)) {
            return redirect()->back()->withErrors('Cannot find csv directory on M: drive');
        }

        if (!file_exists($this->sort_root)) {
            return redirect()->back()->withErrors('Cannot find graphics directory on P: drive');
        }

        ini_set('memory_limit', '256M');

        $request->has('tab') ? $tab = $request->get('tab') : $tab = 'summary';

        if ($tab == 'summary') {
            $dates = array();
            $date[] = date("Y-m-d");
            $date[] = date("Y-m-d", strtotime('-3 days'));
            $date[] = date("Y-m-d", strtotime('-4 days'));
            $date[] = date("Y-m-d", strtotime('-7 days'));
            $date[] = date("Y-m-d", strtotime('-8 days'));

            $items = Item::join('batches', 'batches.batch_number', '=', 'items.batch_number')
                ->join('orders', 'items.order_5p', '=', 'orders.id')
                ->join('stations', 'batches.station_id', '=', 'stations.id')
                ->join('sections', 'stations.section', '=', 'sections.id')
                ->where('batches.status', 2)
                ->where('items.item_status', 1)
                ->where('stations.type', 'G')
//->where('orders.order_status', 4)
                ->groupBy('stations.station_name')
//->groupBy ( 'orders.order_status' )
                ->orderBy('sections.section_name')
                ->orderBy('stations.station_description', 'ASC')
                ->selectRaw("
SUM(items.item_quantity) as items_count, 
count(items.id) as lines_count, 
stations.station_name,
stations.station_description,
stations.type,
batches.station_id,
stations.section as section_id,
sections.section_name,
DATE(MIN(orders.order_date)) as earliest_order_date,
DATE(MIN(batches.change_date)) as earliest_scan_date,
COUNT(IF(orders.order_date >= '{$date[1]} 00:00:00', items.id, NULL)) as order_1,
COUNT(IF(orders.order_date >= '{$date[3]} 00:00:00' AND orders.order_date <= '{$date[2]} 23:59:59', items.id, NULL)) as order_2,
COUNT(IF(orders.order_date <= '{$date[4]} 23:59:59', items.id, NULL)) as order_3,
COUNT(IF(batches.change_date >= '{$date[1]} 00:00:00', items.id, NULL)) as scan_1,
COUNT(IF(batches.change_date >= '{$date[3]} 00:00:00' AND batches.change_date <= '{$date[2]} 23:59:59', items.id, NULL)) as scan_2,
COUNT(IF(batches.change_date <= '{$date[4]} 23:59:59', items.id, NULL)) as scan_3
")
                ->get();

            $rejects = Item::join('rejections', 'items.id', '=', 'rejections.item_id')
                ->join('orders', 'items.order_5p', '=', 'orders.id')
                ->join('batches', 'items.batch_number', '=', 'batches.batch_number')
                ->join('sections', 'batches.section_id', '=', 'sections.id')
                ->where('items.is_deleted', '0')
                ->where('rejections.complete', '0')
                ->whereNotIn('rejections.graphic_status', [4, 5])// exclude CS rejects
                ->searchStatus('rejected')
                ->groupBy('batches.section_id', 'rejections.graphic_status')
                ->selectRaw("
SUM(items.item_quantity) as items_count, 
count(items.id) as lines_count, 
rejections.graphic_status,
batches.section_id,
sections.section_name,
DATE(MIN(orders.order_date)) as earliest_order_date,
COUNT(IF(orders.order_date >= '{$date[1]} 00:00:00', items.id, NULL)) as order_1,
COUNT(IF(orders.order_date >= '{$date[3]} 00:00:00' AND orders.order_date <= '{$date[2]} 23:59:59', items.id, NULL)) as order_2,
COUNT(IF(orders.order_date <= '{$date[4]} 23:59:59', items.id, NULL)) as order_3,
COUNT(IF(batches.change_date >= '{$date[1]} 00:00:00', items.id, NULL)) as scan_1,
COUNT(IF(batches.change_date >= '{$date[3]} 00:00:00' AND batches.change_date <= '{$date[2]} 23:59:59', items.id, NULL)) as scan_2,
COUNT(IF(batches.change_date <= '{$date[4]} 23:59:59', items.id, NULL)) as scan_3
")
                ->get();

            $unbatched = Item::join('orders', 'items.order_5p', '=', 'orders.id')
                ->whereNull('items.tracking_number')
                ->where('items.batch_number', '=', '0')
                ->where('items.item_status', '=', '1')
                ->whereIn('orders.order_status', [4, 11, 12, 7, 9])
                ->where('orders.is_deleted', '0')
                ->where('items.is_deleted', '0')
                ->selectRaw("
items.id, orders.order_date, items.item_quantity,
SUM(items.item_quantity) as items_count, 
count(items.id) as lines_count,
DATE(MIN(orders.order_date)) as earliest_order_date,
COUNT(IF(orders.order_date >= '{$date[1]} 00:00:00', items.id, NULL)) as order_1,
COUNT(IF(orders.order_date >= '{$date[3]} 00:00:00' AND orders.order_date <= '{$date[2]} 23:59:59', items.id, NULL)) as order_2,
COUNT(IF(orders.order_date <= '{$date[4]} 23:59:59', items.id, NULL)) as order_3
")
                ->first();

            $total = $items->sum('items_count') + $rejects->sum('items_count') + $unbatched->items_count;

        } else {
            $items = $unbatched = $rejects = [];
            $total = 0;
        }

        $graphic_statuses = Rejection::graphicStatus();

        $section = 'start';

        $now = date("F j, Y, g:i a");

        $count = array();

        if ($tab == 'to_export') {
            $to_export = $this->toExport();
            $count['to_export'] = count($to_export);
        } else {
            $count['to_export'] = $this->toExport('count');
        }

        $manual = $this->getManual();
        $count['manual'] = count($manual);

        if ($tab == 'exported') {
            $exported = $this->exported($manual->pluck('batch_number')->all());
            $count['exported'] = count($exported);
        } else {
            $count['exported'] = $this->exported($manual->pluck('batch_number')->all(), 'count');
        }

        if ($tab == 'error') {
            $error_list = $this->graphicErrors();

            $count['error'] = count($error_list);
        } else {
            $count['error'] = $this->graphicErrors('count');
        }

// $found = $this->graphicFound();

        $sections = Section::get()->pluck('section_name', 'id');

        return view('graphics.index', compact('to_export', 'exported', 'error_list', 'manual', 'found', 'sections',
            'count', 'total', 'date', 'items', 'rejects', 'unbatched', 'now',
            'section', 'graphic_statuses', 'tab'));
    }

    private function toExport($action = 'get')
    {
        if ($action == 'get') {
            $batches = Batch::with('itemsCount', 'first_item')
                ->join('stations', 'batches.station_id', '=', 'stations.id')
                ->whereIn('batches.status', [2, 4])
                ->where('stations.type', 'G')
                ->whereNull('export_date')
                ->where('graphic_found', '0')
                ->orderBy('min_order_date')
                ->paginate(50);
        } else if ($action == 'count') {
            $batches = Batch::join('stations', 'batches.station_id', '=', 'stations.id')
                ->whereIn('batches.status', [2, 4])
                ->where('stations.type', 'G')
                ->whereNull('export_date')
                ->where('graphic_found', '0')
                ->count();
        }

        return $batches;
    }

    private function exported($manual, $action = 'get')
    {

        if ($action == 'get') {

            $this->findFiles('exports');

            $batches = Batch::with('itemsCount', 'first_item')
                ->join('stations', 'batches.station_id', '=', 'stations.id')
                ->whereIn('batches.status', [2, 4])
                ->where('stations.type', 'G')
                ->whereNotNull('export_date')
                ->where('graphic_found', '0')
                ->whereNotIn('batch_number', $manual)
                ->orderBy('min_order_date')
                ->get();

        } else if ($action == 'count') {

            $batches = Batch::join('stations', 'batches.station_id', '=', 'stations.id')
                ->whereIn('batches.status', [2, 4])
                ->where('stations.type', 'G')
                ->whereNotNull('export_date')
                ->where('graphic_found', '0')
                ->whereNotIn('batch_number', $manual)
                ->count();

        }

        return $batches;
    }

    private function graphicErrors($action = 'get')
    {

        if ($action == 'get') {

            $error_files = $this->findErrorFiles();

            $batch_numbers = Batch::join('stations', 'batches.station_id', '=', 'stations.id')
                ->whereIn('batches.status', [2, 4])
                ->where('stations.type', 'G')
                ->where('graphic_found', '>', 1)
                ->select('batch_number')
                ->get()
                ->pluck('batch_number')
                ->toArray();

            $batch_numbers = $this->removeSure3d($batch_numbers);

            $batches = Batch::with('items.parameter_option.design')
                ->whereIn('batch_number', $batch_numbers)
                ->orderBy('batches.min_order_date')
                ->get();

            $errors = array();

            foreach ($batches as $batch) {

                $error = array();

                $error['batch'] = $batch;

                $graphic_skus = array();

                if (count($batch->items) == 0) {
                    Log::error('graphicErrors: Batch with zero items ' . $batch->batch_number);
                }

                foreach ($batch->items as $item) {

                    $graphic = array();

                    if ($item->parameter_option && !in_array($item->parameter_option->graphic_sku, $graphic_skus)) {

                        $graphic_skus[] = $item->parameter_option->graphic_sku;

                        $graphic['child_sku'] = $item->child_sku;

                        $graphic['sku'] = $item->parameter_option->graphic_sku;

                        if (!$item->parameter_option->design) {
                            Design::check($item->parameter_option->graphic_sku);
                        }

                        if ($item->parameter_option->design->xml == '1') {
                            $graphic['xml'] = 'Found';
                        } else {
                            $graphic['xml'] = 'Not Found';
                        }

                        if ($item->parameter_option->design->template == '1') {
                            $graphic['template'] = 'Found';
                        } else {
                            $graphic['template'] = 'Not Found';
                        }

                        $error['graphics'][] = $graphic;

                    } else if (!$item->parameter_option) {
                        Log::error('Parameter option not found ' . $batch->batch_number . ',' . $item->id);
                    }
                }

                if (array_key_exists($batch->batch_number, $error_files)) {
                    $error['in_dir'] = 'Found';
                } else {
                    $error['in_dir'] = 'Not Found';
                }

                $errors[] = $error;
                $error = null;
            }

        } else if ($action == 'count') {

            $errors = Batch::join('stations', 'batches.station_id', '=', 'stations.id')
                ->whereIn('batches.status', [2, 4])
                ->where('stations.type', 'G')
                ->where('graphic_found', '>', 1)
                ->count();
        }

        return $errors;
    }

    private function getManual($return_type = 'batches')
    {

        if (env("GRAPHICS_ENV") == "devgraphics") {
            $manual_dir = '/media/devJi-share/5p_batch_csv_export/MANUAL/';
        } else {
            $manual_dir = '/media/Ji-share/5p_batch_csv_export/MANUAL/';
        }

        $manual_list = array_diff(scandir($manual_dir), array('..', '.'));

        $batch_numbers = array();

        if ($return_type == 'list') {

            foreach ($manual_list as $dir) {
//          $batch_numbers[$this->getBatchNumber($dir)] = self::$manual_dir . $dir;
                if (env("GRAPHICS_ENV") == "devgraphics") {
                    $manual_dir = '/media/devJi-share/5p_batch_csv_export/MANUAL/';
                } else {
                    $manual_dir = '/media/Ji-share/5p_batch_csv_export/MANUAL/';
                }
                $batch_numbers[$this->getBatchNumber($dir)] = $manual_dir . $dir;
            }

            $batch_numbers = $this->removeSure3d($batch_numbers);

            return $batch_numbers;

        } else {

            foreach ($manual_list as $dir) {

                $batch_numbers[] = $this->getBatchNumber($dir);

            }

            $batch_numbers = $this->removeSure3d($batch_numbers);

            $batches = Batch::with('itemsCount', 'first_item', 'items')
                ->join('stations', 'batches.station_id', '=', 'stations.id')
                ->whereIn('batches.status', [2, 4])
                ->where('stations.type', 'G')
                ->whereIn('batch_number', $batch_numbers)
                ->orderBy('min_order_date')
                ->get();


            return $batches;
        }
    }

    private function removeSure3d($batch_numbers)
    {

        $items = Item::where('item_status', 1)
            ->whereNull('sure3d')
            ->where('is_deleted', '0')
            ->whereIn('batch_number', $batch_numbers)
            ->where('item_option', 'LIKE', '%Custom_EPS_download_link%')
            ->get();

        $sure3d_batches = array();

        foreach ($items as $item) {

            if (!in_array(substr($item->batch_number, 0, 1), ['R', 'X'])) {

                $options = json_decode($item->item_option, true);
                if (isset($options["Custom_EPS_download_link"]) && $item->sure3d == null) {
                    $item->sure3d = $options["Custom_EPS_download_link"];
                    $item->save();
                    $sure3d_batches[] = $item->batch_number;
                }
            }
        }

        $sure3d_batches = array_unique($sure3d_batches);

        foreach ($sure3d_batches as $batch) {

            $result = Batch::export($batch);

            if (!isset($result['error'])) {
                unset($batch_numbers[array_search($batch, $batch_numbers)]);
            }
        }

        return $batch_numbers;
    }

    private function graphicFound()
    {

        $batches = Batch::with('itemsCount', 'first_item')
            ->join('stations', 'batches.station_id', '=', 'stations.id')
            ->whereIn('batches.status', [2, 4])
            ->where('stations.type', 'G')
            ->where('graphic_found', '1')
            ->where('to_printer', '0')
            ->orderBy('min_order_date')
            ->get();

        return $batches;
    }

    public function sentToPrinter(Request $request)
    {
        $printers = $this->printers;

        $dates = array();
        $date[] = date("Y-m-d");
        $date[] = date("Y-m-d", strtotime('-3 days'));
        $date[] = date("Y-m-d", strtotime('-4 days'));
        $date[] = date("Y-m-d", strtotime('-7 days'));
        $date[] = date("Y-m-d", strtotime('-8 days'));

        if ($request->all() == []) {

            $summary = Batch::join('stations', 'batches.station_id', '=', 'stations.id')
                ->whereIn('batches.status', [2, 4])
                ->where('stations.type', 'G')
                ->where('graphic_found', '1')
                ->where('to_printer', '!=', '0')
                ->selectRaw("
to_printer,
count(DISTINCT batches.id) as batch_count,
COUNT(IF(to_printer_date >= '{$date[1]} 00:00:00', batches.id, NULL)) as group_1,
COUNT(IF(to_printer_date >= '{$date[3]} 00:00:00' AND to_printer_date <= '{$date[2]} 23:59:59', batches.id, NULL)) as group_2,
COUNT(IF(to_printer_date <= '{$date[4]} 23:59:59', batches.id, NULL)) as group_3
")
                ->groupBy('to_printer')
                ->get();

            return view('graphics.sent_printer', compact('summary', 'printers'));

        } else {

            $op = '!=';
            $printer = '0';

            if ($request->has('printer') && $request->get('printer') != '') {
                $op = '=';
                $printer = $request->get('printer');
            }

            $date_1 = '2016-06-01';
            $date_2 = $date[0];

            if ($request->has('date')) {
                if ($request->get('date') == 1) {
                    $date_1 = $date[1];
                } else if ($request->get('date') == 2) {
                    $date_1 = $date[3];
                    $date_2 = $date[2];
                } else if ($request->get('date') == 3) {
                    $date_2 = $date[4];
                } else {
                    Log::error('Sent to Printer: Error unrecognized date ' . $request->get('date'));
                }
            }

            $to_printer = Batch::with('itemsCount', 'first_item')
                ->join('stations', 'batches.station_id', '=', 'stations.id')
                ->whereIn('batches.status', [2, 4])
                ->where('stations.type', 'G')
                ->where('graphic_found', '1')
                ->where('to_printer', $op, $printer)
                ->where('to_printer_date', '>=', $date_1 . ' 00:00:00')
                ->where('to_printer_date', '<=', $date_2 . ' 23:59:59')
                ->selectRaw('batches.*, stations.*, datediff(CURDATE(), to_printer_date) as days')
                ->orderBy('to_printer_date', 'ASC')
                ->get();

            $batch_numbers = $to_printer->pluck('batch_number');

            $w = new Wasatch;
            $batch_queue = array();

            foreach ($batch_numbers as $batch_number) {
                $batch_queue[$batch_number] = $w->notInQueue($batch_number);
            }

            $total_items = Item::where('is_deleted', '0')
                ->whereIn('batch_number', $batch_numbers)
                ->count();

            return view('graphics.sent_printer', compact('to_printer', 'printers', 'total_items', 'batch_queue'));
        }
    }

    public function completeManual(Request $request)
    {

        $success = array();
        $error = array();

        $batch_numbers = $request->get('batch_number');

        if (is_array($batch_numbers)) {

            $batches = Batch::with('route')
                ->whereIn('batch_number', $batch_numbers)
                ->get();

            foreach ($batches as $batch) {

                $batch->graphic_found = '1';
                $batch->save();

                $result = $this->moveNext($batch, 'graphics');

                if ($result['success'] != null) {
                    $success[] = $result['success'];
                }

                if ($result['error'] != null) {
                    $error[] = $result['error'];
                }

                $list = glob($this->csv_dir . 'MANUAL/' . $batch->batch_number . "*");

                if (count($list) > 1) {
                    $error[] = 'More than one CSV file in Manual Directory for batch ' . $batch->batch_number . ' - Files Not Moved';
                }

                foreach ($list as $file) {

                    $to_file = $this->uniqueFilename($this->finished_dir, substr($file, strrpos($file, '/') + 1));

                    try {
                        $moved = @rename($file, $this->finished_dir . $to_file);
                        if (!$moved) {
                            $this->recurseCopy($file, $this->finished_dir . $to_file);
                            $this->removeFile($file);
                        }
                    } catch (Exception $e) {
                        Log::error('completeManual: Error moving manual csv file ' . $to_file . ' - ' . $e->getMessage());
                        $error[] = 'Error moving manual csv file - ' . $e->getMessage();
                    }
                }
            }

            $success[] = sprintf("Batches: %s Graphics processed.", implode(", ", $batch_numbers));

            return redirect()->action('GraphicsController@index', ['tab' => 'manual'])
                ->withSuccess($success)
                ->withErrors($error);

        } else {
            return redirect()->action('GraphicsController@index', ['tab' => 'manual'])->withErrors('No Batches Selected*');
        }

    }

    public function selectSummaries()
    {

// if (auth()->user()->id != 83) {
//   return 'Please try again later';
// }

        $production = Batch::with('production_station', 'store')
// ->join('sections', function($join)
//         {
//             $join->on('batches.section_id', '=', 'sections.id')
//                   ->where('sections.inventory', '!=', '1')
//                   ->orWhere(DB::raw('batches.inventory'), '=', '2');
//         })
            ->where('batches.is_deleted', '0')
            ->selectRaw('production_station_id, section_id, store_id, if(substr(batch_number,1,1) = "R", "Reject", "") as type, count(batches.id) as count')
            ->searchStatus('active')
            ->searchPrinted('0')
            ->groupBy('section_id')
            ->groupBy('production_station_id')
            ->groupBy('store_id')
            ->groupBy('type')//->toSql();
            ->get();

        $graphics = Batch::with('store')
            ->join('batch_routes', 'batches.batch_route_id', '=', 'batch_routes.id')
            ->where('batches.is_deleted', '0')
            ->selectRaw('batch_route_id, batch_routes.graphic_dir, store_id, if(substr(batch_number,1,1) = "R", "Reject", "") as type, count(batches.id) as count')
            ->searchStatus('active')
            ->searchPrinted('2')
            ->groupBy('batch_routes.graphic_dir')
            ->groupBy('store_id')
            ->groupBy('type')
            ->get();

        $date = date("Y-m-d") . ' 00:00:00';

        $today = Batch::with('production_station', 'section', 'summary_user')
            ->selectRaw('summary_date, summary_user_id, production_station_id, section_id, count(batch_number) as count')
            ->searchStatus('active')
            ->where('summary_date', '>', $date)
            ->groupBy('section_id')
            ->groupBy('production_station_id')
            ->groupBy('summary_date')
            ->groupBy('summary_user_id')
            ->orderBy('summary_date', 'DESC')
            ->get();

        return view('graphics.print_summaries', compact('production', 'graphics', 'today'));
    }

    public function selectToMove(Request $request)
    {

        $request->has('store_id') ? $store_id = $request->get('store_id') : $store_id = null;

        $to_move = Batch::with('section', 'production_station')
            ->join('stations', 'batches.station_id', '=', 'stations.id')
            ->where('batches.is_deleted', '0')
            ->searchStatus('movable')
            ->where('graphic_found', '1')
////->whereNotNull('summary_date')
            ->where('stations.type', 'G')
            ->searchStore($store_id)
            ->selectRaw('section_id, production_station_id, count(*) as total')
            ->groupBy('section_id')
            ->groupBy('production_station_id')
            ->orderBy('section_id')
            ->get();
        $last_scan = Batch::with('section', 'production_station')
            ->join('stations', 'batches.station_id', '=', 'stations.id')
            ->where('batches.is_deleted', '0')
            ->where('stations.type', 'P')
            ->latest('batches.change_date')
            ->take(5)
            ->get();

//    foreach ($last_scan as $scan){
//        $usernames[] = Batch::lastScan($scan->batch_number);
//    }
        for ($i = 0; $i < 5; $i++) {
            $username[$i] = Batch::lastScan($last_scan[$i]->batch_number);
            $name[$i] = $username[$i]['username'];
        }

        $sections = Section::get()->pluck('section_name', 'id');

        $stores = Store::list('1');

        return view('graphics.move_production', compact('last_scan', 'name', 'to_move', 'sections', 'stores', 'store_id'));
    }

    public function selectToMoveQc(Request $request)
    {

        $request->has('store_id') ? $store_id = $request->get('store_id') : $store_id = null;

        $to_move = Batch::with('section', 'production_station')
            ->join('stations', 'batches.station_id', '=', 'stations.id')
            ->where('batches.is_deleted', '0')
            ->searchStatus('movable')
            ->where('graphic_found', '1')
////->whereNotNull('summary_date')
            ->where('stations.type', 'P')
            ->searchStore($store_id)
            ->selectRaw('section_id, production_station_id, count(*) as total')
            ->groupBy('section_id')
            ->groupBy('production_station_id')
            ->orderBy('section_id')
            ->get();
        $last_scan = Batch::with('section', 'production_station', 'items')
            ->join('stations', 'batches.station_id', '=', 'stations.id')
            ->where('batches.is_deleted', '0')
            ->where('stations.type', 'Q')
            ->latest('batches.change_date')
            ->take(5)
            ->get();

        for ($i = 0; $i < 5; $i++) {
            $username[$i] = Batch::lastScan($last_scan[$i]->batch_number);
            $name[$i] = $username[$i]['username'];
        }


        $sections = Section::get()->pluck('section_name', 'id');

        $stores = Store::list('1');


        return view('graphics.move_qc', compact('last_scan', 'name', 'to_move', 'sections', 'stores', 'store_id', 'label'));
    }

    public function showBatch(Request $request)
    {

        if (!$request->has('scan_batches')) {
            return redirect()->action('GraphicsController@selectToMove')->withErrors('No Batch Selected');
        }

        $scan_batches = trim($request->get('scan_batches'));

        if (substr($scan_batches, 0, 4) == 'BATC') {
            $batch_number = $this->getBatchNumber(substr($scan_batches, 4));
        } else {
            $batch_number = $this->getBatchNumber($scan_batches);
        }

        if ($batch_number == null) {
            return redirect()->action('GraphicsController@selectToMove')->withErrors('No Batch Selected');
        }

        $result = $this->moveNext($batch_number, 'production');

        if ($result['error'] != null) {
            return redirect()->action('GraphicsController@selectToMove')->withErrors($result['error']);
        }

        $to_move = Batch::with('items', 'route', 'station', 'summary_user')
            ->where('batch_number', $result['batch_number'])
            ->first();

        return view('graphics.show_batch', compact('to_move'));

    }

    public function showBatchQc(Request $request)
    {

        if (!$request->has('scan_batches')) {
            return redirect()->action('GraphicsController@selectToMoveQc')->withErrors('No Batch Selected');
        }

        $scan_batches = trim($request->get('scan_batches'));

        if (substr($scan_batches, 0, 4) == 'BATC') {
            $batch_number = $this->getBatchNumber(substr($scan_batches, 4));
        } else {
            $batch_number = $this->getBatchNumber($scan_batches);
        }

        if ($batch_number == null) {
            return redirect()->action('GraphicsController@selectToMoveQc')->withErrors('No Batch Selected');
        }

        $result = $this->moveNext($batch_number, 'qc');

        if ($result['error'] != '') {
            Batch::note($batch_number, 4, '6', 'Production - ' . $result['error']);
            return redirect()->action('GraphicsController@selectToMoveQc')->withErrors($result['error']);
        }


        $items = Item::where('items.batch_number', $batch_number)
            ->where('items.is_deleted', '0')
            ->first();

        $customer = Customer::where('order_id', $items->order_id)
            ->where('is_deleted', '0')
            ->first();

        $parts = parse_url($items->item_thumb);
        $ImageFilename = basename($parts["path"]);

// /assets/images/Sure3d/thumbs/1217029-13-Image.jpg
        $thumb_dir = base_path() . Sure3d::thumb_dir . $ImageFilename;
        if (file_exists($thumb_dir)) {
            $decoder = GdDecoder::fromPath($thumb_dir);
            $image = new Image($decoder);
            $zpl = new Builder();
//            $zplImg = $zpl->fo(50, 330)->gf($image)->fs();
            $zplImg = $zpl->gf($image);

            $zplImage = "^FO50,330";
            $zplImage .= $zplImg->__toString();
            $zplImage .= "^FS";
            $zplImage = str_replace("^XA", "", $zplImage);
            $zplImage = str_replace("^XZ", "", $zplImage);

        } else {
            $zplImage = "^FO40,360^FDNo Image Found^FS";
        }


        $filename = "^XA";
        $filename .= "^CF0,60";
        $filename .= "^FO100,50^FD Batch Number^FS";
        $filename .= "^FX for barcode.";
        $filename .= "^BY5,2,270";
        $filename .= "^FO50,100";
        $filename .= "^BCN,100,Y,N,N";
        $filename .= "^FD$batch_number^FS";
        $filename .= "^CF0,40";
        $filename .= "^FO40,245^FDCustomer name: $customer->ship_full_name^FS";
        $filename .= "^FO40,280^FDStyle Number: $items->item_code ^FS";
        $filename .= "^FO40,320^FDQTY: $items->item_quantity^FS";
        $filename .= $zplImage;
        $filename .= "^XZ";


        $label = trim(preg_replace('/\n+/', ' ', $filename));
        $to_move = Batch::with('items', 'route', 'station', 'summary_user')
            ->where('batch_number', $result['batch_number'])
            ->first();

        return view('graphics.show_batch_qc', compact('to_move', 'label'));

    }

    public function showSublimation(Request $request)
    {
// Send Print command to printer by Batch
        set_time_limit(0);

// /media/graphics/
        if (!file_exists($this->sort_root)) {
            return redirect()->back()->withErrors('Cannot find graphics directory on P: drive');
        }
        $helper = new Helper();
        $request->has('from_date') ? $from_date = $request->get('from_date') . ' 00:00:00' : $from_date = date("Y-m-d H:i:s");
        $request->has('to_date') ? $to_date = $request->get('to_date') . ' 23:59:59' : $to_date = date("Y-m-d H:i:s");
        $request->has('store_id') ? $store_id = $request->get('store_id') : $store_id = null;
        $request->has('filterWap') ? $filterWap = $request->get('filterWap') : $filterWap = null;
        $request->has('production_station_id') ? $production_station_id = $request->get('production_station_id') : $production_station_id = null;
        $request->has('type') ? $type = $request->get('type') : $type = null;
        $request->has('select_batch') ? $select_batch = $request->get('select_batch') : $select_batch = null;
        $request->has('details_btn') ? $detailsRepot = $request->get('details_btn') : $detailsRepot = null;

#############
        $from_date_auto = $this->getAutoDate($this->sort_root . "fromDate"); # date("Y-m-d H:i:s");
        $to_date_auto  = $this->getAutoDate($this->sort_root . "toDate"); #date("Y-m-d H:i:s");
#############
        if ($request->all() != [] && empty(!$detailsRepot)) {
// List view
            $batches = Batch::with('items.parameter_option.design', 'production_station', 'route')
                ->join('stations', 'batches.station_id', '=', 'stations.id')
//                  ->where('batches.section_id', 1)
                ->searchStatus('active')
                ->where('stations.type', 'G')
                ->where('stations.id', 6)
                ->where('graphic_found', '1')
                ->where('to_printer', '0')
                ->searchBatch($select_batch)
                ->where('min_order_date', '>', $from_date)
                ->where('min_order_date', '<', $to_date)
                ->searchStore($store_id)
                ->searchProductionStation($production_station_id)
// ->where('to_printer', '0')
                ->select('batch_number', 'status', 'station_id', 'batch_route_id', 'store_id',
                    'to_printer', 'to_printer_date', 'min_order_date', 'production_station_id')
                ->orderBy('min_order_date')
                ->get();
            $summary = [];
//            dd($batches);
        } else {
// Send Print command
            $batches = [];
            $summary = Batch::with('production_station')
                ->join('stations', 'batches.station_id', '=', 'stations.id')
//                          ->where('batches.section_id', 1) // sections.section_name = SUBLIMATION
                ->searchStatus('active')
//                          ->where('stations.type', 'G')
                ->where('stations.id', 6)//  stations.station_name = SUB-GRAPHICS
//                          ->where('graphic_found', '1')
//                          ->where('to_printer', '0')

                ->where('min_order_date', '>', $from_date)
                ->where('min_order_date', '<', $to_date)

                ->selectRaw('production_station_id, MIN(min_order_date) as date, count(*) as count')
                ->groupBy('production_station_id')
                ->orderBy('date', 'ASC')
                ->get();
            foreach ($summary as $key => $value) {

            }
        }

        $w = new Wasatch;
        $queues = $w->getQueues();
# $station_ids = array_unique($batches->pluck('production_station_id')->toArray());

        $stations = Station::where('is_deleted', '0')
            ->whereIn('type', ['P', 'Q'])
            ->where('section', 1)
            ->get()
            ->pluck('station_description', 'id');

        if (count($batches) > 0) {
            $store_ids = array_unique($batches->pluck('store_id')->toArray());

            $stores = Store::where('is_deleted', '0')
                ->where('batch', '1')
                ->whereIn('store_id', $store_ids)
                ->where('invisible', '0')
                ->orderBy('sort_order')
                ->get()
                ->pluck('store_name', 'store_id');
        } else {
            $stores = Store::list(1, '%', 'none');
        }

        $printers = $this->printers;

        $config = Printer::configurations();

        if (isset($from_date) && $from_date == '2016-06-01 00:00:00') {
            $from_date = null;
        }

        $childScale = 100;
        $file_pointer = $this->sort_root . 'autoPrintHard';
        $locked = self::jobLocked($file_pointer);
        $rejectLocks = self::jobLocked($this->sort_root . 'rejectLocks');
        ###################
        $batchNumberWasatchDashboard = [];
        $jobsInDatabase = WasatchDashboard::get();
        foreach ($jobsInDatabase as $results) {
            $batchNumberWasatchDashboard[$results->batch_number] = $results->batch_number;
        }
//        echo "<pre>";
//        print_r($batchNumberWasatchDashboard);
//        echo "</pre>";
        ###################
        return view('graphics.print_sublimation', compact('batches', 'printers', 'queues', 'stores', 'store_id', 'config', 'select_batch', 'filterWap',
            'stations', 'production_station_id', 'from_date', 'from_date', 'to_date', 'summary', 'childScale', 'locked','rejectLocks','batchNumberWasatchDashboard', 'from_date_auto', 'to_date_auto'));
    }

    public function printSublimation(Request $request)
    {
//        ########################3
//        #############20191011 Send PDF via ssh ######################
//        $batch_number = "8984";
//        try {
//            $host = "96.57.0.134";
//            $sftp = new SFTPConnection($host, 22);
//            $sftp->login('jewel', 'Dtw#123Dtw');
//        } catch (\Exception $e) {
//            Log::error('Wasatch: SFTP pdf connection error ' . $e->getMessage());
//            return FALSE;
//        }
//
//        try {
//
////            $files = $sftp->uploadDirectory( "/home/jewel/test/" , "/wasatch/staging-1/");
//            if(env("GRAPHICS_ENV") == "devgraphics"){
//                $files = $sftp->uploadFiles( [$batch_number . '.pdf'],"/media/Wasatch/DEVEpson-1/", "/../../../../media/devgraphics/summaries/");
//            }else{
//                $files = $sftp->uploadFiles( [$batch_number . '.pdf'],"/media/Wasatch/Collage_summaries/" , "/../../../../media/graphics/summaries/");
//            }
//
//
//        } catch (\Exception $e) {
//            Log::error('Wasatch: SFTP pdf  upload error ' . $e->getMessage());
//            return FALSE;
//        }

//        #############20191011 Send PDF via ssh ######################
//        ##########################
// Print command send using send button

        if (!file_exists($this->sort_root)) {
            return 'Cannot find graphics directory on P: drive';
        }


        if (!$request->has('printer')) {
            Log::error('printSublimation: Printer not provided');
            return 'Printer not provided';
        }

        if ($request->has('batch_number') && $request->get('batch_number') != '') {
            $batch_number = $request->get('batch_number');
        }

        $file = $this->getArchiveGraphic($batch_number);

//        Log::debug("printSublimation file: " . print_r($file, true));

        if (substr($file, 0, 5) == 'ERROR') {
            return $file;
        }

        $printer = $request->get('printer');
//        Log::debug("printSublimation printer: " . print_r($printer, true));

//      $file = /var/www/devoms/public_html/media/graphics/archive/59178
//      $printer = EPSON-3
        $x = $this->printSubFile($file, $printer, $batch_number,
            $request->get('scale'), $request->get('minsize'), $request->get('mirror'));

// flock($f, LOCK_UN);
// fclose($f);

        return $x;

    }

    public function printAllSublimation(Request $request)
    {

        $error = array();
        $success = array();

        if ($request->getRealMethod() == "POST") {
            if (!file_exists($this->sort_root)) {
                return redirect()->back()->withErrors('Cannot find graphics directory =' . $this->sort_root);
            }


            if (!file_exists($this->sort_root . 'lockPost')) {
                touch($this->sort_root . 'lockGet');
            }

            $f = fopen($this->sort_root . 'lockPost', 'r');

            if (!flock($f, LOCK_EX)) {
                Log::info('Print sublimation - Sublimation is  lockPost locked');
                return json_encode(['Sublimation lockGet Directory Busy... Retry']);
            }


            if (!$request->has('print_batches') || !$request->has('printer')) {
                Log::error('printAllSublimation: Batches or Printer not provided');
                return redirect()->back()->withErrors('Batches or Printer not provided');
            }

            $print_batches = $request->get('print_batches');
            $printer = $request->get('printer');


            foreach ($print_batches as $batch_number) {

                $wasatchDashboard = $this->wasatchDashboard->getBatchInfo($batch_number);

                if ($wasatchDashboard) {
//                    $error[] = "Batch# " . $batch_number . " already in Print queue tbl  ";
                    $wasatchDashboard->batch_number = $batch_number;
                    $wasatchDashboard->is_auto = 1;
                    $wasatchDashboard->printer_number = substr($printer, -1);
                    $wasatchDashboard->save();
                } else {
                    $success[] = "Batch# " . $batch_number . " will print in  queue";
                    $wasatchDashboard = new WasatchDashboard();
                    $wasatchDashboard->batch_number = $batch_number;
                    $wasatchDashboard->is_auto = 1;
                    $wasatchDashboard->printer_number = substr($printer, -1);
                    $wasatchDashboard->save();
                }
            }


            flock($f, LOCK_UN);
            fclose($f);


            return redirect()->action('GraphicsController@showSublimation')
                ->withError($error)
                ->withSuccess($success);

        } elseif ($request->getRealMethod() == "GET") {

            if (!file_exists($this->sort_root . 'lockGet')) {
                touch($this->sort_root . 'lockGet');
            }

            $f = fopen($this->sort_root . 'lockGet', 'r');

            if (!flock($f, LOCK_EX)) {
                Log::info('Print sublimation - Sublimation is  lockGet locked');
                return json_encode(['Sublimation lockGet Directory Busy... Retry']);
            }

            ###############Get Print Queu############
            $ripStatus=[];
            $w = new Wasatch;
//            $queues = $w->getPrintAndRipQueues("all");
//            $ripStatus['all'][] =  $w->checkPrinQueu($queues);
            $ripStatus['getwasatch11status'][] =  $w->checkPrinQueu($w->getPrintAndRipQueues("getwasatch11status"));  // 1,3,4,5
            $ripStatus['getwasatch23status'][] = $w->checkPrinQueu($w->getPrintAndRipQueues("getwasatch23status"));   // 6
            $ripStatus['getwasatch130status'][] = $w->checkPrinQueu($w->getPrintAndRipQueues("getwasatch130status")); // 2

            ###############Get Print Queu############

            $jobsInDatabase = WasatchDashboard::get();

            foreach ($jobsInDatabase as $results) {
                if ($results->is_auto != 1) {
                    continue;
                }

                $file = $this->getArchiveGraphic($results->batch_number);
                echo "<pre>"; echo $results->batch_number." file name =".$file." sent to printer = EPSON-".$results->printer_number; echo "</pre>";
                if (substr($file, 0, 5) != 'ERROR') {


                    if((count($ripStatus['getwasatch11status'][0]) == 0) && (in_array($results->printer_number, [1,3,4,5]))){
                        $x = $this->printSubFile($file, "EPSON-" . $results->printer_number, $results->batch_number, $scale = 100, $minsize = null, $mirror = 0);
                        sleep(180);
                        continue;
                    }

                    if((count($ripStatus['getwasatch23status'][0]) > 0) && (in_array($results->printer_number, [6]))){
                        $x = $this->printSubFile($file, "EPSON-" . $results->printer_number, $results->batch_number, $scale = 100, $minsize = null, $mirror = 0);
                        continue;
                    }

                    if((count($ripStatus['getwasatch130status'][0]) > 0)  && (in_array($results->printer_number, [2]))){
                        $x = $this->printSubFile($file, "EPSON-" . $results->printer_number, $results->batch_number, $scale = 100, $minsize = null, $mirror = 0);
                        continue;
                    }

                }
            }


            flock($f, LOCK_UN);
            fclose($f);
            return json_encode([$error, $success]);

        }
    }


    public function printerConfig(Request $request)
    {
        if (!$request->has('number')) {
            return 'ERROR: Printer not specified';
        }

        if (Printer::config($request->get('number'), $request->get('station'))) {
            return 'success';
        } else {
            return 'ERROR: Update Failed';
        }
    }

    public function autoPrint()
    {
        set_time_limit(0);
        $jobStatus = [];
        $file_pointer = $this->sort_root . 'autoPrint';
        $file_pointerHard = $this->sort_root . 'autoPrintHard';
        $fromDate = $this->getAutoDate($this->sort_root . 'fromDate'). ' 00:00:00';
        $toDate = $this->getAutoDate($this->sort_root . 'toDate'). ' 23:59:59';


        ################ Check Previous Corn already Executed ##############
        if (file_exists($file_pointer)) {
            Log::error('AutoPrint: Sublimation lockGet Directory already executing');
            return json_encode($jobStatus["error"][] = 'AutoPrint: Sublimation Directory already executing in' . $file_pointer);
        } else {
            self::jobLock($file_pointer);
            Log::info('AutoPrint: Lock file created  in' . $file_pointer);
        }
        ################ Check Previous Corn already Executed ##############

        $batches = Batch::with('items.parameter_option.design', 'production_station', 'route')
            ->join('stations', 'batches.station_id', '=', 'stations.id')
            ->where('batches.section_id', 1)
            ->searchStatus('active')
            ->where('stations.type', 'G')
            ->where('stations.id', 6)
            ->where('graphic_found', '1')
            ->where('to_printer', '0')
            ->where('min_order_date', '>', $fromDate)
            ->where('min_order_date', '<', $toDate)
            ->searchStore(null)
            ->searchProductionStation(null)
            ->select('batch_number', 'status', 'station_id', 'batch_route_id', 'store_id',
                'to_printer', 'to_printer_date', 'min_order_date', 'production_station_id')
            ->orderBy('min_order_date')
            ->limit(10)
            ->get();

        $i = 0;
        foreach ($batches as $batch) {
            ############ Debug Codes ###############
//            echo "<pre>". $i++ ."   -> ";
//                print_r($batch->batch_number);
//            echo "</pre>";
//            echo "<pre>"; print_r($batch->route->printer); echo "</pre>";
            ############ Debug Codes ###############

            ############ Validation's ##############
            if(file_exists($file_pointerHard)){
                $jobStatus["error"][$batch->batch_number] = "AutoPrint: Hard Sublimation Directory executing stop in" . $file_pointer;
                break; // Stop Loop
            }

            if((file_exists($this->sort_root . 'rejectLocks') && (substr($batch->batch_number, 0, 1) == 'R'))){
                $jobStatus["error"][$batch->batch_number] = "AutoPrint: Don't print Reject Batch which are rejected ";
                continue; // Don't print Reject Batch which are rejected
            }

            $wasatchDashboard = $this->wasatchDashboard->getBatchInfo($batch->batch_number);

            if ($wasatchDashboard) {
                $jobStatus["error"][$batch->batch_number] = "Auto Print: already exist in wasatch_dashboard tbl";
                continue; // Go to next Batch
            }

            // If no printer selected from Route
            if (empty($batch->route->printer)) {
                $jobStatus["error"][$batch->batch_number] = "Auto Print: No printer configured in route";
                continue; // Go to next Batch
            }
            ############ Validation's ##############

            ### Code for send file to Printer ####
            ## $file = /var/www/devoms/public_html/media/graphics/archive/59178
            $file = $this->getArchiveGraphic($batch->batch_number);

            ## $printer = EPSON-3
            $printer = $batch->route->printer;

            ## Call Send to printer function
            $result = $this->printSubFile($file, $printer, $batch->batch_number, $scale = 100, $minsize = null, $mirror = 0);

            if($result == "success") {
                Log::info("Auto  Batch # " . $batch->batch_number . " sent to Printer = " . $printer . " status = " . $result);
                $jobStatus["success"][$batch->batch_number] = ["Auto Print:  Batch # " . $batch->batch_number . " sent to Printer = " . $printer . " Status = " . $result];
            }else{
                Log::info("Auto  Batch # " . $batch->batch_number . " sent to Printer = " . $printer . " status = " . $result);
                $jobStatus["error"][$batch->batch_number] = ["Auto Print:  Batch # " . $batch->batch_number . " sent to Printer = " . $printer . " Status = " . $result];
            }
            sleep(10);
            ### Code for send file to Printer ####

        }

        ################ Unlock after successful execution ##############
        if (!self::jobUnlock($file_pointer)) {
            $jobStatus["error"]["lockStatus"] = $file_pointer . " -> cannot be deleted due to an error";
        } else {
            $jobStatus["success"]["lockStatus"] = $file_pointer . " -> has been deleted";
            Log::info("AutoPrint:'" . $file_pointer . "' -> has been deleted");
        }
        ################ Unlock after successful execution ##############
        return json_encode($jobStatus);

    }

    /**
     * @param $file
     * @param $printer
     * @param null $batch_number
     * @param int $scale
     * @param null $minsize
     * @param int $mirror
     * @return string|null
     */
    private function printSubFile($file, $printer, $batch_number = null, $scale = 100, $minsize = null, $mirror = 0)
    {
//      $file = /var/www/devoms/public_html/media/graphics/archive/7
//      $printer = SOFT-1

// Check Batch directory exist.
        if (!file_exists($file)) {
            Log::error('printSubFile: File not found ' . $file);
            return 'File not found';
        }

// Get batch number from file last / digit
        if ($batch_number == null) {
            $batch_number = $this->getBatchNumber($file);
        }

// Get Batch detail's by batch ID
//      $batch = Batch::where('batch_number', $batch_number)->first();
        $batch = Batch::with('items.parameter_option.design')
            ->where('batch_number', $batch_number)
            ->first();
// ($item->parameter_option->graphic_sku

        $orientation = 0;
        $qty = 1;
        $childScale = 100;
        set_time_limit(0);
        $graphics = array();
        foreach ($batch->items as $item) {
            $orientation = $item->parameter_option->orientation;
            $mirror = $item->parameter_option->mirror;
            $qty = (int)$item->item_quantity;
            $childScale = $item->parameter_option->scale;

            ##############XXXXXXXXXXXXX############

            $options = json_decode($item->item_option, true);
            foreach ($options as $label => $imageUrl) {
                if ($label != "Custom_EPS_download_link") {
                    $label = ucwords(strtolower($label));
                    $graphics[] = $item->batch_number . '-#_' .$item->order->short_order . '-#_' . $item->id . '-#_' . $label . substr($item->sure3d, strrpos($item->sure3d, '.')).'-#_'.$item->order->order_numeric_time;
                    $lineItemQty[] = $item->item_quantity;
                }
            }
            ##############XXXXXXXXXXXXX############
        }
//        Log::info('printSubFile: childScale  = ' . $childScale);
        if (!is_numeric($childScale) || ($childScale <= 0)) {
            die("Please update scale value in  Configure Child SKUs ");
            exit();
        }

        if (!$batch) {
            Log::error('printSubFile: Batch not found ' . $batch_number);
            return 'Batch not found ' . $batch_number;
        }

// Check if Batch already sent to printer (xml file created)
        if ($batch->to_printer != '0') {
            Log::error('printSubFile: Batch already printed ' . $batch_number);
            Batch::note($batch->batch_number, $batch->station_id, '6', 'Batch already printed - printSublimation');
            return 'Batch marked as printed';
        }

//        $w = new Wasatch;
//        $notInQueue = $w->notInQueue($batch_number);
//
//        // Check is Batch already in Wasas Queue
//        if ($notInQueue != '1') {
//            return $notInQueue;
//        }

// Created PDF BATCH Summary
// $summary_file = /media/graphics/summaries/6.pdf
        $summary_file = $this->createSummary($batch_number);

// $file = /var/www/devoms/public_html/media/graphics/archive/7
        $file_list = FileHelper::getContents($file);
//$file_list = [ 0 => "/var/www/devoms/public_html/media/graphics/archive/7/7-0.jpg",
//               1 => "/var/www/devoms/public_html/media/graphics/archive/7/7-1.jpg"]

        if (!is_array($file_list) || count($file_list) < 1) {
            Log::error('printSubFile: No Files Found - ' . $file);
            return 'Error: No Files Found';
        }

        $list = array();
        $i =0;
        foreach ($file_list as $path) {
// Get Archive Image size from  "/var/www/devoms/public_html/media/graphics/archive/7/7-0.jpg"
//        $info = ImageHelper::getImageSize($path, $scale);
            $info = ImageHelper::getImageSize($path, $childScale);

//  $info =  array: [
//      "file" => "/var/www/devoms/public_html/media/graphics/archive/7/7-0.jpg"
//      "type" => ".jpg"
//      "width" => "74.306"
//      "height" => "89.167"
//      "scale" => "100"
//  ]


            if (is_array($info)) {
// If image found
                $info['mirror'] = $mirror;
                if(isset($graphics[$i])) {
                    $info['itmem_info'] = $this->formatLineText($graphics[$i]);
                    $info['line_item_qty'] = $lineItemQty[$i];
                }else{
                    $info['itmem_info'] = [];
                    $info['line_item_qty'] = 0;
                }
                $list[str_replace($this->sort_root, '/', $path)] = $info;
            } else {
// If Image not found
                Log::error('printSubFile: Imagesize Error - ' . $path);
                $batch->graphic_found = '7';
                $batch->save();
                self::removeFile($path);
                return 'Imagesize Error: ' . $path;
            }
            $i++;
        }


//  $summary_file = /media/graphics/summaries/6.pdf


        if ($summary_file != false && file_exists($summary_file)) {
// Get /media/graphics/summaries/6.pdf size.
            $info = ImageHelper::getImageSize($summary_file);
#############20191011 Send PDF via ssh ######################
            try {
                $host = "96.57.0.134";
                $sftp = new SFTPConnection($host, 22);
                $sftp->login('jewel', 'Dtw#123Dtw');
            } catch (Exception $e) {
                Log::error('Wasatch: SFTP pdf connection error ' . $e->getMessage());
                return FALSE;
            }

            try {

//            $files = $sftp->uploadDirectory( "/home/jewel/test/" , "/wasatch/staging-1/");
//                if (env("GRAPHICS_ENV") == "devgraphics") {
//                    $files = $sftp->uploadFiles([$batch_number . '.pdf'], "/media/Wasatch/DEVEpson-1/", "/../../../../media/devgraphics/summaries/");
//                } else {
//                    $files = $sftp->uploadFiles([$batch_number . '.pdf'], "/media/Wasatch/Collage_summaries/", "/../../../../media/graphics/summaries/");
//                }
                $files = $sftp->uploadFiles([$batch_number . '.pdf'], "/media/Wasatch/Collage_summaries/", "/../public_html/media/graphics/summaries/");

            } catch (Exception $e) {
                Log::error('Wasatch: SFTP pdf  upload error ' . $e->getMessage());
                return FALSE;
            }

#############20191011 Send PDF via ssh ######################

        } else {
            Log::error('printSubFile: Batch Summary Creation Error - ' . $batch_number);
            return 'Batch Summary Creation Error';
        }

        if (is_array($info)) {
            $info['line_item_qty'] =1;
            $list[str_replace($this->sort_root, '/', $summary_file)] = $info;
        } else {
            Log::error('printSubFile: Batch Summary Imagesize Error - ' . $batch_number);
            return 'Batch Summary Imagesize Error';
        }

        sleep(15);


        $w = new Wasatch;
// Create one xml file for print in SOFT-1 or Epson folder
// $list files name
        $w->printJob($list,
            $batch_number,
            $qty,
            substr($printer, -1),
            "EPSON",
            $orientation,
            substr($printer, 0, 4)
        );

        Batch::note($batch->batch_number, '', '6', 'Sent to ' . $printer);

        if ($batch) {
            try {

// After create xml file move batch to next station print
                $msg = $this->moveNext($batch, 'print');

                if ($msg['error'] != '') {
                    Log::info('printSubFile: ' . $msg['error'] . ' - ' . $file);
                    Batch::note($batch->batch_number, $batch->station_id, '6', 'printSublimation - ' . $msg['error']);
                    return 'Error: ' . $msg['error'] . ' - ' . $batch_number;
                }

                $batch->to_printer = $printer;
                $batch->to_printer_date = date("Y-m-d H:i:s");
                $batch->change_date = date("Y-m-d H:i:s");
                $batch->save();

            } catch (Exception $e) {
                Log::error('printSubFile: Error moving batch ' . $file . ' - ' . $e->getMessage());
                Batch::note($batch->batch_number, $batch->station_id, '6', 'Exception moving Batch - printSublimation');
                return 'Error: Error moving batch ' . $batch_number;
            }

            Batch::note($batch->batch_number, $batch->station_id, '6', 'Graphics Sent to Printer');
        } else {
            Log::error('printSubFile: Batch not found ' . $batch_number);
            return 'Error: Batch not found ' . $batch_number;
        }

        return 'success';
    }

    public function formatLineText($lineId)
    {
        $lineArray = explode("-#_",$lineId);
        $ordrInfos['batch_id'] = $lineArray[0];
        $ordrInfos['short_order'] = $lineArray[1];
        $ordrInfos['item_id'] = $lineArray[2];
        $ordrInfos['image_name'] = $lineArray[3];
        $ordrInfos['order_date'] = date('Y-m-d', $lineArray[4]);
        $ordrInfos['group'] = "A";

        return $ordrInfos;
    }

    public function printWasatch()
    {

        $w = new Wasatch;
        return $w->stagedXml();

    }

    private function findFiles($type)
    {
        if ($type = 'export') {
            $dir = $this->csv_dir;
            $field = 'csv_found';
        }

        $export_dirs = BatchRoute::where('export_dir', '!=', '')
            ->selectRaw('DISTINCT export_dir')
            ->get()
            ->toArray();

        $batches = array();

        foreach ($export_dirs as $export) {

            if (!is_dir($dir . $export['export_dir'])) {
                continue;
            }

            $directory = new \RecursiveDirectoryIterator($dir . $export['export_dir']);
            $iterator = new \RecursiveIteratorIterator($directory);

            foreach ($iterator as $info) {
                $name = $info->getFilename();

                if ($name[0] != '.') {

                    $batch_num = $this->getBatchNumber($name);

                    if ($batch_num != null) {
                        $batches[] = $batch_num;
                    }
                }
            }
        }

        if (count($batches) > 0) {
            Batch::whereIn('batch_number', $batches)
                ->where($field, '0')
                ->update([
                    $field => '1'
                ]);
        }

        return true;
    }

    public function moveNext($batch, $type)
    {

        $success = null;
        $error = null;

        if (!($batch instanceof Batch)) {

            $num = $batch;

            $batch = Batch::with('route.stations_list', 'station')
                ->where('batch_number', $num)
                ->searchstatus('active')
                ->first();

            if (!$batch || count((array)$batch) == 0) {

                $related = Batch::related($num);

                if ($related == false) {
                    return [
                        'error' => sprintf('Batch not found'),
                        'success' => $success,
                        'batch_number' => $num
                    ];
                } else {
                    $batch = $related;
                }
            }

        }

        $next_station = Batch::getNextStation('object', $batch->batch_route_id, $batch->station_id);
        if ($type == 'graphics') {
// test if it is the first graphics station in route
            if (!($batch->route->stations_list->first()->station_id == $batch->station_id && $batch->station->type == 'G')) {
                return ['error' => $error,
                    'success' => sprintf('Warning: Batch %s not in first graphics station', $batch->batch_number),
                    'batch_number' => $batch->batch_number
                ];
            }

        } else if ($type == 'production') {

            if (!($batch->station->type == 'G' && $next_station->type == 'P')) {
                return [
                    'error' => sprintf('Batch %s not moving from graphics to production - ' .
                        $batch->station->station_name . ' ' . $batch->change_date . '<br>',
                        $batch->batch_number),
                    'success' => $success,
                    'batch_number' => $batch->batch_number
                ];
            }

            if ($batch->status != 'active' && $batch->status != 'back order') {
                return [
                    'error' => sprintf('Batch %s status is %s', $batch->batch_number, $batch->status),
                    'success' => $success,
                    'batch_number' => $batch->batch_number
                ];
            }

        } else if ($type == 'print') {

            if ($next_station == null || $next_station->station_name != 'SUB-PRINTER') {
                return ['error' => 'Batch not moved, next station not printer station',
                    'success' => $success,
                    'batch_number' => $batch->batch_number
                ];
            }
        } else if ($type == 'qc') {

            if ($batch->station->type != 'P') {
                return ['error' => 'Batch only move to QC station from Production station',
                    'success' => $success,
                    'batch_number' => $batch->batch_number
                ];
            }

            if ($next_station == null || $next_station->station_name != 'SUB-QC' || $next_station->type != 'Q') {
                return ['error' => 'Batch not moved, next station not SUB-QC station',
                    'success' => $success,
                    'batch_number' => $batch->batch_number
                ];
            }
        }

        if ($next_station && $next_station->id != '0') {
            $batch->prev_station_id = $batch->station_id;
            $batch->station_id = $next_station->id;
            $batch->save();
            $success = sprintf('Batch %s Successfully Moved to %s<br>', $batch->batch_number, $next_station->station_name);
        } else {
            $error = sprintf('Batch %s has no further stations on route <br>', $batch->batch_number);
        }

        return ['error' => $error, 'success' => $success, 'batch_number' => $batch->batch_number];
    }

    public function getBatchNumber($filename)
    {
// if (substr($filename, -4, 1) == '.') {
//   $filename = substr($filename, 0, -4);
// }

        $ex = explode('-', $filename);

        if (is_numeric($ex[0])) {
            return $ex[0];
        } elseif (isset($ex[1])) {
            return $ex[0] . '-' . $ex[1];
        } else {
            return null;
        }
    }

    private function recurseCopy($src, $dst, $rename = 0)
    {

        if (is_dir($src)) {

            $dir = opendir($src);

            mkdir($dst);

            while (false !== ($file = readdir($dir))) {

                if (($file != '.') && ($file != '..')) {

                    if (is_dir($src . '/' . $file)) {

                        $this->recurseCopy($src . '/' . $file, $dst . '/' . $file, $rename);

                    } else {

                        try {

                            if (substr($file, -4) == '.tmp' || substr($file, -3) == '.db') {
                                unlink($src . '/' . $file);
                                continue;
                            }

                            if ($rename) {
                                $new_file = $this->uniqueFileName($dst, $file, null, 1);
                            } else {
                                $new_file = $file;
                            }

                            copy($src . '/' . $file, $dst . '/' . $new_file);

                        } catch (Exception $e) {
                            Log::error('recurseCopy: Cannot copy file ' . $dir . ' - ' . $e->getMessage());
                        }
                    }
                }
            }
            closedir($dir);
        } else {

            if ($rename) {
                if (strrpos($dst, '/')) {
                    $file = substr($dst, strrpos($dst, '/') + 1);
                    $dir = substr($dst, 0, strlen($dst) - strlen($file));
                } else {
                    $file = $dst;
                    $dir = '/';
                }

                $new_file = $this->uniqueFileName($dir, $file, null, 1);

            } else {
                $new_file = $dst;
                $dir = '';
            }

            try {
                copy($src, $dir . $new_file);
            } catch (Exception $e) {
                Log::error('recurseCopy: Cannot copy file ' . $dir . ' - ' . $e->getMessage());
            }
        }
    }

    private function recurseAppend($src, $dst)
    {
// $src = /media/graphics/MAIN/6
// $dst = /var/www/devoms/public_html/media/graphics/archive/6
        $start = microtime(true);
        if (is_dir($src)) {
            $dir = opendir($src);
            if (!is_dir($dst)) {
                mkdir($dst);
            }
            while (false !== ($file = readdir($dir))) {
                if (($file != '.') && ($file != '..')) {
                    if (is_dir($src . '/' . $file)) {
                        $this->recurseAppend($src . '/' . $file, $dst . '/' . $file);
                    } else {
                        try {
                            copy($src . '/' . $file, $dst . '/' . $file);
                        } catch (Exception $e) {
                            Log::error('recurseAppend: Cannot copy file ' . $dir . ' - ' . $e->getMessage());
                        }
                    }
                }
            }
            closedir($dir);

        } else {
            if (file_exists($dst)) {
                $file = substr($dst, strrpos($dst, '/') + 1);
                $dir = substr($dst, 0, strlen($file));

                $file = $this->uniqueFilename($dir, $file);
            } else {
                $file = $dst;
            }
            try {
                copy($src, $file);
            } catch (Exception $e) {
                Log::error('recurseAppend: Cannot copy file ' . $src . ' - ' . $e->getMessage());
            }
        }

    }

    public static function removeFile($path)
    {

        if (!file_exists($path)) {
            return true;
        }

        if (!is_dir($path)) {
            try {
                return unlink($path);
            } catch (Exception $e) {
                Log::error('Graphics removeFile: cannot remove directory ' . $path);
                return false;
            }
        } else {

            if (substr($path, strlen($path) - 1, 1) != '/') {
                $path .= '/';
            }

            $files = glob($path . '*', GLOB_MARK);
            foreach ($files as $file) {
                if (is_dir($file)) {
                    self::removeFile($file);
                } else {
                    try {
                        unlink($file);
                    } catch (Exception $e) {
                        Log::error('Graphics removeFile: cannot remove file ' . $file);
                        return false;
                    }
                }
            }
            return rmdir($path);
        }

    }

    private function uniqueFilename($dir, $filename, $suffix = '-COPY', $batch_rename = 0)
    {

        if (strpos($filename, '.')) {
            $file = substr($filename, 0, strpos($filename, '.'));
            $ext = substr($filename, strpos($filename, '.'));
        } else {
            $file = $filename;
            $ext = '';
        }

        if ($batch_rename) {
            $batch = $this->getBatchNumber($file);
            if ($batch) {
                $file = $batch;
            }
        } else if (strpos($filename, $suffix)) {
            $file = substr($filename, 0, strpos($filename, $suffix));
        }

        try {
            $num = count(glob($dir . $file . '*'));

            if ($num > 0) {

                return $file . $suffix . '-' . $num . $ext;

            } else {

                return $file . $ext;
            }

        } catch (Exception $e) {
            Log::error('uniqueFilename: Error creating name ' . $filename . ' - ' . $e->getMessage());
            return false;
        }
    }

    private function findErrorFiles()
    {

        $error_list = array_diff(scandir($this->error_dir), array('..', '.'));

        $error_files = array();

        foreach ($error_list as $dir) {

            $batch_number = $this->getBatchNumber($dir);

            $error_files[$batch_number][] = $this->error_dir . $dir;

            $batch = Batch::where('batch_number', $batch_number)->first();

            if (count($batch) > 0 && $batch->graphic_found != 'Pendant Error') {

                $batch->graphic_found = '4';
                $batch->save();

                Batch::note($batch->batch_number, $batch->station_id, '6', 'Graphics Error File Found');
            }
        }

        $this->removeSure3d(array_keys($error_files));

        return $error_files;
    }

    public function sortFiles()
    {
        // "/var/www/oms/storage/autobatch.lock"
        if (file_exists(storage_path() . '/autobatch.lock')) {
            die('Auto Batch is lock');
            return redirect()->back()->withErrors('sortFiles Auto Batch is lock');
        }

        if (!file_exists($this->sort_root)) {
            Log::error('Sortfiles: Cannot find graphics directory on P: drive');
            return;
        }




        Batching::lock();

        $error_files = $this->findErrorFiles();

        $manual_files = $this->getManual('list');

        $dir_list = array_diff(scandir($this->main_dir), array('..', '.', 'lock'));

        $id = rand(1000, 9999);

        if (count($dir_list) > 0) {
            Log::info('STARTING SORTFILES ' . $id);
        }else{
            Log::info('Nothing to SORT');
        }

//        array:5 [  2 => "11",  3 => "13",  4 => "15",  5 => "9" ]
        foreach ($dir_list as $dir) {

            $result = shell_exec("lsof | grep " . $this->main_dir . $dir . "*");

            if ($result != null || substr($dir, -5) == '.lock' || !file_exists($this->main_dir . $dir) ||
                !is_writable($this->main_dir . $dir) || file_exists($this->main_dir . $dir . "/lock") ||
                file_exists($this->main_dir . $dir . '.lock')) {
//                Log::info('sortFiles can not sort');
                continue;
            }

            touch($this->main_dir . $dir . '.lock');

            Log::info('sortFiles ' . $id . ': ' . $dir);

            $batch_number = $this->getBatchNumber($dir);

            if (isset($error_files[$batch_number])) {
                foreach ($error_files[$batch_number] as $error_file) {
                    Log::info('sortFiles ' . $id . ':  Remove Error File ' . $error_file);
                    if (!$this->removeFile($error_file)) {
                        unlink($this->main_dir . $dir . '.lock');
                        continue;
                    }
                }
            }

            if (isset($manual_files[$batch_number])) {
                Log::info('sortFiles ' . $id . ':  Remove Manual File ' . $manual_files[$batch_number]);
                if (!$this->removeFile($manual_files[$batch_number])) {
                    unlink($this->main_dir . $dir . '.lock');
                    continue;
                }
            }

            $batch = Batch::with('route')
                ->where('batch_number', $batch_number)
                ->first();

            try {

                $to_file = $this->uniqueFilename('/var/www/'.Sure3d::getEnv().'/public_html/media/graphics/archive/', $dir);

                if (file_exists('/var/www/'.Sure3d::getEnv().'/public_html/media/graphics/archive/' . $to_file)) {
                    Log::error('sortFiles ' . $id . ': File already in archive ' . '/var/www/'.Sure3d::getEnv().'/public_html/media/graphics/archive/' . $to_file);
                }

## Moved batch dir with files from /media/graphics/MAIN/ to /var/www/devoms/public_html/media/graphics/archive/
                $this->recurseAppend($this->main_dir . $dir, '/var/www/'.Sure3d::getEnv().'/public_html/media/graphics/archive/' . $to_file);

                if ($batch) {
                    $batch->archived = '1'; // Found
                    $batch->save();
                    Batch::note($batch->batch_number, $batch->station_id, '6', 'Graphics Archived');
                    $this->createSummary($batch->batch_number);
                }
            } catch (Exception $e) {
                Log::error('sortFiles ' . $id . ': Error archiving file ' . $to_file . ' - ' . $e->getMessage());
                if ($batch) {
                    $batch->archived = '2'; // File move error
                    $batch->save();
                    Batch::note($batch->batch_number, $batch->station_id, '6', 'Graphics Archiving Error');
                }
                continue;
            }

            if ($batch && $batch->route) {
                $graphic_dir = $batch->route->graphic_dir . '/';
            } else {
                $graphic_dir = 'MISC/';
                Log::error('sortFiles ' . $id . ':  Graphic Directory Not Set ' . $dir);
                if ($batch) {
                    Batch::note($batch->batch_number, $batch->station_id, '6', 'Graphic Directory Not Set');
                }
            }


            if (!file_exists($this->sort_root . $graphic_dir)) {
                mkdir($this->sort_root . $graphic_dir);
            }

            $result = shell_exec("lsof | grep " . $this->sort_root . $graphic_dir . $dir . "*");

            if ($result != null) {
                Log::info('sortFiles ' . $id . ':  Destination File in use ' . $graphic_dir . $dir);
                if ($batch) {
                    Batch::note($batch->batch_number, $batch->station_id, '6', 'Error moving file to graphic directory - Destination File in use');
                }
                continue;
            }

            try {

//$this->removeFile($this->sort_root . $graphic_dir . $dir);
//$moved = @rename($this->main_dir . $dir, $this->sort_root . $graphic_dir . $dir);

                if ($graphic_dir != '/') {
                    $this->recurseAppend($this->main_dir . $dir, $this->sort_root . $graphic_dir . $dir);
                }
                $this->removeFile($this->main_dir . $dir);

                if ($batch) {
                    $batch->graphic_found = '1';
                    $batch->to_printer = '0';
                    $batch->save();

                    Batch::note($batch->batch_number, $batch->station_id, '6', "Graphic $dir moved to $graphic_dir directory");
                }

            } catch (Exception $e) {
                Log::error('sortFiles ' . $id . ':  Error moving file to graphic directory ' . $dir . ' ' . $e->getMessage());

                if ($batch) {
                    $batch->graphic_found = '2';
                    $batch->save();

                    Batch::note($batch->batch_number, $batch->station_id, '6', 'Error moving file to graphic directory');
                }
                continue;
            }

            if ($batch) {

                try {
                    $msg = $this->moveNext($batch, 'graphics');

                    if ($msg['error'] != '') {
                        Log::info('sortFiles ' . $id . ': ' . $msg['error'] . ' - ' . $dir);
                        Batch::note($batch->batch_number, $batch->station_id, '6', 'Graphics Sort - ' . $msg['error']);
                    }

                } catch (Exception $e) {
                    Log::error('sortFiles ' . $id . ': Error moving batch ' . $dir . ' - ' . $e->getMessage());
                    Batch::note($batch->batch_number, $batch->station_id, '6', 'Exception moving Batch - Graphics Sort');
                }
            } else {
                Log::error('sortFiles ' . $id . ': Batch not found ' . $dir);
            }

            try {
                unlink($this->main_dir . $dir . '.lock');
            } catch (Exception $e) {
                Log::error('sortFiles ' . $id . ': Lock could not be unlinked ' . $dir);
            }
        }

        if (count($dir_list) > 0) {
            Log::info('ENDING SORTFILES ' . $id);
        }

        Design::updateGraphicInfo();

// FileHelper::removeEmptySubFolders($this->sub_dir);
        Batching::unlock();
        return;
    }

    public function cleanUp()
    {
        $directories = BatchRoute::groupBy('graphic_dir')->get()->pluck('graphic_dir');

        foreach ($directories as $directory) {
            $file_list = null;

            if ($directory != '') {
                $file_list = array_diff(scandir($this->sort_root . $directory), array('..', '.'));

                foreach ($file_list as $file) {
                    $batch_numbers[$this->getBatchNumber($file)] = $file;
                }

                $batches = Batch::whereIn('batch_number', $batch_numbers)->get();

                foreach ($batches as $batch) {
                    if ($batch && ($batch->status == 'complete' || $batch->status == 'empty')) {
                        $this->removeFile($this->sort_root . $directory . $file);
                    }
                }
            }
        }
    }

    public function reprintGraphic(Request $request)
    {

        if (!$request->has('name')) {
            Log::error('reprintGraphic: Name not Set');
            return 'ERROR name not Set';
        }

        $result = $this->reprint($request->get('name'), $request->get('directory'));

        if ($result != 'success' || !$request->has('goto')) {
            return $result;
        } else {
            return redirect()->action('GraphicsController@showSublimation', ['select_batch' => $request->get('name')]);
        }
    }

    public function reprint($name, $directory = null)
    {

        if (!file_exists($this->sort_root)) {
            return 'ERROR Cannot find graphics directory on P: drive';
        }

        $name = trim($name);

        $filename = $this->getArchiveGraphic($name);

        $batch_number = $this->getBatchNumber($name);

        $batch = Batch::with('route.stations_list')
            ->where('batch_number', $batch_number)
            ->first();

        if ($batch) {
            Batch::note($batch->batch_number, $batch->station_id, '10', 'Graphic retrieved from archive and sent to graphic directory');
            $batch->to_printer = '0';
            $batch->save();
        }

        if ($directory != '') {
            try {
                Log::info('Reprint: ' . $this->sort_root . $directory . substr($filename, strrpos($filename, '/')));
                $this->recurseCopy($filename, $this->sort_root . $directory . '/' . substr($filename, strrpos($filename, '/')));
                return 'success';
            } catch (Exception $e) {
                Log::error('reprintGraphic: Could not copy File from Archive - ' . $e->getMessage());
                return 'ERROR not copied from Archive';
            }
        } else {
            $batch->station_id = $batch->route->stations_list->first()->station_id;
            $batch->save();
            $this->moveNext($batch, 'graphics');
            return 'success';
        }
    }

    public function reprintBulk(Request $request)
    {

        set_time_limit(0);

        $batch_numbers = $request->get('batch_number');

        $success = array();
        $error = array();

        if (is_array($batch_numbers)) {

            foreach ($batch_numbers as $batch_number) {

                $msg = $this->reprint($batch_number, $request->get('directory'));

                if ($msg == 'success') {
                    $success[] = $batch_number . ' Graphic in Print Sublimation';
                }

                if (substr($msg, 0, 5) == 'ERROR') {
                    $error[] = $batch_number . ' - ' . $msg;
                }
            }

            return redirect()->back()
                ->with('success', $success)
                ->withErrors($error);

        } else {
            return redirect()->back()->withErrors('No Batches Selected**');
        }

    }

    private function getArchiveGraphic($name)
    {

//                    /var/www/devoms/public_html/media/graphics/archive/
        $list = glob('/var/www/'.Sure3d::getEnv().'/public_html/media/graphics/archive/' . $name . "*");
        $files = array();

        if (count($list) < 1) {
            Log::error('reprintGraphic: File not found in Archive ' . $name);
            return 'ERROR not found in Archive';
        }

        foreach ($list as $file) {
            $files[filemtime($file)] = $file;
        }

        ksort($files);

        return array_pop($files);

    }

    public function resizeSure3d($item_id)
    {

//get item
        $item = Item::with('order')->find($item_id);

        if (!$item) {
            return 'Item not Found';
        }

//find sure3d file
        $filename = $this->sort_root . 'Sure3d/' . $item->order->short_order . '-' . $item->id . '.eps';
        if (!file_exists($filename)) {
            return 'Sure3d File not found';
        }

//load into imagick & resize
        set_time_limit(0);
        $image = new \Imagick($filename);
        $geo = $image->getImageGeometry();

        $sizex = $geo['width'] * 1.15;
        $sizey = $geo['height'] * 1.15;

        $image->scaleImage($sizex, $sizey);

//save to sublimation directory
        $fp = $this->sort_root . 'sublimation/' . $item->batch_number . '-' . $item->id . '.eps';
        $image->writeImage($fp);

        return 'done';
    }

    public function resizeBatch($batch_id)
    {

//find graphic file in archive
        $name = trim($batch_id);

        $list = glob('/var/www/'.Sure3d::getEnv().'/public_html/media/graphics/archive/' . $name . "*");
        $files = array();

        if (count($list) < 1) {
            return 'ERROR not found in Archive';
        }

        foreach ($list as $file) {
            $files[filemtime($file)] = $file;
        }

        ksort($files);

        $file = array_pop($files);

        if (is_dir($file)) {
            $filelist = glob($file . "/*");
        } else {
            $filelist = [$file];
        }

        $count = 0;

        foreach ($filelist as $filename) {
//load into imagick & resize
            if (substr($filename, -3) != 'eps') {
                continue;
            }
            set_time_limit(0);
            $image = new \Imagick($filename);
            $geo = $image->getImageGeometry();

            $sizex = $geo['width'] * 1.15;
            $sizey = $geo['height'] * 1.15;

            $image->scaleImage($sizex, $sizey);

//save to sublimation directory
            $fp = $this->sort_root . 'sublimation/' . $batch_id . '-' . $count . '.eps';
            $image->writeImage($fp);

            $count++;
        }
        return 'done';
    }

// Created PDF BATCH Summary
    private function createSummary($batch_number)
    {

        if (auth()->user()) {
//            $url = url("/graphics/sub_summary/" . $batch_number);
            $url = "http://".Sure3d::getEnv().".monogramonline.us/graphics/sub_summary/" . $batch_number;
        } else {
//            $url = url("/graphics/sub_screenshot/" . $batch_number);
            $url = "http://".Sure3d::getEnv().".monogramonline.us/graphics/sub_screenshot/" . $batch_number;
        }

        if (!file_exists($this->sort_root . 'summaries/')) {
            mkdir($this->sort_root . 'summaries/');
        }

        $file = $this->sort_root . 'summaries/' . $batch_number . '.pdf';

//      ######### Remove this later ##########
//      if(fileExists($file)){
//          unlink($file);
//      }
//        ######### Remove this later ##########
        set_time_limit(0);


        $count = 1;
        do {
// url = url('/')./graphics/sub_summary/7
// $file = /media/graphics/summaries/6.pdf
// Log::debug('printSubFile createSummary ' . print_r('xvfb-run --server-args="-screen 0, 1024x768x24" wkhtmltopdf ' . $url . ' ' . $file, true));
            $x = shell_exec('xvfb-run --server-args="-screen 0, 1024x768x24" wkhtmltopdf ' . $url . ' ' . $file);
            $count++;
        } while (!file_exists($file) && $count < 3);

        $count = 1;
        do {
            $y = shell_exec("pdfcrop $file $file");
            $count++;
        } while (!strpos($y, 'page written') && $count < 3);

        if (!file_exists($file)) {
            Log::error('Error creating sublimation summary for batch ' . $batch_number);
            Log::error($x);
            Log::error($y);
            return false;
        }

        return $file;
    }

    public function deleteFile($graphic_dir, $file)
    {
        $removed = $this->removeFile($this->sort_root . $graphic_dir . '/' . $file);
        if ($removed) {
            return redirect()->back()->withSuccess($file . ' Removed from ' . $graphic_dir);
        } else {
            return redirect()->back()->withErrors('Error Removing ' . $file . ' from ' . $graphic_dir);
        }
    }

    public function downloadSure3d()
    {
        try {
            $s = new Sure3d;
            $s->download();
        } catch (Exception $e) {
            Log::error('downloadSure3d: ' . $e->getMessage());
        }

        return;
    }

    public function viewGraphic(Request $request)
    {

        $batch_number = $request->get('batch_number');

        $batch = Batch::where('batch_number', $batch_number)->first();

        if (!$batch) {
            Log::error('viewGraphic: Batch not found ' . $batch_number);
            return redirect()->back()->withErrors('ERROR Batch not found');
        }

        if ($batch->section_id == 6 || $batch->section_id == 15 || $batch->section_id == 18) {
            $flop = 1;
        } else if ($batch->section_id == 3 || $batch->section_id == 10) {
            $flop = 0;
        } else {
            return redirect()->back()->withErrors('ERROR Graphics format cannot be rendered');
        }

        $file = $this->getArchiveGraphic($batch_number);

        if (is_dir($file)) {
            $graphic_path = $file . '/';
            $file_names = array_diff(scandir($file), array('..', '.'));
        } else {
            $graphic_path = '';
            $file_names[] = $file;
        }

        $thumb_path = base_path() . '/public_html/assets/images/graphics/';

        foreach ($file_names as $file_name) {

            $name = substr($file_name, 0, strpos($file_name, '.'));

            try {
                $graphic_path = "http://www.pws-services.net/media/catalog/product/cache/0/image/265x/9df78eab33525d08d6e5fb8d27136e95/p/r/product_3.png";
                ImageHelper::createThumb($graphic_path . $file_name, $flop, $thumb_path . $name . '.jpg', 750);
            } catch (Exception $e) {
                Log::error('viewgraphic: ' . $e->getMessage());
                return view('graphics.view_graphic', compact('batch_number', 'file', 'file_names', 'files'))
                    ->withErrors('Could not create image: ' . $e->getMessage());
            }
        }

        return view('graphics.view_graphic', compact('file_names', 'files', 'file', 'batch_number'));
    }

    public function uploadFile(Request $request)
    {
        if (!$request->has('batch_number')) {
            return redirect()->back()->withInput()->withErrors('Batch number required');
        }

        $batch = Batch::where('batch_number', $request->get('batch_number'))->first();

        if (!$batch) {
            return redirect()->back()->withInput()->withErrors('Batch not found');
        }

        $filename = $batch->batch_number . '.' . $request->file('upload_file')->getClientOriginalExtension();

        $filename = $this->uniqueFilename($this->main_dir, $filename);

        if (move_uploaded_file($request->file('upload_file'), $this->main_dir . $filename)) {
            Batch::note($batch->batch_number, $batch->station_id, '11', 'Graphic Uploaded to Main');
            return redirect()->back()->withInput()->withSuccess('Graphic Uploaded for Batch ' . $batch->batch_number);
        }

        Batch::note($batch->batch_number, $batch->station_id, '11', 'Error uploading graphic');
        return redirect()->back()->withInput()->withErrors('Error uploading graphic for Batch ' . $batch->batch_number);

    }

    public static function jobLocked($fileNameWithPath)
    {
        // "/var/www/oms/storage/autobatch.lock"
        if (file_exists($fileNameWithPath)) {
            return true;
        }
        return false;
    }

    public static function jobLock($fileNameWithPath)
    {
        try {
            touch($fileNameWithPath);
        } catch (Exception $e) {
            Log::error('jobLock: ' . $e->getMessage());
            return false;
        }
    }

    public static function jobUnlock($fileNameWithPath)
    {
        try {
            if (file_exists($fileNameWithPath)) {
                unlink($fileNameWithPath);
                return true;
            }
            return false;

        } catch (Exception $e) {
            Log::error('jobUnlock: ' . $e->getMessage());
            return false;
        }
    }

    public function getAutoDate($fileName){
        try {
            $fp = fopen($fileName, 'r');
            $line = fgets($fp);
            fclose($fp);
            return $line;
        } catch (Exception $e) {
            Log::error('getAutoDate: ' . $e->getMessage());
            return false;
        }
    }

    public function writeAutoDate($fileName, $fromDateAutoVal){
        try {
            $fp = fopen($fileName, 'w');
            fwrite($fp, $fromDateAutoVal);
            fclose($fp);
        } catch (Exception $e) {
            Log::error('writeAutoDate: ' . $e->getMessage());
            return false;
        }
    }

    public function postJobLock()
    {
        try {
            $file_pointer = $this->sort_root . $_GET['fileNameWithOutPath'];
            $flag = $_GET['flag'];

            if ($flag == "1") {
                Log::info("Job Created in: " . $file_pointer);
                self::jobLock($file_pointer);

                if("autoPrintHard" == $_GET['fileNameWithOutPath']){
                    self::jobLock($this->sort_root."autoPrint");
                }
            }

            if ($flag == "0") {
                Log::info("Job Deleted: " . $file_pointer);
                self::jobUnlock($file_pointer);
                if("autoPrintHard" == $_GET['fileNameWithOutPath']){
                    self::jobUnlock($this->sort_root."autoPrint");
                }
            }

            if ($flag == "from_date_auto") {
                $fileNameWithPath = $this->sort_root . "fromDate";
                $fromDateAutoVal = $_GET['fromDateAutoVal'];
                $this->writeAutoDate($fileNameWithPath, $fromDateAutoVal);
            }

            if ($flag == "to_date_auto") {
                $fileNameWithPath = $this->sort_root . "toDate";
                $fromDateAutoVal = $_GET['toDateAutoVal'];
                $this->writeAutoDate($fileNameWithPath, $fromDateAutoVal);
            }

        } catch (Exception $e) {
            Log::error('postJobLock : ' . $e->getMessage());
        }
    }

}

