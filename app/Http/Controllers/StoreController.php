<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCreateRequest;
use App\Item;
use App\Order;
use App\Ship;
use App\Store;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Market\Quickbooks;
use Monogram\Batching;
use Monogram\GXSConnection;

class StoreController extends Controller
{

    public static function retrieveData()
    {

        $gxs = new GXSConnection;
        $gxs->getFiles();

        $stores = Store::where('is_deleted', '0')
            ->orderBy('sort_order')
            ->get();

        foreach ($stores as $store) {

            if ($store->input == '1') {
                //load class dynamically
                try {
                    $className = 'Market\\' . $store->class_name;
                    $controller = new $className;
                    $controller->getInput($store->store_id);

                    if ($store->batch == '2') {
                        Batching::auto(0, $store->store_id, null);
                    }
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                }

            }
        }

        $gxs->sendFiles();
    }


    public static function backOrderNotify($storeID, $item_ids)
    {

        //

    }

    public function import(Request $request)
    {
        $orders = [];
        $errors = null;

        if ($request->has('store_id')) {
            $store = Store::where('store_id', $request->get('store_id'))->first();

            if ($store->input == '3') {
                $className = 'Market\\' . $store->class_name;
                $controller = new $className;
                $result = $controller->importCsv($store, $request->file('import'));
                $errors = $result['errors'];
                $orders = Order::with('items', 'customer', 'store')
                    ->whereIn('id', $result['order_ids'])
                    ->get();

                if ($store->batch == '2') {
                    $store_ids = array_unique($orders->pluck('store_id')->toArray());

                    foreach ($store_ids as $store_imported) {
                        Batching::auto(0, $store_imported);
                    }
                }
            }

        } else if ($request->has('download_file')) {

            $pathToFile = storage_path() . '/EDI/download/' . $request->get('download_file');

            if (file_exists($pathToFile)) {
                return response()->download($pathToFile)->deleteFileAfterSend(true);
            } else {
                $errors[] = $request->get('download_file') . ' Not Found';
            }
        }

        $downloads = array_diff(scandir(storage_path() . '/EDI/download'), array('..', '.'));

        $import_stores = Store::where('is_deleted', '0')
            ->where('input', '3')
            ->orderBy('sort_order')
            ->get()
            ->pluck('store_name', 'store_id');

        // return redirect()->action('OrderController@getList')->withErrors($errors);
        return view('stores.import', compact('import_stores', 'orders', 'downloads'))->withErrors($errors);
    }

    public function exportSummary(Request $request)
    {

        $qb_summary = Ship::join('stores', 'shipping.store_id', '=', 'stores.store_id')
            ->selectRaw('stores.store_id, stores.store_name, COUNT(*) as count')
            ->whereNull('shipping.qb_export')
            ->where('stores.qb_export', '1')
            ->where('shipping.is_deleted', '0')
            ->groupBy('store_id')
            ->get();

        $csv_summary = Ship::join('stores', 'shipping.store_id', '=', 'stores.store_id')
            ->selectRaw('shipping.store_id, stores.store_name, COUNT(*) as count')
            ->whereNull('shipping.csv_export')
            ->where('stores.ship', '4')
            ->where('shipping.is_deleted', '0')
            ->groupBy('store_id')
            ->get();

        $stores = Store::where('is_deleted', '0')
            ->where('qb_export', '1')
            ->where('invisible', '0')
            ->orderBy('sort_order')
            ->get()
            ->pluck('store_name', 'store_id');

        return view('stores.export_summary', compact('qb_summary', 'csv_summary', 'stores'));
    }

    public function exportDetails(Request $request)
    {
        $store = Store::where('store_id', $request->get('store_id'))->first();
        $type = $request->get('type');

        if ($type == 'qb') {

            $title = 'QuickBooks';

            if ($store->qb_export != '1') {
                return redirect()->back()->withErrors($store->store_name . ' is not configured for Quickbooks Export');
            }

            $field = 'qb_export';

        } else if ($type == 'csv') {

            $title = 'Shipment';

            if ($store->ship != '4') {
                return redirect()->back()->withErrors($store->store_name . ' is not configured for CSV Export');
            }

            $field = 'csv_export';
        }

        $details = Ship::with('order')
            ->where('store_id', $request->get('store_id'))
            ->whereNull($field)
            ->where('is_deleted', '0')
            ->get();

        return view('stores.export_details', compact('title', 'store', 'details', 'type'));
    }

    public function createExport(Request $request)
    {

        $shipments = Ship::with('items')
            ->whereIn('id', $request->get('ship_ids'))
            ->where('is_deleted', '0')
            ->get();

        if ($request->get('type') == 'qb') {

            $pathToFile = Quickbooks::export($shipments);

            Ship::whereIn('id', $request->get('ship_ids'))
                ->where('is_deleted', '0')
                ->update(['qb_export' => '1']);

        } else if ($request->get('type') == 'csv') {

            $store = Store::where('store_id', $request->get('store_id'))->first();
            $pathToFile = null;

            if ($store->ship == '4') {

                try {
                    $className = 'Market\\' . $store->class_name;
                    $controller = new $className;
                    $pathToFile = $controller->exportShipments($store->store_id, $shipments);

                    Ship::whereIn('id', $request->get('ship_ids'))
                        ->where('is_deleted', '0')
                        ->update(['csv_export' => '1']);

                } catch (Exception $e) {
                    Log::error($e->getMessage());
                }

            }
        }

        if ($pathToFile != null) {
            if (!is_array($pathToFile)) {
                return response()->download($pathToFile)->deleteFileAfterSend(false);
            } else {

            }
        } else {
            return redirect()->back()->withErrors('Error creating export file');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $stores = Store::with('store_items')
            ->where('is_deleted', '0')
            ->orderBy('sort_order')
            ->get();

        $companies = Store::$companies;

        return view('stores.index', compact('stores', 'companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $companies = Store::$companies;

        return view('stores.create', compact('input_options', 'companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(StoreCreateRequest $request)
    {
        $sort = Store::selectRaw('MAX(sort_order) as num')->first();

        $store = new Store;

        $new_id = strtolower($request->get('store_id'));
        $new_id = str_replace(' ', '', $new_id);
        $new_id = preg_replace('/[^\w]+/', '-', $new_id);

        $store->store_id = $new_id;

        $store->store_name = $request->get('store_name');
        $store->company = $request->get('company');
        $store->qb_export = $request->get('qb_export') ? $request->get('qb_export') : '0';
        $store->sort_order = $sort->num + 1;
        $store->class_name = $request->get('class_name');
        $store->email = $request->get('email');
        $store->input = $request->get('input');
        $store->change_items = $request->get('change_items') ? $request->get('change_items') : '0';
        $store->qc = $request->get('qc') ? $request->get('qc') : '0';
        $store->batch = $request->get('batch') ? $request->get('batch') : '0';
        $store->print = $request->get('print') ? $request->get('print') : '0';
        $store->confirm = $request->get('confirm') ? $request->get('confirm') : '0';
        $store->backorder = $request->get('backorder') ? $request->get('backorder') : '0';
        $store->ship_banner_url = $request->get('ship_banner_url') ? $request->get('ship_banner_url') : '';
        $store->ship_banner_image = $request->get('ship_banner_image') ? $request->get('ship_banner_image') : '';
        $store->ship = $request->get('ship') ? $request->get('ship') : '0';
        $store->validate_addresses = $request->get('validate_addresses') ? $request->get('validate_addresses') : '0';
        $store->change_method = $request->get('change_method') != null ? $request->get('change_method') : '1';
        $store->ship_label = $request->get('ship_label');
        $store->packing_list = $request->get('packing_list');
        $store->multi_carton = $request->get('multi_carton');

        $store->ups_type = $request->get('ups_type');
        $store->ups_account = $request->get('ups_account');

        $store->fedex_type = $request->get('fedex_type');
        $store->fedex_account = $request->get('fedex_account');
        $store->fedex_password = $request->get('fedex_password');
        $store->fedex_key = $request->get('fedex_key');
        $store->fedex_meter = $request->get('fedex_meter');

        $store->ship_name = $request->get('ship_name');
        $store->address_1 = $request->get('address1');
        $store->address_2 = $request->get('address2');
        $store->city = $request->get('city');
        $store->state = $request->get('state');
        $store->zip = $request->get('zip');
        $store->phone = $request->get('phone');

        $store->save();

        return redirect()->action('StoreController@index')
            ->with('success', $store->store_name . ' Created');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        // $store = Store::find($id);
        //
        // $input_options = Store::inputOptions();
        // $batch_options = Store::batchOptions();
        // $notify_options = Store::notifyOptions();
        //
        // return view('stores.show', compact('store', 'input_options', 'batch_options', 'notify_options'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $store = Store::find($id);

        $companies = Store::$companies;

        return view('stores.edit', compact('store', 'companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if (!$request->has('email')) {
            return redirect()->back()->withInput()->withErrors('Email Required');
        }
        $store = Store::find($id);

        $store->store_name = $request->get('store_name');
        $store->company = $request->get('company');
        $store->qb_export = $request->get('qb_export') ? $request->get('qb_export') : '0';
        $store->class_name = $request->get('class_name');
        $store->email = $request->get('email');
        $store->input = $request->get('input');
        $store->change_items = $request->get('change_items') ? $request->get('change_items') : '0';
        $store->batch = $request->get('batch') ? $request->get('batch') : '0';
        $store->print = $request->get('print') ? $request->get('print') : '0';
        $store->qc = $request->get('qc') ? $request->get('qc') : '0';
        $store->confirm = $request->get('confirm') ? $request->get('confirm') : '0';
        $store->backorder = $request->get('backorder') ? $request->get('backorder') : '0';
        $store->ship_banner_url = $request->get('ship_banner_url') ? $request->get('ship_banner_url') : '';
        $store->ship_banner_image = $request->get('ship_banner_image') ? $request->get('ship_banner_image') : '';
        $store->ship = $request->get('ship') ? $request->get('ship') : '0';
        $store->validate_addresses = $request->get('validate_addresses') ? $request->get('validate_addresses') : '0';
        $store->change_method = $request->get('change_method') != null ? $request->get('change_method') : '1';
        $store->ship_label = $request->get('ship_label');
        $store->packing_list = $request->get('packing_list');
        $store->multi_carton = $request->get('multi_carton');

        $store->ups_type = $request->get('ups_type');
        $store->ups_account = $request->get('ups_account');
        $store->fedex_type = $request->get('fedex_type');
        $store->fedex_account = $request->get('fedex_account');
        $store->fedex_password = $request->get('fedex_password');
        $store->fedex_key = $request->get('fedex_key');
        $store->fedex_meter = $request->get('fedex_meter');

        $store->ship_name = $request->get('ship_name');
        $store->address_1 = $request->get('address1');
        $store->address_2 = $request->get('address2');
        $store->city = $request->get('city');
        $store->state = $request->get('state');
        $store->zip = $request->get('zip');
        $store->phone = $request->get('phone');

        $store->save();

        return redirect()->action('StoreController@edit', ['id' => $id])
            ->with('success', $store->store_name . ' Updated');
    }

    public function sortOrder($direction, $id)
    {
        $store = Store::find($id);

        if (!$store) {
            Log::error('Store sort: Store not Found');
            return redirect()->action('StoreController@index')->withError('Store not Found');
        }

        if ($direction == 'up') {
            $new_order = $store->sort_order - 1;
        } else if ($direction == 'down') {
            $new_order = $store->sort_order + 1;
        } else {
            Log::error('Store sort: Direction not recognized');
            return redirect()->action('StoreController@index')->withError('Sort direction not recognized');
        }

        $switch = Store::where('sort_order', $new_order)->get();

        if (count($switch) > 1) {
            Log::error('Store sort: More than one store with same sort order');
            return redirect()->action('StoreController@index')->withError('Sort Order Error');
        }

        if (count($switch) == 1) {
            $switch->first()->sort_order = $store->sort_order;
            $switch->first()->save();
        }

        $store->sort_order = $new_order;
        $store->save();

        return redirect()->action('StoreController@index');
    }

    public function visible($id)
    {
        $store = Store::find($id);

        if (!$store) {
            Log::error('Store visible: Store not Found');
            return redirect()->action('StoreController@index')->withError('Store not Found');
        }

        if ($store->invisible == '0') {
            $store->invisible = '1';
        } else {
            $store->invisible = '0';
        }

        $store->save();

        return redirect()->action('StoreController@index');
    }

    public function destroy($id)
    {
        $store = Store::find($id);
        $store->is_deleted = '1';
        $store->save();

        return redirect()->action('StoreController@index');
    }

    public function qbExport(Request $request)
    {
        if (!$request->has('store_ids') || !$request->has('start_date') || !$request->has('end_date')) {
            return redirect()->back()->withInput()->withErrors('Stores and dates required to create Quickbooks export');
        }

        try {
            $shipments = Ship::with('items')
                ->whereIn('store_id', $request->get('store_ids'))
                ->where('transaction_datetime', '>=', $request->get('start_date') . ' 00:00:00')
                ->where('transaction_datetime', '<=', $request->get('end_date') . ' 23:59:59')
                ->where('is_deleted', '0')
                ->get();

            $pathToFile = Quickbooks::export($shipments);

            $ids = $shipments->pluck('id')->toArray();

            Ship::whereIn('id', $ids)->update(['qb_export' => '1']);

            if ($pathToFile != null) {
                return response()->download($pathToFile)->deleteFileAfterSend(false);
            }

        } catch (Exception $e) {
            Log::error('Error Creating qbExport - ' . $e->getMessage());
        }
    }


    public function qbCsvExport(Request $request)
    {

        if (!$request->has('store_ids') || !$request->has('start_date') || !$request->has('end_date')) {
            return redirect()->back()->withInput()->withErrors('Stores and dates required to create CSV export');
        }

        try {
            $shipments = Ship::  join('items', 'items.tracking_number', '=', 'shipping.tracking_number')
                ->join('orders', 'items.order_5p', '=', 'orders.id')
                ->whereIn('shipping.store_id', $request->get('store_ids'))
                ->where('shipping.transaction_datetime', '>=', $request->get('start_date') . ' 00:00:00')
                ->where('shipping.transaction_datetime', '<=', $request->get('end_date') . ' 23:59:59')
                ->where('shipping.is_deleted', '0')
//                ->limit(5)
//                ->selectRaw('sum(items.item_quantity) as sum, count(items.id) as count')
                ->get();
//                ->get(['item_code', 'item_quantity', 'item_unit_price', 'purchase_order', 'order_date','transaction_datetime']);


//                $shipments = Ship::  join('items', 'items.tracking_number', '=', 'shipping.tracking_number')
//                ->join('orders', 'items.order_5p', '=', 'orders.id')
////                ->whereIn('shipping.store_id', $request->get('store_ids'))
//                ->where('orders.order_date', '>=', $request->get('start_date') . ' 00:00:00')
//                ->where('orders.order_date', '<=', $request->get('end_date') . ' 23:59:59')
//                ->where('orders.is_deleted', '0')
//                ->limit(5)
//                ->get(['item_code', 'item_quantity', 'item_unit_price', 'purchase_order', 'order_date','transaction_datetime']);
//
////            $shipments = Item::with('order')
////            $shipments = Item::with('order')
//            $shipments = Item::with('order')
//            ->where('is_deleted', '0')
//                ->searchStore('524339241')
//                ->searchStatus('2')
//                ->searchSection($request->get('section'))
//                ->searchOrderDate($request->get('start_date'), $request->get('end_date'))
////                ->selectRaw('sum(items.item_quantity) as sum, count(items.id) as count')
////                ->limit(5)
////                ->pluck('id')
////                ->get(['items.item_code', 'items.item_quantity', 'items.item_unit_price', 'order.purchase_order', 'order.order_date','order.created_at']);
////                ->select('items.item_code', 'items.item_quantity','items.item_unit_price')
//            ->get();

            set_time_limit(0);
            $pathToFile = Quickbooks::csvExport($shipments);

            if ($pathToFile != null) {
                return response()->download($pathToFile)->deleteFileAfterSend(false);
            }

        } catch (Exception $e) {
            Log::error('Error Creating qbCsvExport - ' . $e->getMessage());
        }
    }
}
