<?php

namespace App\Http\Controllers;

use App\WasatchDashboard;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Monogram\SFTPConnection;
use Monogram\Wasatch;

class WasatchDashboardController extends Controller
{

    protected $staging = ['1' => '/wasatch/staging-1/',
        '2' => '/wasatch/staging-2/',
        '3' => '/wasatch/staging-3/',
        '4' => '/wasatch/staging-4/',
        '5' => '/wasatch/staging-5/',
        '6' => '/wasatch/staging-6/',
        '7' => '/wasatch/staging-7/',
        '8' => '/wasatch/staging-8/',
        '9' => '/wasatch/staging-9/'
    ];

    protected $wasatchDashboard;

    protected $graphicsController;

    public function __construct()
    {
        $this->wasatchDashboard = new WasatchDashboard;
        $this->graphicsController = new GraphicsController;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $w = new Wasatch;
        $queues = $w->getPrintAndRipQueues("getPrintAndRipQueues"); // Get both Rip and Print QUEUE


#-------------------------------------Check if Ripqueue EXISTS in Printqueue----------------------------------------------
        $jobsInPrint = array();
        $jobsInDatabase = "";# WasatchDashboard::get();

//        foreach ($jobsInDatabase as $results) {
            // Read from xml
//        $printerNumber = 1;
//            foreach ($queues as $printJobs) {
//                if (isset($printJobs["PRINTQUEUE"]["ITEM"])) {
//                    foreach ($printJobs["PRINTQUEUE"]["ITEM"] as $items) {
//
//                        ######## Count Number of batch in each Printer ###############
//                        if (isset($items["@attributes"]) && strstr($items["@attributes"]["notes"], '_Layout', true) && ($items["@attributes"]["layout"] == true)) {
//                            $batchArray = explode("_Layout", $items["@attributes"]["notes"]);
//                            if (isset($batchArray[0])) {
//                                $currentBatchNumbers[$printerNumber][] = $batchArray[0];
//                                Log::info('batchArray[0] # ' . $batchArray[0]);
//                            }
//                        }
//                        #######################
//
//                        if (isset($results)) {
//                            // if database batch_number is equal to Printqueue Notes number : 4940_Layout
//                            if (($results->batch_number == strstr($items["@attributes"]["notes"], '_0', true))) {
//                                WasatchDashboard::where('batch_number', '=', $results->batch_number)
//                                    ->whereRaw('found_jpg < jpg_qty')
//                                    ->increment('found_jpg', 1);
//                            }
//                            if (($results->batch_number == strstr($items["@attributes"]["notes"], '_1', true))) {
//                                WasatchDashboard::where('batch_number', '=', $results->batch_number)
//                                    ->whereRaw('found_jpg < jpg_qty')
//                                    ->increment('found_jpg', 1);
//                            }
//                            if (($results->batch_number == strstr($items["@attributes"]["notes"], '_pdf', true))) {
//
//                                WasatchDashboard::where('batch_number', '=', $results->batch_number)
//                                    ->whereRaw('found_pdf < pdf_qty')
//                                    ->increment('found_pdf', 1);
//
//                            }
//                        }
//                    }
//                }
//                $printerNumber++;
//            }
//        }
        $jpg = array();
        $pdf = array();
//        $jpg1 = WasatchDashboard::whereRaw('jpg_qty')->get('jpg_qty')->pluck('batch_number');
//        $pdf1 = WasatchDashboard::whereRaw('pdf_qty')->get()->pluck('batch_number');

        $jpg =  WasatchDashboard::get()->pluck('jpg_qty', 'batch_number');
        $pdf =  WasatchDashboard::get()->pluck('pdf_qty', 'batch_number');

//        foreach ($jpg1 as $r) {
//            array_push($jpg, $r);
//        }
//
//        foreach ($pdf1 as $t) {
//            array_push($pdf, $t);
//        }
#-------------------------------------Check if Ripqueue EXISTS in Printqueue----------------------------------------------

        ########################33
        $printQue = [];
        foreach($queues as $printQueue){

            $printer = substr($printQueue["@attributes"]["name"], 0, 10);

            if(isset($printQueue["PRINTQUEUE"]["ITEM"])){
                foreach($printQueue["PRINTQUEUE"]["ITEM"]  as $items){
                    $batchNo = strstr($items["@attributes"]["notes"], '_', true);
                    $index = $items["@attributes"]["index"];

                    if(isset($items["@attributes"]["copies"])) {
                        $printQue[$printer][$batchNo][$index]['copies'] = $items["@attributes"]["copies"];
                    }

                    if(isset($items["@attributes"]["index"])) {
                        $printQue[$printer][$batchNo][$index]['index'] = $items["@attributes"]["index"];
                    }

                    if(isset($items["@attributes"]["notes"])) {
                        $printQue[$printer][$batchNo][$index]['notes'] = $items["@attributes"]["notes"];
                    }

                    if(isset($items["@attributes"]["output"])) {
                        $printQue[$printer][$batchNo][$index]['output'] = $items["@attributes"]["output"];
                    }

                    if(isset($items["@attributes"]["sourcefile"])) {
                        $printQue[$printer][$batchNo][$index]['sourcefile'] = $items["@attributes"]["sourcefile"];
                    }

                }
                ksort($printQue[$printer][$batchNo]);
                ksort($printQue[$printer]);

            }
        }

        $jpgPdfCount =[];

        foreach ($printQue as $printer => $printQu) {
            foreach ($printQu as $batches => $indexs) {
                $jpgc = 1;
                foreach ($indexs as $key => $val) {

                    if (strpos($val['notes'], 'Layout') !== false) {
                        $jpgPdfCount[$batches]['Layout'] = 1;
                    }

                    if (strpos($val['notes'], 'jpg') !== false) {
                        $jpgPdfCount[$batches]['jpg'] = $jpgc;
                        $jpgc++;
                    }

                    if (strpos($val['notes'], 'pdf') !== false) {
                        $jpgPdfCount[$batches]['pdf'] = 1;
                    }
                }
            }
        }

        foreach ($jpgPdfCount as $batches => $indexs){

            if(isset($jpg[$batches])){
                $jpgPdfCount[$batches]['jpg_in_db'] = count(json_decode($jpg[$batches],true));
            }else{
                $jpgPdfCount[$batches]['jpg_in_db'] = 0;
            }

            if(isset($pdf[$batches])){
                $jpgPdfCount[$batches]['pdf_in_db'] = count(json_decode($pdf[$batches],true));
            }else{
                $jpgPdfCount[$batches]['pdf_in_db'] = 0;
            }

            if(isset($indexs['jpg']) && isset($jpg[$batches])){
                if((count(json_decode($jpg[$batches],true)) == $indexs['jpg'])){
                    $jpgPdfCount[$batches]['jpg_match'] = true;
                }else{
                    $jpgPdfCount[$batches]['jpg_match'] = false;
                }
            }else{
                $jpgPdfCount[$batches]['jpg_match'] = false;
            }

            if(isset($indexs['pdf']) && isset($pdf[$batches])){
                if((count(json_decode($pdf[$batches],true)) == $indexs['pdf'])){
                    $jpgPdfCount[$batches]['pdf_match'] = true;
                }else{
                    $jpgPdfCount[$batches]['pdf_match'] = false;
                }
            }else{
                $jpgPdfCount[$batches]['pdf_match'] = false;
            }
        }

        ########################33
        return view('wasatch_dashboard.index', compact('printQue', 'jpgPdfCount'));

    }


    /**
     * @param $queues
     * @return array
     */
    public function removeCompletePrintBatch($queues)
    {
        #--------Get Batch number's from Queues ---------
        $currentBatchNumbers = [];
        $allBatchWithIndex = [];
        $batchNumbersWithUnit = [];
        $printerNumber = 1;
        foreach ($queues as $printJobs) {

            if (isset($printJobs["PRINTQUEUE"]["ITEM"])) {
                foreach ($printJobs["PRINTQUEUE"]["ITEM"] as $items) {

                    if (isset($items["@attributes"]) && strstr($items["@attributes"]["notes"], '_Layout', true) && ($items["@attributes"]["layout"] == true)) {
                        $batchArray = explode("_Layout", $items["@attributes"]["notes"]);
                        if (isset($batchArray[0])) {

                            WasatchDashboard::where('batch_number', '=', $batchArray[0])
                                ->update(['output_copies' => $items["@attributes"]["output"]]);

                            $getUnit = explode(":", $printJobs["@attributes"]["name"]);
                            $getUnit = substr($getUnit[0], -1);

                            echo "<pre>";
                            echo $printJobs["@attributes"]["name"]." -- ".$getUnit." --- ".$items["@attributes"]["notes"]. "  ---->   " .$batchArray[0]." -------- output = ". $items["@attributes"]["output"];
                            echo "</pre>";

                            $currentBatchNumbers[$printerNumber][] = $batchArray[0];
                            $batchNumbersWithUnit[$batchArray[0]] = $getUnit;

//                            $currentBatchNumbers[$printerNumber]['batch'] = $batchArray[0];
                            $currentOutput[$printerNumber][$batchArray[0]] = $items["@attributes"]["output"];
                        }
                    }

                    if (isset($items["@attributes"])) {
                        $getBatch = explode("_", $items["@attributes"]["notes"]);
                        if (isset($getBatch[0])) {
                            if(substr($getBatch[0], 0, 1) == "R"){
                                $allBatchWithIndex[$getBatch[0]."_".$getBatch[1]][] = $items["@attributes"]["index"];
                            }elseif (substr($getBatch[0], 0, 1) == "X"){
                                $allBatchWithIndex[$getBatch[0]."_".$getBatch[1]][] = $items["@attributes"]["index"];
                            }else{
                                $allBatchWithIndex[$getBatch[0]][] = $items["@attributes"]["index"];
                            }
                        }
                    }
                }
            }
            $printerNumber++;
        }


        $jobsInDatabase = WasatchDashboard::get();
        $allActiveBatch = call_user_func_array('array_merge', $currentBatchNumbers);
        $allActiveOutput = [];
        foreach ($currentOutput as $printers) {
            foreach ($printers as $batch => $outPut) {
                if ($outPut == 1) {
                    $allActiveOutput[$batch] = $outPut;
                }

            }
        }
        $batchesInTabl = [];
        foreach ($jobsInDatabase as $results) {
            $batchesInTabl[] =  $this->bFix($results->batch_number);
        }
        $notExis = array_diff($batchesInTabl, $allActiveOutput);
////        foreach ($notExis as $bbbb){
////            $batch = str_replace("_","-",$bbbb);
////            WasatchDashboard::where('batch_number', $batch)->delete();
////        }
//
//        foreach ($allBatchWithIndex as $batch_number => $indexex){
//            $this->deleteFromWasath($batch_number,6, $batchNumbersWithUnit[$batch_number], $allBatchWithIndex[$batch_number]);
////                                    sleep(5);
//            $this->ftpUplodXml($batch_number,6, $batchNumbersWithUnit[$batch_number], $allBatchWithIndex[$batch_number]);
//        }
//
//
            foreach ($jobsInDatabase as $results) {
//                if($results->batch_number =="R01-43942") {
//                    echo "<pre>";
//                    echo "1. Batch### " . $results->batch_number;
//                    echo "</pre>";
//                }
                if (isset($results) && (count($allActiveBatch) > 0)) {

                    $currentDateTime = date("Y-m-d H:i:s");

                        ########## 2019-11-19 ###########
                        if(6 > ((strtotime($currentDateTime) - strtotime($results->created_at)) / 60) ){
//                            echo "<pre> batch_number =".$results->batch_number." Current Date_Time = ".$currentDateTime. " --- strtotime = ".strtotime($currentDateTime)." Diff = ".((strtotime($currentDateTime) - strtotime($results->created_at))/60)."</pre>";
                            continue;
                        }
                        ########## 2019-11-19 ###########


                        // Step 1 Move to Production Station.
                        if (substr($results->batch_number, 0, 4) == 'BATC') {
                            $batch_number = $this->graphicsController->getBatchNumber(substr($results->batch_number, 4));
                        } else {
                            $batch_number = $this->graphicsController->getBatchNumber($results->batch_number);
                        }

                        $batch_number = $this->bFix($batch_number);

                        if (empty($batch_number)) {
                            Log::info('Unable Batch moved by removeCompletePrintBatch: to move batch #' . $results->batch_number);
                        }else{

                            if (isset($allActiveOutput[$batch_number]) && $allActiveOutput[$batch_number] > 0) {
                                # echo "<pre> No Batch " . $results->batch_number . " not exist. key =   results->printer_number " . $results->printer_number . "</pre>";
                                // Step 1, Delete from Table by
                                WasatchDashboard::where('batch_number', $results->batch_number)->delete();

                                // Step 2
                                $result = $this->graphicsController->moveNext($batch_number, 'production');
                                Log::info('Auto : error = ' . $result['error'] . ' success = ' . $result['success'] . ' batch_number= ' . $result['batch_number'].' printer_number= EPSON-'.$results->printer_number);

                                // Step3 Delete from Wasah by put xml


                                if (isset($batchNumbersWithUnit[$batch_number])) {

                                    $this->deleteFromWasath($batch_number,$results->printer_number, $batchNumbersWithUnit[$batch_number], $allBatchWithIndex[$batch_number]);
//                                    sleep(5);
                                    $this->ftpUplodXml($batch_number,$results->printer_number, $batchNumbersWithUnit[$batch_number], $allBatchWithIndex[$batch_number]);
                                }

                            }else{

                                if(240 > ((strtotime($currentDateTime) - strtotime($results->created_at)) / 60) ){
                                    echo "<pre>";
                                    echo "Batch# ".$results->batch_number."   not exist in print_queqe = ".((strtotime($currentDateTime) - strtotime($results->created_at)) / 60);
                                    echo "</pre>";
//                                    WasatchDashboard::where('batch_number', $results->batch_number)->delete();
                                }
                            }
                        }
//                    }
                }
            }
        return $currentBatchNumbers;
        #--------Get Batch number's from Queues ---------
    }

    private function bFix($batch){

        $batch = str_replace("-","_",$batch);

        return $batch;
    }
    public function deleteFromWasath($batch_number,$printerNumber, $unit, $indexArray)
    {
        $type = "index";
        foreach ($indexArray as $index) {

            $xml = null;

            $xml = '<?xml version="1.0" encoding="utf-8"?><WASATCH ACTION="JOB">';

            $xml .= '<DELETE printunit=' . $unit . ' ' . $type . '="' . $index . '" />';

            $xml .= '</WASATCH>';

            #################
            try {
                // Write XML file in =  /var/www/oms/storage/wasatch/staging-1/4.xml
                $newfile = fopen(storage_path() . $this->staging[$printerNumber] . $index . '.xml', "w"); // $hotfolder= 1,  $this->staging[$hotfolder] = /wasatch/staging-1/
                Log::info("printJob XML file created at = " . storage_path() . $this->staging[$printerNumber] . $index . '.xml');
                fwrite($newfile, $xml);
                fclose($newfile);
            } catch (Exception $e) {
                Log::error('Unable printJob XML file created delete file  ' . $e->getMessage());
            }
            #################

//            $this->ftpUplodXml($index);
        }

    }

    public function ftpUplodXml($batch_number,$printerNumber, $unit, $indexArray)
    {

        #############20191011######################
        try {
            $host = "96.57.0.134";
            $sftp = new SFTPConnection($host, 22);
            $sftp->login('jewel', 'Dtw#123Dtw');
        } catch (Exception $e) {
            Log::error('Wasatch: SFTP connection error ' . $e->getMessage());
            die("SFTP connection error". $e->getMessage());
            return FALSE;
        }

        try {

            foreach ($indexArray as $index) {
                echo "<pre>";
                echo "Printer Number = ".$printerNumber." Unit# ".$unit."   Batch# ".$batch_number."   Index = ".$index."   Destination folder = ".$this->staging[$printerNumber];
                echo "</pre>";
//            $files = $sftp->uploadDirectory( "/home/jewel/test/" , "/wasatch/staging-1/");
                if (env("GRAPHICS_ENV") == "devgraphics") {
                    $files = $sftp->uploadFiles([$index . '.xml'], "/media/Wasatch/DEVEpson-1/", $this->staging[$printerNumber]);
                } else {
                    $files = $sftp->uploadFiles([$index . '.xml'], "/media/Wasatch/Epson-" . $printerNumber . "/", $this->staging[$printerNumber]);
                }
                sleep(1);
                unlink(storage_path() . $this->staging[$printerNumber] . $index . '.xml');
            }

        } catch (Exception $e) {
            Log::error('Wasatch: SFTP upload error ' . $e->getMessage());
            die("SFTP  upload error ". $e->getMessage());
        }

        #############20191011######################
    }
#-------------------------------------Save RipQueues in Database----------------------------------------------
    public function saveRipPrint(&$request)
    {
        foreach ($request as $r) {
            if (isset($r["RIPQUEUE"]["ITEM"])) {
                foreach ($r["RIPQUEUE"]["ITEM"] as $items) {
                    $user = WasatchDashboard::where('batch_number', strstr($items["@attributes"]["job"], '_', true))->first();

                    if (count(array($user)) < 1 || $user === null) {
                        $ripPrint = new WasatchDashboard();
                        $ripPrint->job = $items["@attributes"]["job"];
                        $ripPrint->rip = $items["@attributes"]["rip"];
                        $ripPrint->copies = $items["@attributes"]["copies"];
                        $ripPrint->sourcefile = $items["@attributes"]["sourcefile"];;
                        $ripPrint->submission_date = date('Y-m-d H:i:s', $items["@attributes"]["submission"]);
                        $ripPrint->printer_number = $r["@attributes"]["number"];

                        $ripPrint->save();
                    }
                }
            }
        }
    }

#-------------------------------------Save RipQueues in Database----------------------------------------------

    public function compare($batch_number, $copies, $jpg, $pdf, $total, $printer)
    {
        $wasatchDashboard = $this->wasatchDashboard->getBatchInfo($batch_number);

        if ($wasatchDashboard) {
            $wasatchDashboard->jpg_qty = $jpg;
            $wasatchDashboard->pdf_qty = $pdf;
            $wasatchDashboard->total_files = $total;
            $wasatchDashboard->copies = $copies;
            $wasatchDashboard->printer_number = $printer;
            $wasatchDashboard->save();
        } else {
            $wasatchDashboard = new WasatchDashboard();
            $wasatchDashboard->batch_number = $batch_number;
            $wasatchDashboard->jpg_qty = $jpg;
            $wasatchDashboard->pdf_qty = $pdf;
            $wasatchDashboard->total_files = $total;
            $wasatchDashboard->copies = $copies;
            $wasatchDashboard->printer_number = $printer;
            $wasatchDashboard->save();
        }
    }

    public function updateWasatch()
    {
        $w = new Wasatch;
        $queues = $w->getPrintAndRipQueues("getwasatch11status"); // Get both Rip and Print QUEUE

//        $url = 'http://order.monogramonline.com/getwasatchstatus';
//        $client = new \GuzzleHttp\Client();
//        $response = $client->request('GET', $url);
//        $response->getStatusCode(); # 200
//        $response->getHeaderLine('content-type');
//        $queues = json_decode($response->getBody(), true);


        $this->removeCompletePrintBatch($queues);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//
    }

    public function getWasatchFormatfileName($files){
        $i =0;
        $groupJgp =[];
        $groupPdf = [];
        foreach ($files as $name => $info) {
            $nameWithBatch = explode("/", $name);
            $countNamewithBatch = count($nameWithBatch);
            $info['type'] = str_replace(".", "", $info['type']);
            if($info['type'] == "pdf"){
                $getBatch = explode(".", $nameWithBatch[$countNamewithBatch-1]);
                $groupPdf[$nameWithBatch[$countNamewithBatch-1]] = $getBatch[0]."_".$info['type']."_000";
                continue;
            }

            $groupJgp[$nameWithBatch[$countNamewithBatch-1]] = $nameWithBatch[$countNamewithBatch-2]."_".$i."_".$info['type']."_000";
            $i++;
        }
        return ["jpg" => json_encode($groupJgp), "pdf" => json_encode($groupPdf)];
    }
}
