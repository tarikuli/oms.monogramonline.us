<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Customer;
use App\Item;
use App\Note;
use App\Notification;
use App\Option;
use App\Order;
use App\Parameter;
use App\Product;
use App\Store;
use App\Wap;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Monogram\CSV;
use Monogram\Helper;
use Ship\Shipper;

class OrderController extends Controller
{
    public function show($id)
    {
        Log::error('Order Show called for id ' . $id . ' - ' . URL::previous());
    }

    public function getList(Request $request)
    {
        if ($request->has('start_date')) {
            $start = $request->get('start_date');
        } else if (!$request->has('search_for_first') && !$request->has('search_for_second') &&
            !$request->has('store') && !$request->has('status')) {
            $start = date("Y-m-d");
        } else {
            $start = null;
        }

        if ($request->has('status') || $request->has('search_for_first') ||
            $request->has('search_for_second') || $request->has('store')) {
            $status = $request->get('status');
        } else {
            $status = 'not_cancelled';
        }

        $orders = Order::with('store', 'customer', 'items')
            ->where('is_deleted', '0')
            ->storeId($request->get('store'))
            ->status($status)
            ->searchShipping($request->get('shipping_method'))
            ->withinDate($start, $request->get('end_date'))
            ->search($request->get('search_for_first'), $request->get('operator_first'), $request->get('search_in_first'))
            ->search($request->get('search_for_second'), $request->get('operator_second'), $request->get('search_in_second'))
            //->groupBy('order_id')
            ->latest()
            ->paginate(50);

        $statuses = Order::statuses();

        $stores = Store::list('%', '%', 'none');

        $companies = Store::$companies;

        $total = Order::where('is_deleted', '0')
            ->storeId($request->get('store'))
            ->status($status)
            ->searchShipping($request->get('shipping_method'))
            ->withinDate($start, $request->get('end_date'))
            ->search($request->get('search_for_first'), $request->get('operator_first'), $request->get('search_in_first'))
            ->search($request->get('search_for_second'), $request->get('operator_second'), $request->get('search_in_second'))
            ->selectRaw('SUM(total) as money, SUM(shipping_charge) as shipping, SUM(tax_charge) as tax')
            ->first();

        $operators = ['in' => 'In',
            'not_in' => 'Not In',
            'starts_with' => 'Starts With',
            'ends_with' => 'Ends With',
            'equals' => 'Equals',
            'not_equals' => 'Not Equal',
            'less_than' => 'Less Than',
            'greater_than' => 'Greater Than',
            // 'blank' => 'Is Blank',
            // 'not_blank' => 'Is Not Blank'
        ];
        $shipping_methods = Shipper::listMethods();

        return view('orders.lists', compact('orders', 'stores', 'statuses', 'request', 'operators', 'total', 'companies', 'shipping_methods'))
            ->with('search_in', Order::$search_in);
    }

    public function csvExport(Request $request)
    {

        if (count($request->all()) > 0) {

            $header = [
                'id',
                'short_order',
                'purchase_order',
                'store_id',
                'order_date',
                'item_count',
                'sub_total',
                'coupon_id',
                'coupon_value',
                'promotion_id',
                'promotion_value',
                'gift_wrap_cost',
                'adjustments',
                'insurance',
                'tax_charge',
                'shipping_charge',
                'total',
                'order_ip',
                'bill_email',
                'bill_full_name',
                'ship_state',
            ];

            //ini_set('memory_limit','16M');
            set_time_limit(0);

            $offset = 0;
            $filename = sprintf("orders_%s.csv", date("Y_m_d_His", strtotime('now')));

            while ($offset < $request->get('count')) {

                $orders = Order::join('customers', 'orders.customer_id', '=', 'customers.id')
                    ->where('orders.is_deleted', '0')
                    ->storeId(unserialize($request->get('store')))
                    ->status(unserialize($request->get('status')))
                    ->searchShipping($request->get('shipping_method'))
                    ->withinDate($request->get('start_date'), $request->get('end_date'))
                    ->search($request->get('search_for_first'), $request->get('operator_first'), $request->get('search_in_first'))
                    ->search($request->get('search_for_second'), $request->get('operator_second'), $request->get('search_in_second'))
                    ->groupBy('orders.id')
                    ->latest('orders.created_at')
                    ->limit(5000)
                    ->offset($offset)
                    ->get([
                        'orders.id',
                        'orders.short_order',
                        'orders.purchase_order',
                        'orders.store_id',
                        'order_date',
                        'item_count',
                        'sub_total',
                        'coupon_id',
                        'coupon_value',
                        'promotion_id',
                        'promotion_value',
                        'gift_wrap_cost',
                        'adjustments',
                        'insurance',
                        'tax_charge',
                        'shipping_charge',
                        'total',
                        'order_ip',
                        'bill_email',
                        'bill_full_name',
                        'customers.ship_state',
                    ])->toArray();

                $csv = new CSV;
                $pathToFile = $csv->createFile($orders, 'assets/exports/', $header, $filename);

                $offset += 5000;
            }

            return response()->download($pathToFile)->deleteFileAfterSend(true);
        }
    }

    public function details($order_id)
    {
        $order = Order::with('customer', 'items.shipInfo', 'items.batch.station', 'items.product',
            'items.allChildSkus', 'items.parameter_option', 'notes.user')
            ->where('is_deleted', '0')
            ->find($order_id);

        if (!$order) {
            return view('errors.404');
        }

        $batched = $order->items->filter(function ($item) {
            return $item->batch_number != '0';
        })->count();

        $stores = Store::list();

        $statuses = Order::statuses();

        $status_selector = array();

        $status_selector[$order->order_status] = $statuses[$order->order_status];

        if ($order->order_status == 4) {
            if (!$batched) {
                $status_selector['Prevent Production'] = ['13' => 'Payment / Fraud Hold', '23' => 'Other Hold'];

                if ($order->ship_date != null) {
                    $status_selector['Prevent Shipping'] = ['7' => 'Shipping Hold', '12' => 'Hold until Ship Date'];
                } else {
                    $status_selector['Prevent Shipping'] = ['7' => 'Shipping Hold'];
                }
            } else if ($order->ship_date != null) {
                $status_selector['Prevent Shipping'] =
                    ['13' => 'Payment / Fraud Hold', '23' => 'Other Hold', '7' => 'Shipping Hold', '12' => 'Hold until Ship Date'];
            } else {
                $status_selector['Prevent Shipping'] = ['13' => 'Payment / Fraud Hold', '23' => 'Other Hold', '7' => 'Shipping Hold'];
            }

            $status_selector['8'] = 'Cancel Order';

            if (auth()->user()->accesses->where('page', 'orders_admin')->all()) {
                $status_selector['X'] = 'Delete Order';
            }

        } else if ($order->order_status == 6 || $order->order_status == 8) {
            // do nothing
        } else {
            $status_selector['4'] = 'Release Hold';
            $status_selector['8'] = 'Cancel Order';
        }

        $shipping_methods = Shipper::listMethods();

        return view('orders.details', compact('order', 'order_id', 'batched', 'statuses', 'status_selector', 'shipping_methods', 'stores'));
    }

    public function changeStatus (Request $request)
    {
        if ($request->get('current_status') == 6) {
            return redirect()->back()->withErrors('Cannot unship and order');
        }

        if ($request->get('new_status') == 'X') {
            $msg = $this->destroy($request->get('order'));
            return redirect()->action('OrderController@getList')->withSuccess($msg);
        }

        $order = Order::with('items')->where('id', $request->get('order'))->first();

        if ($request->get('new_status') == 8) {

            $shipped_items = false;

            foreach ($order->items as $item) {
                if ($item->item_status != 'shipped') {
                    if ($item->batch_number != '0') {

                        $batch_number = $item->batch_number;

                        $item->batch_number = '0';
                        $item->save();

                        Order::note("Item $item->id Cancelled and removed from Batch $batch_number", $order->id, $order->order_id);
                        Batch::note($batch_number, '', '4', "Item $item->id Cancelled and removed from Batch");
                        Batch::isFinished($batch_number);
                    }

                    if ($item->item_status == 'wap') {
                        Wap::removeItem($item->id, $item->order_5p);
                    }

                    $item->item_status = 6;
                    $item->save();

                } else {
                    $shipped_items = true;
                }
            }

            if ($shipped_items == false) {
                $order->order_status = 8;
                $order->save();
                Order::note('CS: Order Cancelled: ' . $request->get('status_note'), $order->id);
                $this->updateOrderStatusInMagento($order->order_id, "canceled", "canceled", 'CS: Order Cancelled: ' . $request->get('status_note'));
            } else {
                $order->order_status = 6;
                $order->save();
            }

        } else {

            $order->order_status = $request->get('new_status');
            $order->save();

            Order::note('OH: Reason - ' . $request->get('status_note'), $order->id);

        }

        return redirect()->action('OrderController@details', ['order_id' => $order->id]);
    }

    private function updateOrderStatusInMagento($shortOrderNumber, $orderState, $orderStatus, $statusNote)
    {

// jSON URL which should be requested
        $json_url = "http://www.pws-services.net/statusupdate";
        if (env("GRAPHICS_ENV") == "devgraphics") {
            $json_url = "http://magento.monogramonline.us/statusupdate";
        }

        $orderStatusData["apiUser"] = "collage";
        $orderStatusData["apiPass"] = "Collage99~!";
        $orderStatusData["orderId"] = $shortOrderNumber; #1216964;
        $orderStatusData["orderState"] = $orderState; #"canceled";
        $orderStatusData["orderStatus"] = $orderStatus; #"canceled";
        $orderStatusData["orderComment"] = $statusNote; #'Changing state to Processing and status to expedier Status';

        $queryUrl = http_build_query($orderStatusData, 'flags_');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $json_url);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $queryUrl);
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, FALSE);
        curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
// Submit the POST request
        $result = curl_exec($curl);
        curl_close($curl);
    }

    public function update(Request $request, $id)
    {

        if ($id == 'new') {
            $manual_order_count = Order::where('short_order', "LIKE", sprintf("%%WH%%"))
                ->orderBy('id', 'desc')
                ->first();

            $order = new Order;
            $order->short_order = sprintf("WH%d", (10000 + $manual_order_count->id));
            $order->order_id = sprintf("%s-%s", $request->get('store'), $order->short_order);
            $order->order_numeric_time = strtotime('Y-m-d h:i:s', strtotime("now"));
            $order->store_id = $request->get('store');
            $order->order_status = 4;

            if ($request->has('order_date')) {
                $order->order_date = date('Y-m-d h:i:s', strtotime($request->get('order_date') . date(" H:i:s")));
            } else {
                $order->order_date = date('Y-m-d h:i:s', strtotime("now"));
            }

            $customer = new Customer;
            $customer->order_id = $order->order_id;
            $customer->save();

            try {
                $order->customer_id = $customer->id;
            } catch (Exception $exception) {
                Log::error('Failed to insert customer id - Update new');
            }

        } else {
            $order = Order::with('store', 'customer')
                ->where('id', $id)
                ->latest()
                ->first();

            $customer = Customer::find($order->customer_id);
        }
        // 		return $request->all();

        $customer->ship_company_name = $request->get('ship_company_name');
        $customer->bill_company_name = $request->get('bill_company_name');
        $customer->ship_full_name = $request->get('ship_full_name');
        $customer->ship_first_name = $request->get('ship_first_name');
        $customer->ship_last_name = $request->get('ship_last_name');
        $customer->bill_first_name = $request->get('bill_first_name');
        $customer->bill_last_name = $request->get('bill_last_name');
        $customer->ship_address_1 = $request->get('ship_address_1');
        $customer->bill_address_1 = $request->get('bill_address_1');
        $customer->ship_address_2 = $request->get('ship_address_2');
        $customer->bill_address_2 = $request->get('bill_address_2');
        $customer->ship_city = $request->get('ship_city');
        $customer->ship_state = Helper::stateAbbreviation($request->get('ship_state'));
        $customer->bill_city = $request->get('bill_city');
        $customer->bill_state = $request->get('bill_state');
        $customer->ship_zip = $request->get('ship_zip');
        $customer->bill_zip = $request->get('bill_zip');
        $customer->ship_country = $request->get('ship_country');
        $customer->bill_country = $request->get('bill_country');
        $customer->ship_phone = $request->get('ship_phone');
        $customer->bill_phone = $request->get('bill_phone');
        $customer->bill_email = $request->get('bill_email');
        $customer->save();

        if ($order->store->validate_addresses == '1') {

            try {
                $isVerified = Shipper::isAddressVerified($customer);
            } catch (Exception $exception) {
                $isVerified = 0;
            }

            if (!$isVerified && $customer->ignore_validation == FALSE) {
                $customer->is_address_verified = 0;
                if ($order->order_status == 4) {
                    $order->order_status = 11;
                }
            } else if (!$isVerified) {
                $customer->is_address_verified = 0;
            }

            $customer->save();

        } else {
            $isVerified = 1;
        }

        $order->order_comments = $request->get('order_comments');

        if ($request->has('purchase_order')) {
            $order->purchase_order = $request->get('purchase_order');
        }

        if ($request->has('coupon_id')) {
            $order->coupon_id = $request->get('coupon_id');
        }

        if ($request->has('coupon_value')) {
            $order->coupon_value = floatval($request->get('coupon_value'));
        }

        if ($request->has('promotion_value')) {
            $order->promotion_value = floatval($request->get('promotion_value'));
        }

        if ($request->has('shipping_charge')) {
            $order->shipping_charge = floatval($request->get('shipping_charge'));
        }

        if ($request->has('adjustments')) {
            $order->adjustments = floatval($request->get('adjustments'));
        }

        if ($request->has('insurance')) {
            $order->insurance = floatval($request->get('insurance'));
        }

        if ($request->has('gift_wrap_cost')) {
            $order->gift_wrap_cost = floatval($request->get('gift_wrap_cost'));
        }

        if ($request->has('tax_charge')) {
            $order->tax_charge = floatval($request->get('tax_charge'));
        }

        if ($request->get('shipping') != null) {
            if ($request->get('shipping') != 'MN*') {
                $order->carrier = substr($request->get('shipping'), 0, 2);
                $order->method = substr($request->get('shipping'), 3);
            } else {
                $order->carrier = 'MN';
                $order->method = 'Give to ' . auth()->user()->username;
            }
        }

        $order->save();

        $item_ids = $request->get('item_id');
        $item_skus = $request->get('item_sku');
        $item_options = $request->get('item_option');
        $item_quantities = $request->get('item_quantity');
        $item_descriptions = $request->get('item_description');
        $item_prices = $request->get('item_price');
        $child_skus = $request->get('child_sku');

        $grand_sub_total = 0;

        if (!is_array($item_skus)) {
            return redirect()->back()->withInput()->withErrors('No Items Entered');
        }

        foreach ($item_skus as $index => $item_sku) {

            $options = [];
            $exploded = explode("\r\n", trim($item_options[$index], "\r\n"));

            foreach ($exploded as $key => $line) {
                $pieces = explode("=", trim($line));
                if (isset($pieces[1])) {
                    $item_option_key = trim($pieces[0]);
                    $item_option_value = trim($pieces[1]);
                } else if (isset($pieces[0]) && strlen(trim($pieces[0])) > 0) {
                    $item_option_key = 'comment_' . $key;
                    $item_option_value = trim($pieces[0]);
                } else {
                    continue;
                }

                $key = str_replace(["\u00a0", "\u0081", "\u0091"], '',
                    str_replace(" ", "_", preg_replace("/\s+/", " ", $item_option_key)));

                if (strpos(str_replace([' ', ','], '', strtolower($item_option_value)), 'nothankyou') === FALSE) {
                    $options [$key] = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
                        return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
                    }, str_replace(["\u00a0", "\u0081", "\u0091"], '', $item_option_value));
                } else {
                    Log::info('Deleted option: ' . $item_option_value);
                }
            }

            if ($item_ids == null || $item_ids[$index] == '') {

                $product = Product::where('product_model', $item_sku)->first();

                $item = new Item();
                $item->order_id = $order->order_id;
                $item->store_id = $order->store_id;
                $item->order_5p = $order->id;
                $item->item_code = $item_sku;
                $item->item_unit_price = $item_prices[$index];
                $item->item_option = json_encode($options);

                if ($product) {
                    $item->item_id = $product->id_catalog;
                    $item->item_description = $product->product_name;
                    $item->item_thumb = $product->product_thumb;
                    $item->item_url = $product->product_url;
                } else {
                    $item->item_description = 'PRODUCT NOT FOUND';
                }

                $item->child_sku = Helper::getChildSku($item);;

                $item->data_parse_type = 'manual';

                if ($order->order_status == 6) {
                    $order->order_status = 4;
                }
            } else {

                $item = Item::find($item_ids[$index]);
                $item->child_sku = $child_skus[$index];
                $item->item_option = json_encode($options);
                if (isset($item_descriptions[$index])) {
                    $item->item_description = $item_descriptions[$index];
                }
            }

            $item->item_quantity = $item_quantities[$index];
            $item->save();

            $grand_sub_total += ((int)$item->item_quantity * (float)$item->item_unit_price);

            if (isset($item_ids[$index]) && $item_ids[$index] == '') {
                Order::note('CS: Item ' . $item->id . ' added to order', $order->id);
            }
        }

        $order->item_count = count($item_skus);
        // $order->sub_total = $order->items->sum;
        $order->total = ($grand_sub_total -
            $order->coupon_value -
            $order->promotion_value +
            $order->gift_wrap_cost +
            $order->shipping_charge +
            $order->insurance +
            $order->adjustments +
            $order->tax_charge);

        $order->save();

        if ($request->has('note')) {
            Order::note(trim($request->get('note')), $order->id);
        }

        $responseType = $isVerified ? "success" : "error";

        if ($id == 'new') {
            Notification::orderConfirm($order);
            $message = $isVerified ? sprintf("Order %s is entered.", $order->order_id) : sprintf("Order %s saved but address is unverified", $order->order_id);
            $note_text = "Order Entered Manually";
        } else {
            $message = $isVerified ? sprintf('Order %s is updated', $order->order_id) : sprintf("Order %s updated but address is unverified", $order->order_id);
            $note_text = "Order Info Manually Updated";
        }

        Order::note($note_text, $order->id);

        return redirect()->action('OrderController@details', ['order_id' => $order->id])->with($responseType, $message);
    }

    public function updateMethod(Request $request)
    {
        $order = Order::with('store')
            ->where('id', $request->get('id'))
            ->latest()
            ->first();

        if (!$order) {
            return redirect()->back()->withError('Order not Found');
        }

        $old_method = $order->carrier != null ? $order->carrier . ' ' . $order->method : 'DEFAULT';

        if ($request->get('method') == 'MN') {
            Order::note('CS: Ship Method set from ' . $old_method . ' to MANUAL', $order->id);
            $order->carrier = 'MN';
            $order->method = $request->get('method_note');
        } else if ($request->get('shipping_method') == '' && strlen($order->carrier) > 0) {
            Order::note('CS: Ship Method set from ' . $old_method . ' to DEFAULT SHIPPING', $order->id);
            $order->carrier = null;
            $order->method = null;
        } else if ($request->get('shipping_method') != '' &&
            $request->get('shipping_method') != $order->carrier . '*' . $order->method) {
            $order->carrier = substr($request->get('shipping_method'), 0, strpos($request->get('shipping_method'), '*'));
            $order->method = substr($request->get('shipping_method'), strpos($request->get('shipping_method'), '*') + 1);
            Order::note('CS: Ship Method set from ' . $old_method . ' to ' . $order->carrier . ' ' . $order->method, $order->id);
        }

        $order->save();

        return redirect()->back()->withSuccess('Shipping Method Updated');
    }

    public function updateMethods(Request $request)
    {

        $orders = $request->get('order_ids');

        if ((Auth::user()->id != 16)) {
            return redirect()->back()->withError("You don't have permission to update shipping method");
        }

        if (empty($orders)) {
            return redirect()->back()->withError('Selected Order not Found');
        }

        if (empty($request->get('Hidden_shipping_method'))) {
            return redirect()->back();
        }

        list($carrier, $method) = explode("*", $request->get('Hidden_shipping_method'));

        foreach ($orders as $order_id) {

            $order = Order::with('store')
                ->where('id', $order_id)
                ->latest()
                ->first();

            if (!$order) {
                return redirect()->back()->withError('Order not Found');
            }

            $old_method = $order->carrier != null ? $order->carrier . ' ' . $order->method : 'DEFAULT';

            if ($request->get('Hidden_shipping_method') == 'MN') {
                Order::note('CS: Ship Method set from ' . $old_method . ' to MANUAL', $order->id);
                $order->carrier = 'MN';
                $order->method = "MN";
            } else if ($request->get('Hidden_shipping_method') == '' && strlen($order->carrier) > 0) {
                Order::note('CS: Ship Method set from ' . $old_method . ' to DEFAULT SHIPPING', $order->id);
                $order->carrier = null;
                $order->method = null;
            } else if ($request->get('Hidden_shipping_method') != '' && $request->get('Hidden_shipping_method') != $order->carrier . '*' . $order->method) {
                $order->carrier = $carrier;
                $order->method = $method;
                Order::note('CS: Ship Method set from ' . $old_method . ' to ' . $order->carrier . ' ' . $order->method, $order->id);
            }

            $order->save();
        }
        return redirect()->back()->withSuccess('Shipping Methods Updated');
    }

    public function updateStore(Request $request)
    {
        $order = Order::with('items', 'customer')
            ->where('id', $request->get('order_5p'))
            ->where('is_deleted', '0')
            ->first();

        if (!$order) {
            return redirect()->back()->withErrors('Order not Found');
        }

        $old = $order->store_id;
        $new = $request->get('store_select');

        $order->store_id = $new;
        $order->order_id = str_replace($old . '-', '', $order->order_id);
        $order->save();

        foreach ($order->items as $item) {
            $item->store_id = $new;
            $item->order_id = str_replace($old . '-', '', $item->order_id);
            $item->save();
        }

        $order->customer->order_id = str_replace($old . '-', '', $order->customer->order_id);
        $order->customer->save();

        $notes = Note::where('order_id', 'LIKE', $old . '%')->get();

        foreach ($notes as $note) {
            $note->order_id = str_replace($old . '-', '', $note->order_id);
            $note->save();
        }

        $notes = Note::where('note_text', 'LIKE', '%' . $old . '%')
            ->get();

        foreach ($notes as $note) {
            $note->note_text = str_replace($old . '-', '', $note->note_text);
            $note->save();
        }

        return redirect()->action('OrderController@details', ['order_id' => $order->id]);
    }

    public function updateShipDate(Request $request)
    {
        $order = Order::with('store')
            ->where('id', $request->get('id'))
            ->latest()
            ->first();

        if (!$order) {
            return redirect()->back()->withError('Order not Found');
        }

        if (substr($request->get('ship_date'), 0, 1) == '0' || $request->get('ship_date') == '') {
            if ($order->ship_date != null) {
                Order::note('CS: Ship Date Unset', $order->id);
                $order->ship_date = null;
            }
        } else if ($order->ship_date != $request->get('ship_date')) {
            Order::note('CS: Ship Date set to ' . $request->get('ship_date'), $order->id);
            $order->ship_date = $request->get('ship_date');
            // Task::new('Order ' . $order->short_order . ' must ship by ' . $order->ship_date, null, 'App\Order', $order->id);
        }

        $order->save();

        return redirect()->back()->withSuccess('Ship Date Updated');
    }

    public function destroy($id)
    {
        $order = Order::find($id);
        if (!$order) {
            return view('errors.404');
        }

        foreach ($order->items as $item) {
            $item->is_deleted = '1';
            $item->save();

            if ($item->item_status == 'wap') {
                Wap::removeItem($item->id, $item->order_5p);
            }

            if ($item->batch_number != '0') {
                Batch::isFinished($item->batch_number);
            }
        }

        $order->is_deleted = '1';
        $order->save();

        return 'Order ' . $order->short_order . ' Deleted';
    }

    public function getManual(Request $request)
    {
        $shipping_methods = Shipper::listMethods();

        $stores = Store::list('%', '%', 'none');

        return view('orders.manual_order', compact('shipping_methods'))->with('stores', $stores);
    }

    public function hook(Request $request)
    {

        try {

            set_time_limit(0);

            $order_id = $request->get('ID');
            $previous_order = Order::with('items')
                ->where('order_id', $order_id)
                ->where('is_deleted', '0')
                ->orderBy('created_at', 'DESC')
                ->first();

            if ($previous_order) {
                $batched = $previous_order->items->filter(function ($item) {
                    return $item->batch_number != '0';
                })->count();

                if ($batched > 0) {
                    Log::info('Hook: Duplicate order not inserted (batched) ' . $order_id);
                    return response()->json([
                        'error' => true,
                        'message' => "Batch exists data can't be inserted",
                    ], 200);
                } else if ($previous_order->order_status != 4) {
                    Log::info('Hook: Duplicate order not inserted (status)' . $order_id);
                    return response()->json([
                        'error' => true,
                        'message' => "Order in DB, status not in process, data can't be inserted",
                    ], 200);
                } else {
                    Order::where('order_id', $order_id)->update(['is_deleted' => 1]);
                    Item::where('order_id', $order_id)->update(['is_deleted' => 1]);
                    Customer::where('order_id', $order_id)->update(['is_deleted' => 1]);
                }
            }

            try {
                $exploded = explode("-", $order_id);
                if (isset($exploded[2])) {
                    $short_order = $exploded[2];
                } else {
                    $short_order = $exploded[1];
                }
            } catch (Exception $e) {
                Log::error('Undefined offset when trying to create short order. Order ' . $order_id);
                Log::error($request);
                if (strlen($order_id) < 1) {
                    exit('no order');
                } else {
                    $short_order = $order_id;
                }
            }
            // -------------- Customers table data insertion started ----------------------//
            $customer = new Customer();
            $customer->order_id = $request->get('ID');
            $customer->ship_full_name = str_replace('&', '+', $request->get('Ship-Name'));
            $customer->ship_first_name = $request->get('Ship-Firstname');
            $customer->ship_last_name = $request->get('Ship-Lastname');
            $customer->ship_company_name = $request->get('Ship-Company');
            $customer->ship_address_1 = $request->get('Ship-Address1');
            $customer->ship_address_2 = $request->get('Ship-Address2');
            $customer->ship_city = $request->get('Ship-City');
            $customer->ship_state = Helper::stateAbbreviation($request->get('Ship-State'));
            $customer->ship_zip = $request->get('Ship-Zip');
            $customer->ship_country = $request->get('Ship-Country');
            $customer->ship_phone = $request->get('Ship-Phone');
            $customer->ship_email = $request->get('Ship-Email');
            $customer->shipping = $request->get('Shipping', "N/A"); # 1) flatrate_flatrate 2) jewel_priorityshipping_priority  3) jewel_expressshipping_express
            $customer->bill_full_name = $request->get('Bill-Name');
            $customer->bill_first_name = $request->get('Bill-Firstname');
            $customer->bill_last_name = $request->get('Bill-Lastname');
            $customer->bill_company_name = $request->get('Bill-Company');
            $customer->bill_address_1 = $request->get('Bill-Address1');
            $customer->bill_address_2 = $request->get('Bill-Address2');
            $customer->bill_city = $request->get('Bill-City');
            $customer->bill_state = $request->get('Bill-State');
            $customer->bill_zip = $request->get('Bill-Zip');
            $customer->bill_country = $request->get('Bill-Country');
            $customer->bill_phone = $request->get('Bill-Phone');
            $customer->bill_email = $request->get('Bill-Email');
            $customer->bill_mailing_list = $request->get('Bill-maillist');
            $customer->save();
//            Log::info('Hook: Customer saved');
            // -------------- Customers table data insertion ended ----------------------//
            // -------------- Orders table data insertion started ----------------------//
            $order = new Order();
            $order->order_id = $request->get('ID');
            try {
                $order->customer_id = $customer->id;
            } catch (Exception $exception) {
                Log::error('Failed to insert customer id in hook');
            }
            $order->short_order = $short_order;
            $order->purchase_order = $request->get('Purchase-Order');
            $order->item_count = $request->get('Item-Count');
            $order->coupon_description = $request->get('Coupon-Description');
            $order->coupon_id = $request->get('Promotions-Code');
            $order->coupon_value = abs($request->get('Promotions-Value'));
            $order->promotion_id = $request->get('Coupon-Id');
            $order->promotion_value = abs($request->get('Coupon-Value'));
            $order->shipping_charge = $request->get('Shipping-Charge');
            $order->tax_charge = $request->get('Tax-Charge');
//			$order->total = $request->get('Total');
            $order->total = ((double)$request->get('Total') + (double)$request->get('Shipping-Charge'));
            $order->card_name = $request->get('Card-Name');
            $order->card_expiry = $request->get('Card-Expiry');
            $order->order_comments = $request->get('Comments');
            $order->order_date = date('Y-m-d H:i:s', strtotime($request->get('Date')));
            //$order->order_numeric_time = strtotime($request->get('Numeric-Time'));
            // 06-22-2016 Change by Jewel
            $order->order_numeric_time = ($request->get('Numeric-Time'));
            $order->order_ip = $request->get('IP');
            $order->paypal_merchant_email = $request->get('PayPal-Merchant-Email', '');
            $order->paypal_txid = $request->get('PayPal-TxID', '');
            $order->space_id = $request->get('Space-Id');
            $order->store_id = $request->get('Store-Id');
            $order->store_name = $request->get('Store-Name');
            $order->order_status = 4;

            # 1) flatrate_flatrate 2) jewel_priorityshipping_priority  3) jewel_expressshipping_express 4) jewel_surepost
            switch ($request->get('Shipping', "N/A")) {
                case "jewel_priorityshipping_priority":
                    $order->carrier = "US";
                    $order->method = "PRIORITY";
                    break;
                case "jewel_expressshipping_express":
                    $order->carrier = "US";
                    $order->method = "EXPRESS";
                    break;
                case "jewel_surepostshipping_surepost":
                    $order->carrier = "UP";
                    $order->method = "S_SUREPOST";
                    break;
                case "jewel_upsgroundshipping_upsground":
                    $order->carrier = "UP";
                    $order->method = "S_GROUND";
                    break;
                case "jewel_ups2ndshipping_ups2nd":
                    $order->carrier = "UP";
                    $order->method = "S_AIR_2DAY";
                    break;
                case "jewel_upsnextdayshipping_upsnextday":
                    $order->carrier = "UP";
                    $order->method = "S_AIR_1DAYSAVER";
                    break;
                default:
                    $order->method = NULL;
            }


            $order->save();
//            Log::info('Hook: Orders saved');
            try {
                $order_5p = $order->id;
            } catch (Exception $exception) {
                $order_5p = '0';
                Log::error('Failed to get 5p order id in hook');
            }
            // -------------- Orders table data insertion ended ----------------------//
            // -------------- Items table data insertion started ------------------------//
            $upsell = array();
            $upsell_price = 0;

            for ($item_count_index = 1; $item_count_index <= $request->get('Item-Count'); $item_count_index++) {
                $sure3durl = null;
                $ItemOption = [];
                foreach ($request->all() as $key => $value) {

                    if ($item_count_index < 10) {
                        $len = 14;
                    } else {
                        $len = 15;
                    }

                    if ("Item-Option-" . $item_count_index . "-" == substr($key, 0, $len)) {
                        if (substr(strtolower($key), $len, 14) == 'special_offer_') {
                            $upsell[substr($key, $len)] = $value;
                            if (strpos($value, 'Yes') !== FALSE) {
                                Log::info('YES UPSELL ITEM ' . $value);
                            }
                            Order::note(substr($key, $len) . ' - ' . $value, $order->id, $order->order_id);

                        } else {

                            //$k = str_replace(['Choose ', 'Select '], '', substr($key, $len));

                            if (strpos(str_replace([' ', ','], '', strtolower($value)), 'nothankyou') === FALSE) {
                                $ItemOption [preg_replace('/[\x00-\x1F\x7F-\xFF\xA0]/ u ', '', substr($key, $len))] =
                                    preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
                                        return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
                                    }, str_replace(["\u00a0", "\u0081", "\u0091"], '', $value));
                                #########
                                $sure3durl = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
                                    return pack('H*', $match[1]);
                                }, str_replace(["\u00a0", "\u0081", "\u0091"], '', $value));

                                $ItemOption ['Custom_EPS_download_link'] = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
                                    return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
                                }, str_replace(["\u00a0", "\u0081", "\u0091"], '', $value));
                                #########
                            } else {
                                Log::info('Deleted option: ' . $value);
                            }
                        }
                    }
                }

                $matches = [];
                preg_match("~.*src\s*=\s*(\"|\'|)?(.*)\s?\\1.*~im", $request->get('Item-Thumb-' . $item_count_index), $matches);

                if (file_exists(base_path() . 'public_htproduct_thumbml/assets/images/product_thumb/' . $request->get('Item-Code-' . $item_count_index) . '.jpg')) {
                    $item_thumb = url('/') . '/assets/images/product_thumb/' . $request->get('Item-Code-' . $item_count_index) . '.jpg';
                } else if (file_exists(base_path() . 'public_html/assets/images/product_thumb/' . $request->get('Item-Code-' . $item_count_index) . '.png')) {
                    $item_thumb = url('/') . '/assets/images/product_thumb/' . $request->get('Item-Code-' . $item_count_index) . '.png';
                } else if (isset($matches[2])) {
                    $item_thumb = trim($matches[2], ">");
                } else {
                    $item_thumb = url('/') . '/assets/images/no_image.jpg';
                    Log::error(sprintf("Hook found undefinded offset 2 on item thumb %s Order# %s.", $request->get('Item-Thumb-' . $item_count_index), $order_5p));
                }


                ##################### SAVE ITEM #########################

                for ($item_qty = 1; $item_qty <= $request->get('Item-Quantity-' . $item_count_index); $item_qty++) {
                    Log::info("Order ID  = " . $request->get('ID') . " QTY = " . $item_qty . " out of = " . $request->get('Item-Quantity-' . $item_count_index));
//                }
                    $item = new Item();
                    $item->order_5p = $order_5p;
                    $item->order_id = $request->get('ID');
                    $item->store_id = $order->store_id;
                    $item->item_code = $request->get('Item-Code-' . $item_count_index);
                    $item->item_description = $request->get('Item-Description-' . $item_count_index);
                    $item->item_id = $request->get('Item-Id-' . $item_count_index);
                    $item->item_option = json_encode($ItemOption);
                    $item->item_quantity = 1;   # $request->get('Item-Quantity-' . $item_count_index);
                    $item->item_thumb = $item_thumb;
                    $item->item_unit_price = $request->get('Item-Unit-Price-' . $item_count_index);
                    $item->item_url = $request->get('Item-Url-' . $item_count_index);
                    $item->item_taxable = $request->get('Item-Taxable-' . $item_count_index);
                    $item->data_parse_type = 'hook';
                    $item->child_sku = Helper::getChildSku($item);
                    $item->sure3d = $sure3durl;
                    $item->save();

                    try {
                        $item_id = $item->id;
                    } catch (Exception $exception) {
                        $item_id = null;
                        Log::error('Failed to get item id in hook');
                    }

                    if (isset($ItemOption['Custom_EPS_download_link']) &&
                        (strpos(strtolower($item->item_description), 'photo') ||
                            Option::where('child_sku', $item->child_sku)->first()->sure3d == '1')) {
                        $item->sure3d = html_entity_decode($ItemOption['Custom_EPS_download_link']);
                        $item->save();
                    }

                    if (count($upsell) > 0) {
                        $upsell_price = $this->upsellItems($upsell, $order, $item);

                        $item->item_unit_price = $item->item_unit_price - $upsell_price;
                    }
                    //                Log::info('Hook: Items saved');

                }
                ##################### SAVE ITEM #########################
                // if (substr($order->order_comments, 0, 3) != 'MAH' && (substr($item->item_code, 0, 2) == 'MH' || substr($item->item_code, 0, 3) == 'KIT')) {
                // 	$checkMahOrder = true;
                // 	$order->order_comments = 'MAH ' . $order->order_comments;
                // 	$order->save();
                // }

                if ($item->item_option == '[]' || $item->item_option == '0') {
                    $order->order_status = 15;
                    $order->save();
                }

                // -------------- Items table data insertion ended ---------------------- //

                $product = Product::where('product_model', $item->item_code)->first();
                // where('id_catalog', $item->item_id)

                // no product found matching model
                if (!$product) {
                    $product = new Product();
                    $product->product_model = $item->item_code;
                }

                if ($product->id_catalog == null || $item->store_id == '52053152') {
                    $product->id_catalog = $item->item_id;
                }

                $product->product_url = $item->item_url;
                $product->product_name = $item->item_description;
                $product->product_price = $item->item_unit_price;
                $product->is_taxable = ($item->item_taxable == 'Yes' ? 1 : 0);
                $product->product_thumb = $item->item_thumb;
                $product->save();
//                Log::info('Hook: Product saved');

            }
            // -------------- Order Confirmation email sent Start   ---------------------- //
            if (substr($item->item_code, 0, 3) != 'KIT') {
                Notification::orderConfirm($order);
            }
            // -------------- Order Confirmation email sent End---------------------- //
            try {
                $isVerified = Shipper::isAddressVerified($customer);
            } catch (Exception $exception) {
                $isVerified = 0;
            }

            if ($isVerified) {
                $customer->is_address_verified = 1;
            } else {
                $customer->is_address_verified = 0;
                $order->order_status = 11;
                $order->save();
            }
            $customer->save();

            // -------------- Hold Free Orders   ---------------------- //
            // if ($order->total < 4.5) {
            // 	$order->order_status = 23;
            // 	$order->save();
            // }

            return response()->json([
                'error' => false,
                'message' => 'data inserted',
            ], 200);

        } catch (Exception $e) {

            Log::error('Hook: ' . $e->getMessage());

            return response()->json([
                'error' => true,
                'message' => 'error',
            ], 200);

        }
    }

    private function upsellItems($values, $order, $order_item)
    {

        $options = json_decode($order_item->item_option, TRUE);

        $total_price = 0;

        $parameters = Parameter::where('is_deleted', '0')
            ->selectRaw("LOWER(parameter_value) as parameter")
            ->get()
            ->toArray();

        foreach ($options as $key => $value) {
            $k = strtolower($key);

            if (in_array($k, $parameters) && !strpos($k, 'style') && !strpos($k, 'color')) {
                unset($options[$key]);
            } else if ($key == 'Confirmation_of_Order_Details' || $key == 'couponcode') {
                unset($options[$key]);
            } else if (strpos($value, '$') || strpos(str_replace([' ', ','], '', strtolower($value)), 'nothankyou')) {
                unset($options[$key]);
            }
        }

        Log::info($options);

        foreach ($values as $key => $value) {

            if (!strpos(strtolower($value), 'yes')) {
                continue;
            }

            Log::info('OrderController: Upsell Item found ' . $order->id);

            $price = substr($value, strrpos($value, '$') + 1, strrpos($value, '.', strrpos($value, '$')) - strrpos($value, '$') + 2);

            $start = stripos($value, ':') + 1;

            $sku = trim(substr($value, $start, stripos($value, ' ', $start + 2) - $start));

            if (substr(strtolower($key), 0, 15) == 'special_offer_-') {
                $desc = trim(str_replace('_', ' ', substr($key, 16)));
            } else {
                $desc = trim(str_replace('_', ' ', $key));
            }


            Log::info("Upsell: price = $price, sku = $sku, desc = $desc");

            $product = Product::where('product_model', $sku)
                ->first();

            if (!$product) {
                Log::error('Upsell Product not in 5p: ' . $order->order_id);
                continue;
            }

            try {
                $item = new Item();
                $item->order_5p = $order->id;
                $item->order_id = $order->order_id;
                $item->store_id = $order->store_id;
                $item->item_code = $sku;
                $item->item_description = $product->product_name;
                $item->item_id = $product->id_catalog;
                $item->item_option = json_encode($options);
                $item->item_quantity = $order_item->item_quantity;
                $item->item_thumb = isset($product->product_thumb) ? $product->product_thumb : url('/') . '/assets/images/no_image.jpg';
                $item->item_unit_price = $price;
                $item->item_url = isset($product->product_url) ? $product->product_url : null;
                $item->data_parse_type = 'hook';
                $item->child_sku = Helper::getChildSku($item);
                $item->save();

                $total_price += $price;

            } catch (Exception $e) {
                Log::error('Upsell: could not add item ' . $sku);
                Log::error($item);
            }

        }

        return $total_price;
    }

    /**
     * Manual Re-Order
     */
    public function manualReOrder($order_id)
    {
        $manual_order_count = Order::where('short_order', "LIKE", sprintf("%%WH%%"))
            ->orderBy('id', 'desc')
            ->first();

        $short_order = sprintf("WH%d", (10000 + $manual_order_count->id));


        $order_from = Order::where('id', $order_id)
            ->where('is_deleted', '0')
            ->get();

        $order_id_new = $order_from->last()->store_id . '-' . $short_order;

        // 		Helper::deleteByOrderId($order_id_new);
        // -------------- Customers table data insertion started -----------------//
        $customer_from = Customer::where('id', $order_from->last()->customer_id)
            ->get();
        $customer = new Customer();
        $customer->order_id = $order_id_new;
        $customer->ship_full_name = $customer_from->last()->ship_full_name;
        $customer->ship_first_name = $customer_from->last()->ship_first_name;
        $customer->ship_last_name = $customer_from->last()->ship_last_name;
        $customer->ship_company_name = $customer_from->last()->ship_company_name;
        $customer->ship_address_1 = $customer_from->last()->ship_address_1;
        $customer->ship_address_2 = $customer_from->last()->ship_address_2;
        $customer->ship_city = $customer_from->last()->ship_city;
        $customer->ship_state = Helper::stateAbbreviation($customer_from->last()->ship_state);
        $customer->ship_zip = $customer_from->last()->ship_zip;
        $customer->ship_country = $customer_from->last()->ship_country;
        $customer->ship_phone = $customer_from->last()->ship_phone;
        $customer->ship_email = $customer_from->last()->ship_email;
        $customer->shipping = $customer_from->last()->shipping;
        $customer->bill_full_name = $customer_from->last()->bill_full_name;
        $customer->bill_first_name = $customer_from->last()->bill_first_name;
        $customer->bill_last_name = $customer_from->last()->bill_last_name;
        $customer->bill_company_name = $customer_from->last()->bill_company_name;
        $customer->bill_address_1 = $customer_from->last()->bill_address_1;
        $customer->bill_address_2 = $customer_from->last()->bill_address_2;
        $customer->bill_city = $customer_from->last()->bill_city;
        $customer->bill_state = $customer_from->last()->bill_state;
        $customer->bill_zip = $customer_from->last()->bill_zip;
        $customer->bill_country = $customer_from->last()->bill_country;
        $customer->bill_phone = $customer_from->last()->bill_phone;
        $customer->bill_email = $customer_from->last()->bill_email;
        $customer->bill_mailing_list = $customer_from->last()->bill_mailing_list;
        $customer->save();
        // // -------------- Customers table data insertion ended ----------------------//
        // // -------------- Orders table data insertion started ----------------------//

        $order = new Order();
        $order->order_id = $order_id_new;
        $order->short_order = $short_order;
        $order->customer_id = $customer->id;
        $order->item_count = $order_from->last()->item_count;
        $order->coupon_description = $order_from->last()->coupon_description;
        $order->coupon_id = $order_from->last()->coupon_id;
        $order->coupon_value = $order_from->last()->coupon_value;
        $order->shipping_charge = $order_from->last()->shipping_charge;
        $order->tax_charge = $order_from->last()->tax_charge;
        $order->total = $order_from->last()->total;
        $order->card_name = $order_from->last()->card_name;
        $order->card_expiry = $order_from->last()->card_expiry;
        $order->order_comments = $order_from->last()->order_comments;
        $order->order_date = date('Y-m-d H:i:s');
        //$order->order_numeric_time = strtotime($order_from->last()->Numeric-Time'));
        // 06-22-2016 Change by Jewel
        $order->order_numeric_time = strtotime(date('Y-m-d H:i:s'));
        $order->order_ip = gethostbyname(trim('hostname'));
        $order->paypal_merchant_email = $order_from->last()->paypal_merchant_email;
        $order->paypal_txid = $order_from->last()->paypal_txid;
        $order->space_id = $order_from->last()->space_id;
        $order->store_id = $order_from->last()->store_id;
        $order->store_name = $order_from->last()->store_name;
        $order->order_status = 4;
        $order->save();

        try {
            $order_5p = $order->id;
        } catch (Exception $exception) {
            $order_5p = '0';
            Log::error('Failed to get 5p order id in manualReOrder');
        }
        // -------------- Orders ble data insertion ended ----------------------//
        // -------------- Items table data insertion started ------------------------//
        $items = Item::where('order_5p', $order_id)
            ->where('is_deleted', '0')
            ->get();

        foreach ($items as $item_from) {
            $item = new Item();
            $item->order_5p = $order_5p;
            $item->order_id = $order_id_new;
            $item->store_id = $order->store_id;
            $item->item_code = $item_from->item_code;
            $item->item_description = $item_from->item_description;
            $item->item_id = $item_from->item_id;
            $item->item_option = $item_from->item_option;
            $item->item_quantity = $item_from->item_quantity;
            $item->item_thumb = $item_from->item_thumb;
            $item->item_unit_price = $item_from->item_unit_price;
            $item->item_url = $item_from->item_url;
            $item->item_taxable = $item_from->item_taxable;
            //$item->item_order_status_2 = 4;
            $item->data_parse_type = 'hook';
            $item->child_sku = $item_from->child_sku;
            $item->save();

            if ($item->item_option == '[]') {
                $order->order_status = 15;
                $order->save();
            }
        }

        Order::note('CS: Order Duplicated from ' . $order_from->last()->order_id, $order_5p);
        Order::note('Order Duplicated to ' . $order_id_new, $order_id);

        // $note = new Note();
        // $note->note_text = "Copy from Old Order# " . $order_id . " to new Order# " . $order_id_new;
        // $note->order_5p = $order->order_5p;
        // $note->order_id = $order->order_id;
        // $note->user_id = auth()->user()->id;
        // $note->save();

        return redirect()
            ->to(url('orders/details/' . $order_5p))
            ->with('success', 'Manul re-order  success.');

    }

    public function searchOrder(Request $request)
    {

        $input = trim($request->get('search_input'));

        $orders = Order::where('is_deleted', '0')
            ->where('short_order', 'LIKE', '%' . $input . '%')
            ->get();

        if (count($orders) == 0) {
            //error no order
            return redirect()
                ->to(url(sprintf("orders/details/%s", $request->get('prev_order'))))
                ->withErrors('Order Not Found');

        } elseif (count($orders) == 1) {
            //display in details
            return redirect()
                ->to(url(sprintf("orders/details/%s", $orders[0]->id)));

        } elseif (count($orders) > 1) {
            //send to order list
            return redirect()
                ->to(url(sprintf("orders/list?search_for_first=%s&operator_first=in&search_in_first=orders.order_id", $input)));

        } else {
            //error to log
            Log::error('Error searching for order in OrderController');

            return redirect()
                ->to(url(sprintf("orders/details/%s", $request->get('prev_order'))))
                ->withErrors('Error');
        }
    }

    public function checkShipDate()
    {

        $holds = Order::where('order_status', 12)->get();

        foreach ($holds as $order) {
            if ($order->ship_date <= date("Y-m-d")) {
                $order->order_status = 4;
                $order->save();
            }
        }

        return;
    }
}

