<?php

get('trk_order_status', 'ItemController@getOrderStatus');

//cron jobs
get('scripts/getInput', 'StoreController@retrieveData');
get('prints/sendbyscript', 'NotificationController@shipNotify');
get('graphics/sort', 'GraphicsController@sortFiles');
get('note_fix', 'DataController@note_job');
get('cleanup', 'DataController@cleanup');
get('auto_batch/{max_units}', 'ItemController@autoBatch');
get('stock_update', 'InventoryController@updateStock');
get('screenshot', 'ReportController@screenshot');
get('download_images', 'ProductController@download_images');
get('download_sure3d', 'GraphicsController@downloadSure3d');
// get('update_skus', 'ProductController@updateSkus');
get('inventory_images', 'InventoryController@download_images');
get('scripts/ship_date', 'OrderController@checkShipDate');
get('tasks/due', 'TaskController@tasksDue');
get('graphics/sub_summary/{batch_number}', 'PrintController@sublimationSummary'); 
get('graphics/sub_screenshot/{batch_number}', 'PrintController@subScreenshot');
get('graphics/print_wasatch', 'GraphicsController@printWasatch');
get('graphics/auto_print', 'GraphicsController@autoPrint');
get('report/wasatch_dashboard/update', 'WasatchDashboardController@updateWasatch');
get('graphics/print_all', 'GraphicsController@printAllSublimation');

// auth middleware enabled controller
Route::group([ 'middleware' => [ 'auth' ] ], function () {
	Route::group([ 'middleware' => 'user_has_access' ], function () {
		get('/', 'HomeController@index');
		get('logout', 'AuthenticationController@getLogout');
		
		//reporting
		get('report/sales_summary', 'ReportController@salesSummary');
		get('report/profit', 'ReportController@profitSummary');
		get('report/order_date', 'ReportController@orderDateReport');
		get('report/history', 'ReportController@section_history');
		get('report/viewPdf', 'ReportController@viewPdf');
		get('report/logs', 'StationLogController@index');
		get('report/data', 'DataController@index');
		get('shipping/must_ship', 'ReportController@mustShipReport');
		get('report/rejects', 'ReportController@rejectsDetail');
		get('report/ship_date', 'ReportController@shipDateReport');
		get('report/coupon', 'ReportController@couponReport');
		get('report/sales', 'ReportController@salesReport');
		get('report/items', 'ReportController@itemsReport');
		get('report/wasatch_dashboard', 'WasatchDashboardController@index');
		
		//production Reports
		get('prod_report/summary', 'ReportController@section');
		get('prod_report/station_summary', 'ReportController@stationSummary');
		get('prod_report/unbatchable', 'ItemController@unBatchableItems');
		get('prod_report/stockreport', 'InventoryController@getStockReport');
		get('prod_report/missing_report', 'WapController@missingReport');
		get('prod_report/wap', 'ReportController@wapSummary');
		
		//configure production
		get('prod_config/sections', 'SectionController@index');
		post('prod_config/sections_update', 'SectionController@store');
		post('prod_config/sections_assign', 'SectionController@assign');
		post('prod_config/sections_delete', 'SectionController@delete');
		resource('prod_config/batch_routes', 'BatchRouteController');
		post('prod_config/import_batch_routes', 'ImportController@importBatchRoute');
		get('prod_config/export_batch_routes', 'ExportController@batch_routes');
		resource('prod_config/templates', 'TemplateController', [
			'except' => [ 'create' ],
		]);
		get('prod_config/rejection_reasons/sort/{direction}/{id}', 'RejectionReasonController@sortOrder');
		resource('prod_config/rejection_reasons', 'RejectionReasonController');
		resource('prod_config/stations', 'StationController');
		get('prod_config/work_config', 'ProductionController@workConfig');
				
		//purchases
		get('purchases/receive', 'PurchaseController@receive');
		put('purchases/receive', 'PurchaseController@receive');
		post('purchases/getVendorById', 'PurchaseController@getVendorById');
		post('purchases/purchased_inv_products', 'PurchaseController@getPurchasedInvProducts');
		// get('autocomplete',array('as'=>'autocomplete'));
		// post('purchases/searchajax', 'PurchaseController@autoComplete');
		post('purchases/purchasedVendorSku', 'PurchaseController@purchasedVendorSku');
		resource('purchases/vendors', 'VendorController');
		get('purchases/print/{purchase_id}', 'PrintController@purchase');
		resource('purchases/purchasedinvproducts', 'PurchasedInvProductsController');
		resource('purchases', 'PurchaseController');
		post('purchases/getuniquestock','InventoryController@getStockNoUnique');
		
		//user Admin
		resource('users', 'UserController');
		get('users/barcode/{id}', 'UserController@barcode');
		
		//products configuration
		resource('products_config/collections', 'CollectionController');
		resource('products_config/occasions', 'OccasionController');
		resource('products_config/categories', 'CategoryController');
		resource('products_config/sub_categories', 'SubCategoryController');
		resource('products_config/production_categories', 'ProductionCategoryController');
		resource('products_config/sales_categories', 'SalesCategoryController');
		get('products_config/master_categories/get_next/{parent_category_id}', 'MasterCategoryController@getNext');
		resource('products_config/master_categories', 'MasterCategoryController');
		
		post('products/change_mixing_status', 'ProductController@change_mixing_status');
		get('products/unassigned', 'ProductController@unassigned');
		get('products/sync', 'ProductController@getSync');
		post('products/sync', 'ProductController@postSync');
		post('products/post_to_yahoo', 'ProductController@post_to_yahoo');
		#get('products/import', 'ProductController@getAddProductsByCSV');
		post('products/import', 'ProductController@import');
		get('products/export', 'ProductController@export');
		
		resource('products', 'ProductController');
		
		get('products_specifications/step/{id?}', 'ProductSpecificationController@getSteps');
		post('products_specifications/step/{id}', 'ProductSpecificationController@postSteps');
		get('products_specifications/copy/{categoty_id}/{product_sku}', 'ProductSpecificationController@copyProduct');
		get('products_specifications/sheets', 'PrintController@print_spec_sheet');
		resource('products_specifications', 'ProductSpecificationController');
		
		//Configure Child Skus
		get('logistics/add_child_sku', 'LogisticsController@get_add_child_sku');
		post('logistics/add_child_sku', 'LogisticsController@post_add_child_sku');
		get('logistics/create_child_sku', 'LogisticsController@create_child_sku');
		post('logistics/create_child_sku', 'LogisticsController@post_create_child_sku');
		post('logistics/post_preview', 'LogisticsController@post_preview');
		
		//batching
		get('preview_batch', 'ItemController@getBatch');
		post('preview_batch', 'ItemController@postBatch');
        get('preview_batch/lock', 'ItemController@postLock')->name('formSubmit1');

		
		//graphics
		// get('graphics/dash', 'GraphicsController@dash');
		get('graphics', 'GraphicsController@index');
		get('graphics/print_sublimation', 'GraphicsController@showSublimation');
		post('graphics/print_all', 'GraphicsController@printAllSublimation');
        get('graphics/lock', 'GraphicsController@postJobLock')->name('jobFormSubmit');

		get('graphics/printer_config', 'GraphicsController@printerConfig');
		get('graphics/delete_file/{graphic_dir}/{file}', 'GraphicsController@deleteFile');
		post('graphics/move_to_print', 'GraphicsController@printSublimation');
		post('graphics/reprint_graphic', 'GraphicsController@reprintGraphic');
		get('graphics/reprint_bulk', 'GraphicsController@reprintBulk');
		get('graphics/sent_to_printer', 'GraphicsController@sentToPrinter');
		post('graphics/complete_manual', 'GraphicsController@completeManual');
		get('graphics/export_batch/{id}/{force}/{format}', 'BatchController@export_batch');
		get('graphics/export_batchbulk', 'BatchController@export_bulk');
		post('graphics/export_batchbulk', 'BatchController@export_bulk');
		post('graphics/upload_file', 'GraphicsController@uploadFile');
		// get('graphics/resize/{id}', 'GraphicsController@resizeSure3d');
		// get('graphics/resizeBatch/{id}', 'GraphicsController@resizeBatch');
		
		resource('graphics/designs', 'DesignController');
		
		//move to production
		get('move_to_production', 'GraphicsController@selectToMove');
		post('move_to_production/show_batch', 'GraphicsController@showBatch');

        //move to QC
        get('move_to_qc', 'GraphicsController@selectToMoveQc');
        post('move_to_qc/show_batch', 'GraphicsController@showBatchQc');

//        get('production/scan_work', 'ProductionController@openScanWork');
//        post('production/scan_work', 'ProductionController@scanWork');


		//print summaries
		get('summaries/print', 'GraphicsController@selectSummaries');
		post('summaries/print', 'PrintController@batches');
		get('supervisor/print_summaries', 'PrintController@batches');
		get('summaries/single', 'PrintController@singleBatch');
		
		//picking
		get('picking/summary', 'PickingController@getInventorySummary');
		post('picking/report', 'PickingController@printInventoryReport');
		post('picking/view', 'PickingController@viewInventoryReport');
		post('picking/pick', 'PickingController@pickInventoryReport');
		post('picking/delete', 'PickingController@deleteInventoryReport');
		
		//inventory
		get('inventories/ajax_update','InventoryController@updateInventory');
		resource('inventories', 'InventoryController');
		resource('inventoryunit', 'InventoryUnitController');
		
		//inventory Admin
		get('inventory_admin/ajax_update','InventoryController@updateInventory');
		get('inventory_admin/calculate_ordering','InventoryController@calculateOrdering');
		get('inventory_admin/duplicate/{id}', 'InventoryController@create');
		get('inventory_admin/delete', 'InventoryController@delete'); 
		resource('inventory_admin/inventory_adjustments', 'InventoryAdjustmentController');
		
		resource('tasks', 'TaskController');
		
		//manual orders
		get('orders/manual', 'OrderController@getManual');
		post('orders/manual', 'OrderController@postManual');
		get('orders/manual/ajax_search', 'ProductController@ajaxSearch');
		get('orders/manual/product_info', 'ProductController@product_info');
		get('orders/addmanual/{order_id}', 'OrderController@manualReOrder');
		get('orders/ajax', 'OrderController@ajax');
		
		post('orders/mailer', 'NotificationController@getMailMessage');
		post('orders/send_mail', 'NotificationController@send_mail');
		
		//orders & items
		get('orders/details/{order_id}', 'OrderController@details')->name('orderShow');
		post('orders/status_change', 'OrderController@changeStatus');
		post('orders/searchOrder', 'OrderController@searchOrder');
		post('ship_order/update_method', 'OrderController@updateMethod');
        post('ship_order/update_methods', 'OrderController@updateMethods');
		post('orders/update_shipdate', 'OrderController@updateShipDate');
		get('orders/list', 'OrderController@getList');
		resource('items', 'ItemController', ['except' => 'show']);
		resource('orders', 'OrderController');
		get('orders/packing/{id}', 'PrintController@packing');
		
		post('orders_admin/change_store', 'OrderController@updateStore');
		
		//production
		get('batches/list', 'BatchController@index');
        get('batches/list_graphic', 'BatchController@indeGraphic');
		get('batches/details/{batch_number}', 'BatchController@show')->name('batchShow');
		get('batches/tray_label/{id}', 'BatchController@trayLabel');
		get('batches/view_graphic', 'GraphicsController@viewGraphic');
		
		get('reject_item', 'RejectionController@reject');
		
		get('move_next', 'ProductionController@openMoveNext');
		post('move_next', 'ProductionController@moveNextStation');
		
		//production
		get('production/status', 'ProductionController@status');
		get('production/status_detail', 'ProductionController@statusDetail');
		get('production/scan_work', 'ProductionController@openScanWork');
		post('production/scan_work', 'ProductionController@scanWork');
		get('production/user_section', 'ProductionController@ajaxSection');
		
		post('reject_batch', 'RejectionController@rejectBatch');
		
		//production supervisor
		post('supervisor/scan_work', 'ProductionController@scanWork');
		post('supervisor/move_batch', 'ProductionController@moveNextStation');
		get('supervisor/release/{batch_number}', 'BatchController@release');
		
		// get('export_station', 'StationController@getExportStationLog');
		// post('export_station', 'StationController@postExportStationLog');
		
		//rejection Admin
		get('rejections/', 'RejectionController@index');
		post('rejections/process', 'RejectionController@process');
		get('rejections/split', 'RejectionController@splitBatch');
		get('rejections/reprint', 'RejectionController@reprintLabel');
		post('rejections/send_to_start', 'RejectionController@sendToStart');
		
		//backorders
		get('backorders', 'BackorderController@index');
		get('backorders/batch', 'BackorderController@getBatch');
		get('backorders/show', 'BackorderController@show');
		post('backorders/items', 'BackorderController@itemsBackorder');
		post('backorders/arrive', 'BackorderController@batchArrive');
		post('backorders/items_arrive', 'BackorderController@itemsArrived');
		post('backorders/stock_change', 'BackorderController@stockNumber');
		
		get('items/delete_item/{order_id}/{item_id}', 'ItemController@delete_item_id');
		get('items/restore_item/{order_id}/{item_id}', 'ItemController@restore_item_id');
		get('items/child_sku/{item_id}', 'ItemController@refreshChildSku');
        get('items_grapfic', 'ItemController@indexGraphic');

		
		//shipping supervisor
		get('ship_order/ship_from_order', 'ShippingController@shipFromOrder');
		get('ship_order/item_tracking', 'ShippingController@manualShip');
		post('ship_order/returned', 'ShippingController@shipmentReturned');
		post('ship_order/ship_items', 'ShippingController@shipItems');
		get('ship_order/void/{shipment_id}/{order_5p}', 'ShippingController@void');

		//shipper
		post('shipping/ship_items', 'ShippingController@shipItems');
		get('shipping', 'ShippingController@index')->name('shipShow');
		post('shipping', 'ShippingController@index');
		get('shipping/ship_items', 'ShippingController@shipItems');
		post('shipping/add_wap', 'WapController@addItems');
		post('shipping/bad_address', 'WapController@bad_address');
		get('shipping/qc_station', 'QcController@index');
		get('shipping/qc_list', 'QcController@showStation');
		post('shipping/qc_scanIn', 'QcController@scanIn');
		get('shipping/qc_redirect', 'QcController@scanIn');
		get('shipping/qc_batch', 'QcController@showBatch')->name('qcShow');
		post('shipping/qc_order', 'QcController@showOrder');
        post('shipping/qc_order_by_item', 'QcController@scanInByItemId');
		get('shipping/qc_order', 'QcController@showOrder')->name('qcOrder');
		
		//wap
		get('wap/index', 'WapController@index');
		get('wap/details', 'WapController@showBin')->name('wapShow');
		get('wap/reprint', 'WapController@reprintWapLabel');
		
		// get('shipping_address_update', 'ShippingController@shippingAddressUpdate');
	
		get('exports/export_orders', 'ItemController@export_orders');
		post('exports/orders', 'OrderController@csvExport');
		post('exports/items', 'ItemController@csvExport');
		
		//customer service
		get('customer_service/index', 'CsController@index');
		get('customer_service/ajax_button', 'CsController@ajaxButton');
		resource('customer_service/customers', 'CustomerController');
		get('customer_service/csProcess', 'RejectionController@csProcess');
		get('customer_service/bulk_email', 'NotificationController@bulk_email');
		post('customer_service/bulk_email', 'NotificationController@bulk_email_post');
		get('customer_service/test/{order_id}/{type}', 'NotificationController@test');
		resource('customer_service/email_templates', 'EmailTemplateController');
		post('customer_service/redo', 'RejectionController@redoItem');
		
		//configure skus
		get('logistics/update_ajax', 'LogisticsController@update_ajax');
		get('logistics/sku_list', 'LogisticsController@sku_list');
		post('logistics/update_skus', 'LogisticsController@update_skus');
		get('logistics/edit_sku', 'LogisticsController@edit_sku');
		put('logistics/edit_sku', 'LogisticsController@update_sku');
		get('logistics/parameters', 'LogisticsController@parameters');
		put('logistics/update_parameters', 'LogisticsController@update_parameters');
		//post('logistics/update_parameter_option/{unique_row}', 'LogisticsController@update_parameter_option');
		delete('logistics/delete_sku/{unique_row_value}', 'LogisticsController@delete_sku');
		
		//stores
		get('stores/test', 'StoreController@email_test');
		get('stores/inventory', 'StoreController@inventoryUpload');
		get('stores/sort/{direction}/{id}', 'StoreController@sortOrder');
		get('stores/visible/{id}', 'StoreController@visible');
		resource('stores', 'StoreController');
		
		get('stores/items/{store_id}', 'StoreItemController@index');
		get('stores/items/get_csv/{store_id}', 'StoreItemController@getCSV');
		post('stores/items/post_csv', 'StoreItemController@uploadCSV');
		post('stores/items/add', 'StoreItemController@store');
		post('stores/items/update', 'StoreItemController@update');
		get('stores/items/delete/{id}', 'StoreItemController@destroy');
		
		//import order
		get('transfer/import', 'StoreController@import'); 
		post('transfer/import', 'StoreController@import'); 
		get('transfer/export', 'StoreController@exportSummary'); 
		get('transfer/export/details', 'StoreController@exportDetails'); 
		post('transfer/export/create', 'StoreController@createExport'); 
				
		post('transfer/export/qb', 'StoreController@qbExport');
        post('transfer/export/qbcsv', 'StoreController@qbCsvExport');
	});
});

// guest middleware enabled controller
Route::group([ 'middleware' => [ 'guest' ] ], function () {
	get('login', 'AuthenticationController@getLogin');
	post('login', 'AuthenticationController@postLogin');
	post('hook', 'OrderController@hook');
});

// // Redefinition of routes
// get('home', function () {
// 	return redirect(url('/'));
// });

Route::group([ 'prefix' => 'auth' ], function () {
	get('login', 'AuthenticationController@getLogin');
	get('logout', 'AuthenticationController@getLogout');
});

Event::listen('illuminate.query', function ($q) {
	#Log::info($q);
});
