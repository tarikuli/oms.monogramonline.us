<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WasatchDashboard extends Model
{
    protected $table = "wasatch_dashboard";

//

    public function getBatchInfo($batch_number)
    {
        $batch = WasatchDashboard::where('batch_number', $batch_number)
            ->first();
        return $batch;
    }
}
