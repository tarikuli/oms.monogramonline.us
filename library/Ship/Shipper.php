<?php namespace Ship;

use Ship\UPS;
use Ship\FEDEX;
use Ship\Post;
use Ship\USPS;
use App\Item;
use App\Ship;
use App\ItemShip;
use App\Order;
use App\Batch;
use App\Wap;
use Monogram\Helper;
use Illuminate\Support\Facades\Log;
use Ups\Entity\Address;
use Ups\AddressValidation;

use Ups\Shipping;

class Shipper
{
  /* Accepts 5p Order Id and QC or WAP origin flag
    If comes from QC, also require batch_number 
    If comes from WAP accept partial shipment flag */
  public function createShipment($origin, $order_id, $batch_number = NULL, $packages = [0], $item_ids = null) 
  {
    if (empty($origin)) {
      return 'ERROR: Origin not set. Order: ' . $order_id . ' Origin1: ' . $origin;
    }
    
    if ($origin != 'QC' && $origin != 'WAP') {
      return 'ERROR: Unrecognized origin.  Order: ' . $order_id . ' Origin2: ' . $origin;
    }
    
    if (empty($order_id)) {
      return 'ERROR: Order ID not set. Origin3: ' . $origin;
    } 
    
    $order = Order::with('store', 'customer')
              ->find($order_id);
    
    if (count((array)$order) == 0) {
        return 'ERROR: Order Not Found Order: ' . $order_id . ' Origin4: ' . $origin;
    }
    
    if (empty($order->customer)) {
        return 'ERROR: Customer Not Found.  Order: ' . $order_id . ' Origin5: ' . $origin ;
    }

    if (empty($order->store)) {
        return 'ERROR: Store Not Found.  Order: ' . $order_id . ' Origin6: ' . $origin ;
    }
  
    // if (0) { //($order->customer->ignore_validation == FALSE) {
    // 
    //     $val = $this->validateAddress ($order->customer);
    // 
    //     if($val['isAmbiguous']){
    // 
    //         Log::info('Address Validation: Ambiguous address ' . $order_id);
    // 
    //         return ['ambiguous', $val['ambiguousAddress'], $order->customer_id];
    //     }
    // 
    //     if($val['error'] != TRUE) {
    //         $order->order_status = 11;
    //         $order->save();
    // 
    //         return 'ERROR: ' . $val['error'] . '. Order: ' . $order_id . ' Origin: ' . $origin;
    //     }
    // 
    //     if(!$val['validateAddress']){
    //         $order->order_status = 11;
    //         $order->save();
    //         return 'ERROR: Please call Customer Service Department for Update correct Shipping address. Order: ' . $order_id . ' Origin: ' . $origin;
    //     }
    // 
    // }
    
    if ( $origin == 'QC' ) {
      
      $all_items = Item::where('order_5p', $order_id)
                ->searchStatus('shippable')
                ->searchStatus('production')
                ->where('is_deleted', '0')
                ->get();

      if ( empty($batch_number) ) {
        return 'ERROR: Batch number not set. Order: ' . $order_id . ' Origin7: ' . $origin;
      }

      $items = Item::selectRaw('id, (item_unit_price * item_quantity) as amount')
                  ->where('order_5p', $order_id)
                  ->where('is_deleted', '0')
                  ->searchStatus('production')
                  ->where('batch_number', $batch_number)
                  ->get();

      if (count($all_items) > count($items)) {
        return 'ERROR: Shippable items outside of batch, cannot ship this order from QC. Order: ' . $order_id . ' Batch: ' . $batch_number;
      }

    } elseif ( $origin == 'WAP' ) {
      
      $items = Item::selectRaw('id, (item_unit_price * item_quantity) as amount')
                ->where('order_5p', $order_id)
                ->where('is_deleted', '0')
                ->searchStatus('wap')
                ->get();
                
    }  elseif ( $origin == 'OR' ) {
      
      if (is_array($item_ids)) {
        
        $items = Item::selectRaw('id, (item_unit_price * item_quantity) as amount')
                  ->whereIn('id', $item_ids)
                  ->where('is_deleted', '0')
                  ->searchStatus('shippable')
                  ->get();
      } else {
        
        $items = Item::selectRaw('id, (item_unit_price * item_quantity) as amount')
                  ->whereIn('order_5p', $order_id)
                  ->where('is_deleted', '0')
                  ->searchStatus('shippable')
                  ->get();
      }
    }
    
    if (count($items) < 1) { 
      return 'ERROR: No items to Ship. Order: ' . $order_id . ' Batch: ' . $batch_number;
    }
    
    $unique_order_id = $this->generateShippingUniqueId($order->id);
//    Log::info('Shipping Order ' . $unique_order_id);
    
    $total_amount = $items->sum('amount');
    $item_ids = array_column( $items->toArray(), 'id');
    
    if ($order->carrier == 'UP' || $order->carrier == null) {
      $carrier = new UPS;
      $trackingInfo = $carrier->getLabel($order->store, $order, $unique_order_id, $order->method, $packages); 
    } elseif ($order->carrier == 'FX') {
      $carrier = new Fedex;
      $trackingInfo = $carrier->getLabel($order->store, $order, $unique_order_id, $order->method, $packages); 
    } elseif ($order->carrier == 'US') {
      $carrier = new Post;
      $trackingInfo = $carrier->getLabel($order->store, $order, $unique_order_id, $order->method, $packages[0]); 
    } elseif ($order->carrier == 'MN') {
      $trackingInfo = array();
      $trackingInfo['unique_order_id'] = $unique_order_id;
      $trackingInfo['tracking_number'] = auth()->user()->id . date("_Ymd_His");
      $trackingInfo['shipping_id'] = 'none';
      $trackingInfo['mail_class'] = $order->method;
      $trackingInfo['image'] = null;
    } else {
      Log::error('Unrecognized Carrier ' . $order->carrier . ' - ' . $unique_order_id);
      return 'ERROR: Unrecognized Carrier. Order: ' . $order_id;
    }
        
    if (is_array($trackingInfo)) {
      
      $this->insertTracking($trackingInfo, $item_ids, $order->id, $total_amount, $order->order_id, $packages);
      
      $image = $trackingInfo['image'];
      
      if ($order->store->packing_list == 'Z' || $order->store->packing_list == 'B') {
        $slip = $this->packingSlip($order, $item_ids);
      } else {
        $slip = '';
      }
      
      $store_label = '';
      $reminder = '';
      
      if ($order->store->ship_label == '1') {
        $className =  'Market\\' . $order->store->class_name; 
        $controller =  new $className;
        $store_info = $controller->shipLabel($order->store->store_id, $unique_order_id, $order->order_id);
        if (is_array($store_info)) {
          $reminder = $store_info[0];
          $store_label = $store_info[1];
        }
      }
      
      Wap::emptyBin($order_id);

      $image = trim(preg_replace('/\n+/', ' ', $image));
      
      //$image = str_replace("\00", " ", $image);
      
      $labels = str_replace(array("'", "\"", "&quot;"), " ", htmlspecialchars( $slip . $store_label )) . $image;
      
      $this->saveLabels($labels, $unique_order_id);
      
      return ['reminder' => $reminder, 'unique_order_id' => $unique_order_id];
    
    } else {
      return $trackingInfo . ' - Order: ' . $order_id . ' Origin8: ' . $origin;
    }
    
  }
  
  public function shipOrder ($order, $reship)
  {		
    $unique_order_id = $this->generateShippingUniqueId($order->id);
    
    if ($reship == '0') {
      
      $items = Item::selectRaw('id, (item_unit_price * item_quantity) as amount')
                ->where('order_5p', $order->id)
                ->where('is_deleted', '0')
                ->searchStatus('shippable')
                ->get();
                
    } elseif ($reship == '1') {
      
      $items = Item::selectRaw('id, (item_unit_price * item_quantity) as amount')
                ->where('order_5p', $order->id)
                ->where('is_deleted', '0')
                ->searchStatus('reshipment')
                ->get();
    }
              
    $total_amount = $items->sum('amount');
    $item_ids = array_column( $items->toArray(), 'id');
              
    if ($order->carrier == 'UP' || $order->carrier == null) {
      $carrier = new UPS;
      $trackingInfo = $carrier->getLabel($order->store, $order, $unique_order_id, $order->method); 
    } elseif ($order->carrier == 'FX') {
      $carrier = new FEDEX;
      $trackingInfo = $carrier->getLabel($order->store, $order, $unique_order_id, $order->method); 
    } elseif ($order->carrier == 'US') {
      $carrier = new Post;
      $trackingInfo = $carrier->getLabel($order->store, $order, $unique_order_id, $order->method);
      //$trackingInfo = $carrier->getLabel($order->store, $order, $unique_order_id, $order->method, 1);
    } elseif ($order->carrier == 'MN') {
      $trackingInfo = array();
      $trackingInfo['unique_order_id'] = $unique_order_id;
      $trackingInfo['tracking_number'] = 'none';
      $trackingInfo['shipping_id'] = 'none';
      $trackingInfo['mail_class'] = $order->method;
      $trackingInfo['image'] = null;
    } else {
      Log::error('Unrecognized Carrier ' . $order->carrier . ' - ' . $unique_order_id);
      return redirect()->back()->withErrors('ERROR: Unrecognized Carrier');
    }
		
    if (is_array($trackingInfo)) {
      
      $this->insertTracking($trackingInfo, $item_ids, $order->id, $total_amount, $order->order_id);
      $image = $trackingInfo['image'];
      
      $store_label = '';
      $slip = '';
      $reminder = '';
      
      if ($reship == 0) {
        $slip = $this->packingSlip($order, $item_ids);
        
        if ($order->store->ship_label == 1) {
          $className =  'Market\\' . $order->store->class_name; 
          $controller =  new $className;
          $store_info = $controller->shipLabel($order->store->store_id, $unique_order_id, $order->order_id);
          if (is_array($store_info)) {
            $reminder = $store_info[0];
            $store_label = $store_info[1];
          }
        }
      } 
      
      Wap::emptyBin($order->id);
      
      $image = trim(preg_replace('/\n+/', ' ', $image));
      
      $labels = str_replace(array("'", "\"", "&quot;"), " ", htmlspecialchars( $slip . $store_label )) . $image;
      
      $this->saveLabels($labels, $unique_order_id); 
      
      return ['unique_order_id' => $unique_order_id, 'reminder' => $reminder];
      
    } else {
      
      return $trackingInfo;
    }
  }
  
  public function enterTracking ($item_id, $order_5p, $track_number, $method) 
  {
      $methods = Shipper::listMethods();

    if ($item_id != 'all') {
			$items = Item::with('order')
                      ->where('id', $item_id)
                      ->get();
            $order = $items->first()->order;
      
		} else if ($item_id == 'all') {
			$items = Item::where('order_5p', $order_5p)
								->where('is_deleted', '0')
								->searchStatus('shippable')
								->get();
            $order = Order::find($order_5p);
		} else {
			return 'Item Not Set';
		}
    
    $item_ids = array();
    
    foreach ($items as $item) {
      if ($item->item_status == 'wap') {
        Wap::removeItem($item->id, $item->order_5p);
      }
      
      $item->tracking_number = trim($track_number);
      $item->item_status = 'shipped';
      
      $item->save();
      
      if ($item->batch_number != '0') {
        if (!Batch::isFinished($item->batch_number)) {
          $item->batch_number = '0';
        }
      }
      
      $item->save();
      
      $item_ids[] = $item->id;


      $this->updateTrackingInMagento($order->short_order, $track_number, $methods[$method]);

      Order::note('Tracking number ' . $track_number . ' added to item ' . $item->id, $order->id, $order->order_id);
      
    }
    
    $remaining = Item::where('order_5p', $order->id)
                  ->searchStatus('shippable')
                  ->where('is_deleted', '0')
                  ->count();
    
    if ($remaining == 0) {
      
       $order->order_status = 6;
       
       $shipinfo = explode('*', $method);
       $order->carrier = $shipinfo[0];
       if (isset($shipinfo[1])) {
         $order->method = $shipinfo[1];
       } else {
         if ($shipinfo[0] == 'MN') {
           $order->method = 'Manual Ship';
         } 
       }
       
       $order->save();
       
    }
    
    $shipment = Ship::where('shipping_id', trim($track_number))
                      ->where('order_number', $order->id)
                      ->first();
    
    if (count($shipment) == 0) {
      
//      $methods = Shipper::listMethods();
      
      $unique_order_id = $this->generateShippingUniqueId($order->id);
      
      $trackingInfo['image'] = '';
      $trackingInfo['unique_order_id'] = $unique_order_id;
      $trackingInfo['tracking_number'] = $track_number;
      $trackingInfo['mail_class'] =  $methods[$method];
      $trackingInfo['shipping_id'] =  $track_number;
      $trackingInfo['type'] = 'Manually Entered';
      
      $shipment = $this->insertTracking($trackingInfo, $item_ids, $order->id, 0, $order->order_id); 
    } else {
      $unique_order_id = $shipment->unique_order_id;
      $shipment->shipping_unique_id = null;
      $shipment->save();
    }
    
    $label = null;
    $reminder = null;
    
    if ($order->store->ship_label == '1') { 
      $className =  'Market\\' . $order->store->class_name; 
      $controller =  new $className;
      $store_info = $controller->shipLabel($order->store->store_id, $shipment->unique_order_id, $order->order_id);
      
      if (is_array($store_info)) {
        
        $reminder = $store_info[0];
        $labels = str_replace(array("'", "\"", "&quot;"), " ", htmlspecialchars( $store_info[1] ));
        
        $this->saveLabels($labels, $shipment->unique_order_id);
      }
    }
    
    return ['unique_order_id' => $unique_order_id, 'reminder' => $reminder];
  }
  
  public function voidShipment($shipment_id)
  {
    $shipment = Ship::find($shipment_id);
    
    if (!$shipment) {
      return 'Error: Shipment not Found';
    }
    
    $mail_class = explode(' ', $shipment->mail_class);
    $store = $shipment->order->store;
    
    if ($mail_class[0] == 'UPS') {
      $carrier = new UPS;
      $response = $carrier->void($store, $shipment->shipping_id); 
    } elseif ($mail_class[0] == 'FEDEX') {
      $carrier = new FEDEX;
      $response = $carrier->void($store, $shipment->shipping_id); 
    } elseif ($mail_class[0] == 'USPS') {
      $carrier = new Post;
      $response = $carrier->void($store, $shipment->shipping_id); 
    } else {
      Log::error('Unrecognized Carrier ' . $mail_class[0] . ' - ' . $shipment->unique_order_id);
      $response = 'Unrecognized Carrier';
    }
    
    $shipment->order->order_status = 4;
    $shipment->order->save();
    
    foreach($shipment->items as $item) {
      if ($item->item_status == 2) {
        $item->item_status = 1;
        $item->tracking_number = null;
        $item->save();
      }
    }
    
    $shipment->is_deleted = '1';
    $shipment->save();
    
    Order::note('Shipment ' .  $shipment->shipping_id . ' voided', $shipment->order_number);
    
    return $response;
  }
  
  private function generateShippingUniqueId ($order_id)
  {
    $ships = Ship::where('order_number', $order_id)
           ->get()
           ->pluck('unique_order_id', 'id')
           ->toArray();
           
    $lines = count(array_unique($ships));
    
    if ( $lines == 0 ) {
      return sprintf("%s-%s", $order_id, $lines);
    } else {
      $lastNumber = [];
      $lines = array_unique($ships);

      foreach ( $lines as $line ) {
        $lastNumber[] = (int) substr($line, -1);
      }
      $maxLastNumber = max($lastNumber);

      return sprintf("%s-%s", $order_id, $maxLastNumber + 1);
    }

  }
  
  private function saveLabels($graphicImage, $unique_order_id) {
    
    $lock_path = public_path('assets/images/shipping_label/');
    $myfile = fopen($lock_path . $unique_order_id . ".zpl", "wb") or die("Unable to open file!");
    fwrite($myfile, $graphicImage);
    fclose($myfile);
    return True;
  }
  
  private function packingSlip($order, $item_ids)
  {
    $name = Helper::removeSpecial($order->customer->ship_full_name);
    
    $header = "^XA^FX^CF0,^POI 70^FO50,50^FDOrder $order->purchase_order^FS";
    $header .= "^CF0,40^FO50,110^FDCustomer: $name^FS^FO50,150^GB700,1,3^FS";

    $header2 = "^XA^FX^CF0, 70^FO50,50^FDOrder $order->purchase_order^FS";
    $header2 .= "^CF0,40^FO50,110^FDCustomer: $name^FS^FO50,150^GB700,1,3^FS";




      $zpl = $header;
    
    $inshipment = array();
    $shipped = array();
    $unshipped = array();
    
    foreach($order->items as $item) {
      if (in_array($item->id, $item_ids)) {
        $inshipment[] = $item;
      } elseif ($item->item_status == 'shipped') { 
        $shipped[] = $item;
      } else { 
        $unshipped[] = $item;
      }
    }
    
    $inshipment_header = "^CF0,40^FO50,165^FDIn This Shipment:^FS^FO50,200^GB700,1,3^FS"; 
    $inshipment_header .= "^CF0,30^FO50,210^FDItem^FS^FO150,210^FDDescription ^FS^FO690,210^FDQTY ^FS^FO50,235^GB700,1,3^FS";
    
    $zpl .= $inshipment_header;
    
    $pointer = 245;
    $header_printed = 0;
    
    foreach($inshipment as $item) {
      
      if ($pointer > 1000 || $header_printed == 0) {
        if ($pointer > 1000) {
          $zpl .= "^XZ^XA";
        }
        $zpl .= $header2 . $inshipment_header;
        $pointer =  245;
        $header_printed = 1;
      }
      
      $zpl .= "^CF0,28^FO50,$pointer^FD$item->id^FS^FO150,$pointer^FD$item->item_description^FS";
      $zpl .= "^FO700,$pointer^FD$item->item_quantity^FS";
      $pointer += 30;
      $zpl .= "^FD^FO150,$pointer^FB550,6,,^FD" . Helper::optionTransformer($item->item_option, 1, 0, 0, 1, 0, ' ') . "^FS";
      $pointer += 150;
    }
    
    
    
    if (count($shipped) > 0) {
        
        $header_printed = 0;
        
        foreach($shipped as $item) {
          if ($pointer > 1050 || $header_printed == 0) {
            if ($pointer > 1050) {
              $zpl .= "^XZ^XA";
            }
            $zpl .= "^FO50,$pointer^GB700,1,3^FS";
            $pointer += 30;
            
            $zpl .= "^CF0,40^FO50,$pointer^FDPreviously Shipped:^FS";
            $pointer += 35;
            $zpl .= "^FO50,$pointer^GB700,1,3^FS"; 
            $pointer += 10;
            $zpl .= "^CF0,30^FO50,$pointer^FDItem^FS^FO150,$pointer^FDDescription ^FS^FO690,$pointer^FDQTY ^FS";
            $pointer += 25;
            $zpl .= "^FO50,$pointer^GB700,1,3^FS";
            $pointer += 10;
            $header_printed = 1;
          }
          $zpl .= "^CF0,22^FO50,$pointer^FD$item->id^FS^FO150,$pointer^FD$item->item_description^FS^FO700,$pointer^FD$item->item_quantity^FS";
          $pointer += 30;
        }
    }
    
    if (count($unshipped) > 0) {
        
        $header_printed = 0;
        
        foreach($unshipped as $item) {
          
          if ($pointer > 1050 || $header_printed == 0) {
            if ($pointer > 1050) {
              $zpl .= "^XZ^XA";
            }
            $zpl .= "^FO50,$pointer^GB700,1,3^FS";
            $pointer += 30;
            
            $zpl .= "^CF0,40^FO50,$pointer^FDNot Yet Shipped:^FS";
            $pointer += 35;
            $zpl .= "^FO50,$pointer^GB700,1,3^FS"; 
            $pointer += 10;
            $zpl .= "^CF0,30^FO50,$pointer^FDItem^FS^FO150,$pointer^FDDescription ^FS^FO690,$pointer^FDQTY ^FS";
            $pointer += 25;
            $zpl .= "^FO50,$pointer^GB700,1,3^FS";
            $pointer += 10;
            $header_printed = 1;
          }
          
          $zpl .= "^CF0,22^FO50,$pointer^FD$item->id^FS^FO150,$pointer^FD$item->item_description^FS^FO700,$pointer^FD$item->item_quantity^FS";
          $pointer += 30;
        }
    }
    
    $zpl .= "^XZ";
    
    return $zpl;
  }

private function updateTrackingInMagento($order_number, $trackingNumber, $mailClass){

    // jSON URL which should be requested
    $json_url = "http://www.pws-services.net/statusupdate";

    if(env("GRAPHICS_ENV") == "devgraphics"){
        $json_url = "http://www.magento.monogramonline.us/statusupdate";
    }

    $orderStatusData["apiUser"] = "collage";
    $orderStatusData["apiPass"] = "Collage99~!";
    $orderStatusData["orderId"] = $order_number;   //1216964;
    $orderStatusData["carrierTitel"] = $mailClass; // "UPS Mail Innovation";
    $orderStatusData["addTracking"] = $trackingNumber; // "wJ346543645675467";



    $queryUrl = http_build_query($orderStatusData, 'flags_');

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $json_url);
    curl_setopt($curl, CURLOPT_POST, TRUE);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $queryUrl);
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, FALSE);
    curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
// Submit the POST request
    $result = curl_exec($curl);
    curl_close($curl);
}

  private function insertTracking ($trackingInfo, $item_ids, $order_number, $total_amount, $note_order, $packages = [0])
  {
    if ( $trackingInfo['unique_order_id'] ) {
      
      $order = Order::find($order_number);
      $short_order = $order->short_order;
      
      $ship = new Ship;
      $ship->order_number = $order_number;
      $ship->unique_order_id = $trackingInfo['unique_order_id'];
      if ($order) {
        $ship->store_id = $order->store_id;
      }
      $ship->tracking_number = $trackingInfo['tracking_number'];
      if (isset($trackingInfo['type'])) {
        $ship->tracking_type = $trackingInfo['type'];
      }
      if (isset($trackingInfo['transaction_id'])) {
        $ship->transaction_id = $trackingInfo['transaction_id'];
      }
      $ship->shipping_id = $trackingInfo['shipping_id'];
      $ship->mail_class = $trackingInfo['mail_class'];
      $ship->post_value = $total_amount;
      $ship->postmark_date = date("Y-m-d");
      $ship->transaction_datetime = date("Y-m-d H:i:s");
      $ship->actual_weight = array_sum($packages);
      $ship->package_count = count($packages);
      $ship->user_id = auth()->user()->id;
      $ship->save();
      
      $items = Item::whereIn('id', $item_ids)->get();
      foreach ($items as $item) {
        $item->tracking_number = $trackingInfo['tracking_number'];
        $item->item_status = 2;
        $item->save();
        
        if ($item->batch_number != '0'){
          Batch::isFinished($item->batch_number);
        }
        
        $relation = new ItemShip;
        $relation->item_id = $item->id;
        $relation->ship_id = $ship->id;
        $relation->save();
      }
      
      $items_left = Item::where('order_5p', $order_number)
              ->searchStatus('shippable')
              ->where('is_deleted', '0')
              ->count();
      
      if ($items_left == 0) {
        $order->order_status = 6;
        $order->save();
      } elseif ($order->order_status == 10) {
        $order->order_status = 4;
        $order->save();
        
      }
        
      // Add note history by order id
      Order::note("Update Tracking# " . $trackingInfo['tracking_number'], $order_number, $note_order);

      $this->updateTrackingInMagento($short_order, $trackingInfo['tracking_number'],$trackingInfo['mail_class']);

      return $ship;
    }
  }
  
  public static function isAddressVerified ($customer)
  {
    $state = Helper::stateAbbreviation($customer->ship_state);
    $country_code = self::getcountrycode($customer->ship_country);
    
    if ( ! $country_code ) {
      return FALSE;
    } 
    
    if ($country_code != 'US') {
      return TRUE;
    }
    
    $usps = new USPS;
    
    $val = $usps->validate($customer->ship_address_1, $customer->ship_address_2, 
                            $customer->ship_zip, $customer->ship_city, $customer->ship_state);
    
    if ($val) {
      
      $customer->ship_address_1 = $val->Address2;
      $customer->ship_address_2 = $val->Address1;
      $customer->ship_city = $val->City;
      $customer->ship_state = $val->State;
      $customer->ship_zip = $val->Zip5 . '-' . $val->Zip4;
      $customer->save();
      return TRUE;
      
    } else {
      return FALSE;
    }
  }
  
  // public static function isAddressVerified ($customer)
  // {
  //   $order = $customer->order;
  //   $country_code = self::getcountrycode($customer->ship_country);
  //   if ( ! $country_code ) {
  //     $message = 'Order number ' . $order->order_id . ' invalid country code <b>' . $customer->ship_country . '</b><br>Please update correct country code format like<br><b>US United States</b><br><b>CA Canada</b><br><b>VI Virgin Islands (U.S.)</b>';
  //     throw new \Exception($message);
  //   }
  // 
  //   if ($country_code != 'US') {
  //     return true;
  //   }
  // 
  //   try {
  //     $address = new Address();
  //   } catch ( \Exception $e ) {
  //     throw $e;
  //   }
  //   $address->setAttentionName($customer->ship_full_name);
  //   $address->setBuildingName($customer->ship_company_name);
  //   $address->setAddressLine1($customer->ship_address_1);
  //   $address->setAddressLine2($customer->ship_address_2);
  //   $address->setAddressLine3('');
  //   $address->setStateProvinceCode($customer->ship_state);
  //   $address->setCity($customer->ship_city);
  //   $address->setCountryCode(self::getcountrycode($customer->ship_country));
  //   $address->setPostalCode($customer->ship_zip);
  //   // shipmentDigest
  //   // Ptondereau\LaravelUpsApi\UpsApiServiceProvider
  //   $addressVerification = new AddressValidation(env('UPS_ACCESS_KEY'), env('UPS_USER_ID'), env('UPS_PASSWORD'));
  //   $addressVerification->activateReturnObjectOnValidate(); //This is optional
  //   try {
  //     $response = $addressVerification->validate($address, $requestOption = AddressValidation::REQUEST_OPTION_ADDRESS_VALIDATION, $maxSuggestion = 15);
  // 
  //     if ( $response->isValid() ) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  // 
  //     if ( $response->isAmbiguous() ) {
  //       return false;
  //     }
  // 
  //   } catch ( \Exception $e ) {
  //     throw  $e;
  //   }
  // }


  public static function getcountrycode ($country_name)
  {
    $country_code_null = substr($country_name, 2, 1);
    if ( $country_code_null == " " ) {
      
      $country_code = substr($country_name, 0, 2);
      return $country_code;
      
    } elseif ( strtolower(substr($country_name, 0, 2)) == "us" ) {
      
      return 'US';
      
    } elseif (strtolower($country_name) == 'united states') {
      
      return 'US';
      
    } elseif (strlen($country_name) == 2) {
      
      return $country_name;
      
    } else {
      
      return false;
    }

  }
  
  public static function listMethods ($carrier = null)
  {
    $methods = array(       '' => 'DEFAULT SHIPPING',
                            'MN*' => 'MANUAL SHIPPING',
                            'US*FIRST_CLASS' => 'USPS FIRST_CLASS',
                            'US*PRIORITY' => 'USPS PRIORITY',
                            'US*EXPRESS' => 'USPS EXPRESS',
                            'UP*S_GROUND' => 'UPS GROUND',
                            'UP*S_3DAYSELECT' => 'UPS 3DAYSELECT',
                            'UP*S_AIR_2DAY' => 'UPS AIR_2DAY',
                            'UP*S_AIR_2DAYAM' => 'UPS AIR_2DAYAM',
                            'UP*S_AIR_1DAYSAVER' => 'UPS AIR_1DAYSAVER',
                            'UP*S_AIR_1DAY' => 'UPS AIR_1DAY',
                            'UP*S_SUREPOST' => 'UPS SUREPOST',
                            'FX*_SMART_POST' => 'FEDEX SMARTPOST',
                            'FX*_GROUND_HOME_DELIVERY' => 'FEDEX GROUND_HOME_DELIVERY',
                            'FX*_FEDEX_GROUND' => 'FEDEX GROUND',
                            'FX*_FEDEX_2_DAY' => 'FEDEX 2_DAY',
                            'FX*_FEDEX_EXPRESS_SAVER' => 'FEDEX EXPRESS_SAVER',
                            'FX*_STANDARD_OVERNIGHT' => 'FEDEX STANDARD_OVERNIGHT',
                            'FX*_PRIORITY_OVERNIGHT' => 'FEDEX PRIORITY_OVERNIGHT', 
                      );
    
    return $methods;
  }
  
  public static function importWS ($order_id)
  {
    //look for record in ws import table
  }
}