<?php

namespace Monogram;

use App\Batch;
use App\BatchRoute;
use App\Http\Controllers\WasatchDashboardController;
use App\WasatchDashboard;
use Illuminate\Support\Facades\Log;
use Monogram\FileHelper;
use Monogram\SFTPConnection;

class Wasatch {
  
  protected $units = array();
  protected $QUEUES = null;
  
  protected $staging    = [ '1' => '/wasatch/staging-1/',
                            '2' => '/wasatch/staging-2/',
                            '3' => '/wasatch/staging-3/',
                            '4' => '/wasatch/staging-4/',
                            '5' => '/wasatch/staging-5/',
                            '6' => '/wasatch/staging-6/',
                            '7' => '/wasatch/staging-7/',
                            '8' => '/wasatch/staging-8/',
                            '9' => '/wasatch/staging-9/'
                            ];

  protected $hotfolders = [ '1' => '/media/graphics/SOFT-1/',
                            '2' => '/media/graphics/SOFT-2/',
                            '3' => '/media/graphics/SOFT-3/',
                            '4' => '/media/graphics/SOFT-4/',
                            '5' => '/media/graphics/SOFT-5/',
                            '6' => '/media/graphics/SOFT-6/',
                            '7' => '/media/graphics/SOFT-7/',
                            '8' => '/media/graphics/SOFT-8/',
                            '9' => '/media/graphics/SOFT-9/'
                            ];

  protected $conf = [ 'SOFT' => '720v_Yarrington_Bodyflex_DS_Transfer_Production', 
                      'HARD' => '720x1440_Chromaluxe_Gloss_White_DS_Transfer_Multi_Purpose',
                      'EPSON' => '720v_2pass_Yarrington_Bodyflex_DS_Transfer_Production'];
  // 720v_2pass_Yarrington_Bodyflex_DS_Transfer_Production_63_Plus30
                      
  #protected $prefix = '//10.10.0.87/Graphics';
  // protected $prefix = 'W:/Wasatch';
    protected $prefix = 'http://devoms.monogramonline.us/media/graphics';

//  protected $prefix = '//10.10.0.11/Graphics'; /media/graphics/archive/1/1-0.jpg  //10.10.0.11/Graphics//archive/7/7-0.jpg
//  protected $prefix = '/media/graphics';
  
  protected $ip = 'http://10.10.0.80/';

  protected $dashboard_ip = 'http://96.57.0.133/';

  protected $system_setup = '/wasatch/setup.xml';

  public function __construct ()
    {
        if(env("GRAPHICS_ENV") == "devgraphics"){
            $this->hotfolders = [ '1' => '/media/devgraphics/SOFT-1/',
                                '2' => '/media/devgraphics/SOFT-2/',
                                '3' => '/media/devgraphics/SOFT-3/'];
        }
    }

  private function simpleRequest($url) {
    
    try {
      $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL,$url);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
      $xml=curl_exec($ch);
      curl_close($ch);
    } catch (\Exception $e) {
      Log::info('Wasatch Curl Error: ' . $e->getMessage());
      return false;
    }
    
    if($xml != 404) { 
      try {
        $doc = simplexml_load_string($xml);
        $json = json_encode($doc);
//          Log::info('XXXEEEE Curl result: ' . json_decode($json,TRUE));
        return json_decode($json,TRUE);
      } catch (\Exception $e) {
        Log::info('Wasatch XML Decode Error: ' . $e->getMessage());
        return false;
      }
    } else {
      Log::info('Wasatch 404 Error');
      return false;
    }
  }
  
  public function getUnits() {
    
    $response = $this->simpleRequest($this->ip . 'xmlSystem.dyn');
    
    foreach ($response['PRINTUNIT'] as $line) {
      $this->units[$line['@attributes']['number']] = $line['@attributes']['name'];
    }

    if (is_array($this->units)) {
      return $this->units;
    }
  
    return null;
  }
  
  public function getQueues()
  {
     return [
    "PRINTER_1" =>     [
                        "STAGED_XML" =>  [],
                        "HOT_FOLDER" => [],
                        "RIP_QUEUE" => [],
                        "PRINT_QUEUE" => [
                                  '9664000' => "X01_410881_Layout",
                                  '9667000' => "X01_410881_pdf_000",
                                  '9668000' => "X01_410881_soft_pen_sub_DOG_1_eps_000",
                                  '9669000' => "R01_413264_Layout",
                                  '9672000' => "R01_413264_pdf_000",
                                  '9673000' => "R01_413264_soft_pen_sub_DOG_1_eps_000"
                                ],
                        "TOTAL" => 123
                      ],
      "PRINTER_2" =>  [
                        "STAGED_XML" => [],
                        "HOT_FOLDER" => [],
                        "RIP_QUEUE" => [],
                        "PRINT_QUEUE" => [
                          '8434000' => "Untitled-2",
                          '8540000' => "Untitled-1",
                          '9146000' => "LIP3",
                          '9147000' => "LIP3-sequin_pillow",
                          '9153000' => "Untitled-13"
                        ],
                        "TOTAL" => 3
                      ],
    "PRINTER_3" => [
                    "STAGED_XML" => [],
                    "HOT_FOLDER" => [
                          2 => "Thumbs.db"
                      ],
                    "RIP_QUEUE" => [],
                    "PRINT_QUEUE" => [],
                    "TOTAL" => 1
              ]
];

    $this->QUEUES = array();

    foreach ($this->hotfolders as $i => $hotfolder) {

      $xml = '<?xml version="1.0" encoding="utf-8"?><WASATCH ACTION=SYSTEM>' .
              '<SETANNOTATIONBARCODE PRINTUNIT=' . $i . '>IDAutomationHC39XS,18.0</SETANNOTATIONBARCODE>' .
              '</WASATCH>';

      $setup = $this->simpleRequest($this->ip . 'xmlSubmission.dyn?' . $xml);

      $unit = 'PRINTER_' . $i;

      if (!file_exists($this->hotfolders[$i])) {
        Log::error('Wasatch: Hotfolder does not exist - ' . $this->hotfolders[$i]);
        return 'Error: Hotfolder does not exist';
      }

      $this->QUEUES[$unit]['STAGED_XML'] = array_diff(scandir(storage_path() . $this->staging[$i]), array('..', '.'));

      FileHelper::removeEmptySubFolders($this->hotfolders[$i]);

      $this->QUEUES[$unit]['HOT_FOLDER'] = array_diff(scandir($this->hotfolders[$i]), array('..', '.'));

      $input = $this->simpleRequest($this->ip . 'xmlQueueStatus.dyn?PRINTUNIT=' . $i);

      if ($input === false) {
        Log::error('Wasatch: Cannot retrieve Queue Info');
        return 'Error: Cannot retrieve Wasatch queues';
      }

      $this->QUEUES[$unit]['RIP_QUEUE'] = array();
      $this->QUEUES[$unit]['PRINT_QUEUE'] = array();

      $RIP_TOTAL = array();

      if (isset($input['RIPQUEUE']['ITEM'])) {
        foreach ($input['RIPQUEUE']['ITEM'] as $item) {
          if (isset($item['@attributes']['job'])) {
            $this->QUEUES[$unit]['RIP_QUEUE'][$item['@attributes']['job']] = $item['@attributes']['job'];
            try {
              $RIP_TOTAL[] = substr($item['@attributes']['job'], 0, strpos(str_replace('-', '_', $item['@attributes']['job']), '_', 4));
            } catch (\Exception $e) {
              $RIP_TOTAL[] = substr($item['@attributes']['job'], 0, strpos(str_replace('-', '_', $item['@attributes']['job']), '_'));
            }
          }
        }
      } else {
        $this->QUEUES[$unit]['RIP_QUEUE'] = [];
      }

      $PRINT_TOTAL = array();

      if (isset($input['PRINTQUEUE']['ITEM'])) {
        foreach ($input['PRINTQUEUE']['ITEM'] as $item) {
          if (isset($item['@attributes']['notes'])) {
            $this->QUEUES[$unit]['PRINT_QUEUE'][$item['@attributes']['index']] = $item['@attributes']['notes'];
            try {
              $PRINT_TOTAL[] = substr($item['@attributes']['notes'], 0, strpos(str_replace('-', '_', $item['@attributes']['notes']), '_', 4));
            } catch (\Exception $e) {
              $PRINT_TOTAL[] = substr($item['@attributes']['notes'], 0, strpos(str_replace('-', '_', $item['@attributes']['notes']), '_'));
            }
          }
        }
      } else {
        $this->QUEUES[$unit]['PRINT_QUEUE'] = [];
      }

      $this->QUEUES[$unit]['TOTAL'] = count($this->QUEUES[$unit]['STAGED_XML']) +
                                      count($this->QUEUES[$unit]['HOT_FOLDER']) +
                                      count(array_unique($RIP_TOTAL)) +
                                      count(array_unique($PRINT_TOTAL));
    }

    return $this->QUEUES;
  }

  public function getPrintAndRipQueues($pcName)
    {
        #----------------------------------------------DASHBOARD---------------------------------------------------------------------------------

        switch ($pcName) {
            case "getwasatch11status":
                $url = 'http://order.monogramonline.com/getwasatch11status';
                break;
            case "getwasatch23status":
                $url = 'http://order.monogramonline.com/getwasatch23status';
                break;
            case "getwasatch130status":
                $url = 'http://order.monogramonline.com/getwasatch130status';
                break;
            default:
                $url = 'http://order.monogramonline.com/getwasatchstatus';
        }

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url);
        $response->getStatusCode(); # 200
        $response->getHeaderLine('content-type');
        $queue = json_decode($response->getBody(), true);

        return $queue;
        #----------------------------------------------DASHBOARD---------------------------------------------------------------------------------
    }

    public function checkPrinQueu($queues){
        $ripQueue=[];
        foreach ($queues as $printJobs) {
            if (isset($printJobs["RIPQUEUE"]["ITEM"])) {

                foreach ($printJobs["RIPQUEUE"]["ITEM"] as $items) {
                    if(isset($items["@attributes"]["job"])){
                        echo "<pre>";
//                        print_r($items, true);
                        $getUnit = explode(":", $printJobs["@attributes"]["name"]);
                        $getUnit = substr($getUnit[0], -1);
                        echo "name = ".$printJobs["@attributes"]["name"];
                        $ripQueue[$getUnit][]= $items["@attributes"]["job"];
                        print_r($items["@attributes"]["job"]);
//                        print_r($items["@attributes"]["number"]);
                        echo "---------------------------------------------------------";
                        echo "</pre>";
                    }
                }
            }
        }
        return $ripQueue;
    }

  public function clearJob ($batch) {
    
    $units = $this->getQueues();
    
    if (!is_array($this->QUEUES)) {
      return $this->QUEUES;
    }
    
    $indexes = array();
    
    foreach($this->QUEUES as $unit_name => $unit) { 
      $num = substr($unit_name, -1);
      foreach($unit as $queue_name => $queue){
        if (is_array($queue)) {
          foreach($queue as $index => $item) {
            if (strpos($item, $batch) !== false) {
              if ($queue_name == 'STAGED_XML') {
                unlink(storage_path() . $this->staging[$num] . $item);
              } else if ($queue_name == 'HOT_FOLDER') {
                unlink($this->hotfolders[$num] . $item);
              } else if ($queue_name == 'RIP_QUEUE') {
                $this->delete($num, 'job', $index, $batch);
              } else if ($queue_name == 'PRINT_QUEUE') {
                $this->delete($num, 'index', $index, $batch);
              }
            }
          }
        }
      }
    }
    
    return '1';
  }
  
  private function delete ($unit, $type, $id, $note) {
    
    $xml = null;
        
    $xml  = '<?xml version="1.0" encoding="utf-8"?><WASATCH ACTION="JOB">';
    
    $xml .= '<DELETE printunit=' . $unit . ' ' . $type . '="' . $id . '" />';

    $xml .= '</WASATCH>';
    
    $newfile = fopen($this->hotfolders[$unit] . $id . '.xml', "w");
    fwrite($newfile, $xml);
    fclose($newfile);
    
    $newfile = fopen('/media/'.env("GRAPHICS_ENV").'/temp/' . $id . '.xml', "w");
    fwrite($newfile, $xml);
    fclose($newfile);
    
    if (auth()->user()) {
      Log::info(auth()->user()->username . '-' . $id . ' Deleted - ' . $note);
    } else {
      Log::info($id . 'Deleted - ' . $note);
    }
    
    return;
  }
  
  public function notInQueue($batch) {
      return '1';
    // Check is Batch already in Wasas Queue
    if ($this->QUEUES == null) {
      $units = $this->getQueues();
    }
    
    if (!is_array($this->QUEUES)) {
      return $this->QUEUES;
    }
    
    $length = strlen($batch);
    $underscore_batch = str_replace('-', '_', $batch);
    
    foreach($this->QUEUES as $unit_name => $unit) { 
      foreach($unit as $queue_name => $queue){
        if (is_array($queue)) {
          foreach($queue as $item) {
            if (substr($item, 0, $length) == $batch || substr($item, 0, $length) == $underscore_batch) {
              // Log::error('Wasatch: Batch already in Queue - ' . $batch);
              return 'Batch in ' . str_replace('_', ' ', $unit_name . ' ' . $queue_name);
            }
          }
        }
      }
    }
    
    return '1';
  }

    private function isBackImage($path){
        #/media/graphics/archive/7/7-0.jpg
        $file = basename($path, ".jpg");
        $file = explode("-",$file);

        if(isset($file[1]) && (mb_substr($file[1], 0, 1) > 0)){
            return true;
        }else{
            return false;
        }
    }

    private function getRotate($info, $orientation){

        if(($info['type'] != ".pdf") && ($orientation == 1) && ($info['mirror'] == 0)) {
            return 90;
        }elseif(($info['type'] != ".pdf") && ($orientation == 1) && ($info['mirror'] == 1) && ($this->isBackImage($info['file']) === false)) {
            return 90;
        }elseif(($info['type'] != ".pdf") && ($orientation == 1) && ($info['mirror'] == 1) && $this->isBackImage($info['file'])) {
            return 270;
        }else{
            return 0;
        }
    }


    public function printJob ($files, $barcode, $qty, $hotfolder, $imgconf, $orientation, $width = null) {

        $wasatchDashboardController = new WasatchDashboardController;
        $dashboardJpgPdf = $wasatchDashboardController->getWasatchFormatfileName($files);

        $this->prefix = 'http://'.Sure3d::getEnv().'.monogramonline.us/media/graphics';

        // Load route info by Batch ($barcode);
        $batchRoutes = Batch::getBatchWitRoute($barcode);
        $batchMaxUnits = $batchRoutes->route->batch_max_units;
        $aB = $batchRoutes->route->a_b;
        $nesting = $batchRoutes->route->nesting;

//        if((count($files) -1) == ($batchMaxUnits * 2) && ($aB == 1) && ($nesting == 0)){
        if(($aB == 1) && ($nesting == 0)){
            $i = 0;
            $ii = 1;
            foreach ($files as $name => $info) {
                if($info['type'] == ".pdf"){
                    $groupPdf[$name] = $info;
                    continue;
                }

                if($i % 2 == 0){
                    $info['itmem_info']['group']= (($ii+1) / 2) ."-A";
                    $groupA[$name] = $info;
                }else{
                    $info['itmem_info']['group']= ($ii / 2) ."-B";
                    $groupB[$name] = $info;
                }
                $i++;
                $ii++;
            }

            $groupA = array_reverse($groupA);
            $groupB = array_reverse($groupB);
            $files =array_merge($groupB, $groupA, $groupPdf);
        }elseif((count($files) -1) == ($batchMaxUnits * 2) && ($aB == 1) && ($nesting == 1)){
            $i = 0;
            $ii = 1;
            foreach ($files as $name => $info) {
                if($info['type'] == ".pdf"){
                    $groupPdf[$name] = $info;
                    continue;
                }

                if($i % 2 == 0){
                    $info['itmem_info']['group']= (($ii+1) / 2) ."-A";
                    $groupA[$name] = $info;
                }else{
                    $info['itmem_info']['group']= ($ii / 2) ."-B";
                    $groupB[$name] = $info;
                }
                $i++;
                $ii++;
            }

            $files = [];
            $files[0] = $groupA;
            $files[1] = $groupB;
            $files[2] = $groupPdf;
        }elseif ($nesting == 1){

            $i = 0;
            $ii = 1;
            $groupA = [];
            $groupB = [];
            $x =0;
            $y =0;
            $imageHight = 0;




            foreach ($files as $name => $info) {


                for ($xx = 1; $xx <= $info['line_item_qty']; $xx++) {
                    if($info['type'] == ".pdf"){
                        $filess[$name] = $info;
                    }else{
                        $filess[$name."_".$xx] = $info;
                    }
                }
            }


            foreach ($filess as $name => $info) {
                if($info['type'] == ".pdf"){
                    $groupPdf[$name] = $info;
                    continue;
                }

                    if($i % 2 == 0){
                        $x =0;
                        $info['itmem_info']['group']= "-A";
                        $info['itmem_info']['x']= $x;
                        $info['itmem_info']['y']= $y;
                        $info['itmem_info']['i']= $i;
                        $groupA[$name] = $info;
                        $x= $info['width'];
                        $imageHight = $info['height'];

                    }else{
                        $info['itmem_info']['group']= "-A";
                        $info['itmem_info']['x']= $x;
                        $info['itmem_info']['y']= $y;
                        $info['itmem_info']['i']= $i;
                        $groupA[$name] = $info;
                        $imageHight = 0;
                        $y = $y+$info['height'];

                    }

                $i++;
                $ii++;
            }

            $groupPdf["/summaries/".$barcode.".pdf"]['itmem_info']['x'] = 0;
            $groupPdf["/summaries/".$barcode.".pdf"]['itmem_info']['y'] = $y + $imageHight+ $groupPdf["/summaries/".$barcode.".pdf"]['height']+ 2.99;
            $files = array_merge($groupA,$groupPdf);

        }


    $imgconf = "EPSON";
    $xml = null;
    ###################### nesting ######################
    if($nesting == 0) {
        $xml = '<?xml version="1.0" encoding="utf-8"?>';
        $xml .= '<WASATCH ACTION="JOB">';
        $xml .= '<LAYOUT NOTES="' . $barcode . ' Layout">';


        if(($aB == 1) && ($nesting == 0)){
            $xml .= '<Copies>1</Copies>';
        }else{
            $xml .= '<Copies>' . $qty . '</Copies>';
        }

        $y = 0;
        $jpg_count = 0;
        $pdf_count = 0;
        $total_files = count($files);
        foreach ($files as $name => $info) {
            ##########
            $lineInfo = "";
            $itemId = "";

            if ($info['type'] == ".pdf") {
//                $name = str_replace("summaries", "Collage_summaries", $name);
            } else {
                // $name = /var/www/oms/public_html/media/graphics/archive/R01-24790/R01-24790-1.jpg
                $name = str_replace("/var/www/oms/public_html/media/graphics", "", $name);
                ######## Line Info's #########

                if (isset($info['itmem_info'])) {
                    if (!empty($info['itmem_info'])) {
                        $lineInfo = "------------------------------------------------------------------------" . $info['itmem_info']['group'] . ", ---------- Line# " . $info['itmem_info']['item_id'] . ", ---------- Batch# " . $info['itmem_info']['batch_id'] . ", ---------- Order Date: " . $info['itmem_info']['order_date'];
                        $lineInfo .= $lineInfo;
                        $itemId = $info['itmem_info']['item_id'];
                    }
                }
                ######## Line Info's #########
            }
            ##########
            if ($info['type'] == ".jpg") {
                $jpg_count++;
            }
            if ($info['type'] == ".pdf") {
                $pdf_count++;
            }


            $xml .= '<PAGE XPOSITION="0.0" YPOSITION="' . number_format($y, 1) . '">';

//            if(isset($info['itmem_info']['sku']) && in_array($info['itmem_info']['sku'], $this->getOldXmlFormatSku())){
//                $xml .= '<URL>' . $this->prefix . $name . '</URL>'; //  /10.10.0.87/Graphics . /archive/7/7-0.jpg
//            }else{
                $xml .= '<FILENAME>' . $this->prefix . $name . '</FILENAME>'; //  /10.10.0.87/Graphics . /archive/7/7-0.jpg
//            }


            #$xml .= '<IMGCONF>' . $this->conf[$imgconf] . '</IMGCONF>';   // $this->conf[SOFT] = 720v_Yarrington_Bodyflex_DS_Transfer_Production
            $xml .= '<Copies>0</Copies>';
            $xml .= '<ANNOTATE><COMMENT>' . $lineInfo . '</COMMENT><BARCODE>' . $barcode . '</BARCODE></ANNOTATE>'; // 7

            $xml .= '<Rotate>' . $this->getRotate($info, $orientation) . '</Rotate>';


            $xml .= '<Scale>' . $info['scale'] . '</Scale>';      // 100

            if ($info['type'] == ".pdf") {
                $xml .= '<Mirror>0</Mirror>';
            }else{
                $xml .= '<Mirror>1</Mirror>';
            }

            $xml .= '<DELETEAFTERRIP /><DELETEAFTERPRINT />';


            $xml .= '</PAGE>';


            if (($info['type'] != ".pdf") && ($orientation == 1)) {
                $y += $info['width'] + 2.99;
            } else {
                $y += $info['height'] + 2.99;
            }

//      Log::info('printJob: Batch No ' . $barcode . ' - Image Name = ' . $name. ' ');
//      Log::info('printJob: info ' . print_r($info,true));

        }
//        if($hotfolder != 3) {
//            $xml .= '<DELETEAFTERPRINT />';
//        }
        $xml .= '</LAYOUT></WASATCH>';
    }elseif ($nesting == 1){
        $xml = '<?xml version="1.0" encoding="utf-8"?>';
        $xml .= '<WASATCH ACTION="JOB">';
        $xml .= '<LAYOUT NOTES="' . $barcode . ' Layout">';
        $qty = 1;
        $xml .= '<Copies>1</Copies>';
        $jpg_count = 0;
        $pdf_count = 0;
        $total_files = 0;

        $i = 0;
        foreach ($files as $key => $filess) {

            $total_files += count($filess);
//            $height = reset($filess);
//            dd($filess, $height);

            if((double)($filess['width'] * 2) > 64 ){
                dd("Image width ".$filess['width']." inch, its big, Please reduce size.");
            }


//                echo "<br> y = ".$y. " key = " .$key. " height = ". $filess['height'];
                $result = $this->createXml($filess, $key, $barcode,  $orientation);


            $jpg_count += $result['jpg_count'];
            $pdf_count += $result['pdf_count'];
            $xml .= $result['xml'];

            $i++;
        }
//        if($hotfolder != 3) {
//            $xml .= '<DELETEAFTERPRINT />';
//        }

        $xml .= '</LAYOUT></WASATCH>';
    }
    ###################### nesting ######################

    // Write XML file in =  /var/www/oms/storage/wasatch/staging-1/4.xml
    $newfile = fopen(storage_path() . $this->staging[$hotfolder] . $barcode . '.xml', "w"); // $hotfolder= 1,  $this->staging[$hotfolder] = /wasatch/staging-1/
//      Log::info("printJob XML file created at = ".storage_path() . $this->staging[$hotfolder] . $barcode . '.xml');
      fwrite($newfile, $xml);
    fclose($newfile);


        $wasatchDashboardController->compare($barcode,  $qty, $dashboardJpgPdf['jpg'], $dashboardJpgPdf['pdf'], $total_files, $hotfolder);


      #############20191011######################
        try {
            $host = "96.57.0.134";
            $sftp = new SFTPConnection($host, 22);
            $sftp->login('jewel', 'Dtw#123Dtw');
        } catch (\Exception $e) {
            Log::error('Wasatch: SFTP connection error ' . $e->getMessage());
            die("SFTP connection error". $e->getMessage());
            return FALSE;
        }

        try {

//            $files = $sftp->uploadDirectory( "/home/jewel/test/" , "/wasatch/staging-1/");
            if(env("GRAPHICS_ENV") == "devgraphics"){
                $files = $sftp->uploadFiles( [$barcode . '.xml'],"/media/Wasatch/DEVEpson-1/" , $this->staging[$hotfolder]);
            }else{
                $files = $sftp->uploadFiles( [$barcode . '.xml'],"/media/Wasatch/Epson-".$hotfolder."/" , $this->staging[$hotfolder]);
            }


        } catch (\Exception $e) {
            Log::error('Wasatch: SFTP upload error ' . $e->getMessage());
            die("SFTP  upload error ". $e->getMessage());
            return FALSE;
        }

      #############20191011######################

    if (auth()->user()) {
        Log::info("Print Sublimation xml created by " . auth()->user()->username . '-' . $barcode . '-' . $hotfolder);
    } else {
      Log::info('AutoPrint-' . $barcode . '-' . $hotfolder);
    }
      return;
  }

    private function getOldXmlFormatSku(){
          return ["SQ16-SLVR-INSERT-DBL", "SQ16-SLVR-INSERT", "SQ16-SLVR", "SQ16-SLVR-DBL"];
    }

  private function createXml($files, $name,$barcode,  $orientation){

      $this->prefix = 'http://'.Sure3d::getEnv().'.monogramonline.us/media/graphics';

//      $x = 0;
      $xml = '';
      $jpg_count = 0;
      $pdf_count = 0;

//      foreach ($filess as $name => $files) {
          ########## Rename path ##########
          $lineInfo = "";
          if ($files['type'] == ".pdf") {
//              $name = str_replace("summaries", "Collage_summaries", $name);
          } else {
              $xName = explode("_", $name);

//              $name = str_replace("archive", "Collage", $xName[0]);
              $name = str_replace("/var/www/oms/public_html/media/graphics", "", $xName[0]);
//                $name = $xName[0];
            ######## Line Info's #########

              if (isset($files['itmem_info'])) {
                  if (!empty($files['itmem_info'])) {
                      $lineInfo = "Line# " . $files['itmem_info']['item_id']. "------------------------------------------------------------------------" . $files['itmem_info']['group'] . ", ---------> Place ticket here: |  " . $files['itmem_info']['batch_id']. ",". date("m/d", strtotime($files['itmem_info']['order_date']))."  | Coloque el boleto aquí";
//                      $lineInfo .= $lineInfo;
                      $itemId = $files['itmem_info']['item_id'];
                  }
              }
              ######## Line Info's #########
          }
          ########## Rename path ##########
          ########## Count path for update dashboard ##########
          if ($files['type'] == ".jpg") {
              $jpg_count++;
          }
          if ($files['type'] == ".pdf") {
              $pdf_count++;
          }
          ########## Count path for update dashboard ##########
          $xml .= '<PAGE XPOSITION="' . number_format($files['itmem_info']['x'], 1) . '" YPOSITION="' . number_format($files['itmem_info']['y'], 1) . '">';

//          $xml .= '<URL>' . $this->prefix . $name . '</URL>'; //  /10.10.0.87/Graphics . /archive/7/7-0.jpg
          $xml .= '<FILENAME>' . $this->prefix . $name . '</FILENAME>'; //  /10.10.0.87/Graphics . /archive/7/7-0.jpg
          #$xml .= '<IMGCONF>' . $this->conf[$imgconf] . '</IMGCONF>';   // $this->conf[SOFT] = 720v_Yarrington_Bodyflex_DS_Transfer_Production
          $xml .= '<Copies>0</Copies>';
          $xml .= '<ANNOTATE><COMMENT>' . $lineInfo . '</COMMENT><BARCODE>' . $barcode . '</BARCODE></ANNOTATE>'; // 7

          $xml .= '<Rotate>' . $this->getRotate($files, $orientation) . '</Rotate>';


          $xml .= '<Scale>' . $files['scale'] . '</Scale>';      // 100

          if ($files['type'] == ".pdf") {
              $xml .= '<Mirror>0</Mirror>';
          }else{
              $xml .= '<Mirror>1</Mirror>';
          }

          $xml .= '<DELETEAFTERRIP /><DELETEAFTERPRINT />';
          $xml .= '</PAGE>';

          if (($files['type'] != ".pdf") && ($orientation == 1)) {
//              $x += $files['height'] + 0;
//              $y = 0; #$files['width'] + 2.99;
              dd("Image width = ".$files['height']." Landscape  not support, Please update Orientation to portrait");
          } else {
//              $x += $files['width'] + 0;
//              $y = 0; #$files['height'] + 2.99;
          }
//      }
      $result['jpg_count'] = $jpg_count;
      $result['pdf_count'] = $pdf_count;
      $result['xml'] = $xml;

      return $result;
  }
  public function stagedXml () {
    
    $units = $this->getQueues();
    
    foreach ($this->staging as $unit => $stage_dir) {
      if (!isset($units['PRINTER_' . $unit]) || 
            (count($units['PRINTER_' . $unit]['RIP_QUEUE']) == 0 && 
             count($units['PRINTER_' . $unit]['PRINT_QUEUE']) < 100 &&
             count($units['PRINTER_' . $unit]['HOT_FOLDER']) == 0))  {
              
        $files = glob( storage_path() . $stage_dir . '*.*' ); 
        if (count($files) > 0) {
          array_multisort(
            array_map( 'filemtime', $files ),
            SORT_NUMERIC,
            SORT_ASC,
            $files
          );
          copy($files[0], $this->hotfolders[$unit] . basename($files[0]));
          unlink($files[0]);
        }
      }
    }
    
    return;
  }
}
