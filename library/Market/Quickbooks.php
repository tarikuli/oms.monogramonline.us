<?php

namespace Market;

use Excel;
use Exception;
use Illuminate\Support\Facades\Log;

class Quickbooks
{
    public static function export($shipments)
    {
        $lines = array();

        $line = array();

        $line[] = 'blank';
        $line[] = 'RefNumber';
        $line[] = 'Customer';
        $line[] = 'TxnDate';
        $line[] = 'ShipDate';
        $line[] = 'ShipMethodName';
        $line[] = 'TrackingNum';
        $line[] = 'ShipAddrLine1';
        $line[] = 'ShipAddrLine2';
        $line[] = 'ShipAddrLine3';
        $line[] = 'ShipAddrCity';
        $line[] = 'ShipAddrState';
        $line[] = 'ShipAddrPostalCode';
        $line[] = 'ShipAddrCountry';
//      $line[] = 'PrivateNote';
        $line[] = 'Invoice';
        $line[] = 'Msg';
        $line[] = 'Currency';
        $line[] = 'LineItem';
        $line[] = 'LineQty';
        $line[] = 'LineDesc';
        $line[] = 'LineUnitPrice';
        $line[] = 'Shipping';
        $line[] = 'PO';


        $lines[] = $line;

        foreach ($shipments as $shipment) {

            foreach ($shipment->items as $item) {

                $line = array();

                $line[] = '';
                $line[] = $shipment->unique_order_id;
                $line[] = $shipment->order->store->qb_name ?? $shipment->order->store->store_name;
                $line[] = date("m/d/Y", strtotime($shipment->order->order_date));
                $line[] = date("m/d/Y", strtotime($shipment->transaction_datetime));
                $line[] = $shipment->mail_class;
                $line[] = $shipment->tracking_number;
                $line[] = $shipment->order->customer->ship_full_name;
                $line[] = $shipment->order->customer->ship_address_1;
                $line[] = $shipment->order->customer->ship_address_2;
                $line[] = $shipment->order->customer->ship_city;
                $line[] = $shipment->order->customer->ship_state;
                $line[] = $shipment->order->customer->ship_zip;
                $line[] = $shipment->order->customer->ship_country;
                $line[] = $shipment->order->short_order;
//          $line[] = '';
                if ($shipment->order->purchase_order != null) {
                    $line[] = $shipment->order->purchase_order . '-' . $shipment->order->short_order;
                } else {
                    $line[] = $shipment->order->short_order;
                }
                $line[] = 'USD';
                $line[] = $item->item_code;
                $line[] = $item->item_quantity;
                $line[] = $item->id . ' - ' . $item->item_description;
                $line[] = $item->item_unit_price;
                $line[] = $shipment->order->shipping_charge;
                $line[] = $shipment->order->purchase_order;

                $lines[] = $line;
            }

            // $shipment->export = '1';
            $shipment->save();
        }

        if (count($lines) > 0) {
            $filename = strtoupper('QB_' . date('ymd_His'));
            Log::info($filename);
            $path = storage_path() . '/EDI/Quickbooks/';

            try {
                Excel::create($filename, function ($excel) use ($lines) {
                    $excel->sheet('Invoices', function ($sheet) use ($lines) {
                        $sheet->fromArray($lines, null, 'A1', false, false);
                    });

                })->store('xlsx', $path);

                // copy($path . $filename . '.xls', storage_path() . '/EDI/download/' . $filename . '.xls');
            } catch (Exception $e) {
                Log::error('Error Creating Quickbooks XLSX - ' . $e->getMessage());
            }
        }

        Log::info('Quickbooks invoice csv created');

        return $path . $filename . '.xlsx';
    }


    public static function csvExport($shipments)
    {
        $lines = array();

        $line = array();
        $line[] = 'PO';
        $line[] = 'OrderDate';
        $line[] = 'ShipDate';
        $line[] = 'LineItem';
        $line[] = 'LineQty';
        $line[] = 'LineUnitPrice';
        $lines[] = $line;

        foreach ($shipments as $item) {
            $line = array();
            $line[] = $item->order->purchase_order;
            $line[] = date("m/d/Y", strtotime($item->order->order_date));
            $line[] = date("m/d/Y", strtotime($item->transaction_datetime));
            $line[] = $item->item_code;
            $line[] = $item->item_quantity;
            $line[] = $item->item_unit_price;
            $lines[] = $line;
        }

        if (count($lines) > 0) {
            $filename = strtoupper('QB_' . date('ymd_His'));
            $path = storage_path() . '/EDI/Quickbooks/';
            Log::info(storage_path() . $filename);

            try {
                Excel::create($filename, function ($excel) use ($lines) {
                    $excel->sheet('Invoices', function ($sheet) use ($lines) {
                        $sheet->fromArray($lines, null, 'A1', false, false);
                    });
                })->store('csv', $path);

            } catch (Exception $e) {
                Log::error('Error Creating Quickbooks XLSX - ' . $e->getMessage());
            }
        }

        Log::info('Quickbooks invoice csv created');

        return $path . $filename . '.csv';
    }
}
