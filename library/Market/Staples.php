<?php 
 
namespace Market; 

use App\Order;
use App\Customer;
use App\Item;
use App\Ship;
use App\Store;
use App\Product;
use Illuminate\Support\Facades\Log; 
use Monogram\Helper;
use Monogram\CSV;

class Staples extends StoreInterface 
{ 
		protected $download_dir = '/EDI/download/';
    
		public function importCsv($store, $file) {
			
			$filename = 'import_' . date("ymd_His", strtotime('now')) . '.csv';
			
			$saved = move_uploaded_file($file, storage_path() . '/EDI/Staples/' . $filename); 
			
			if (!$saved) {
				return false;
			}
			
			$csv = new CSV;
			$data = $csv->intoArray(storage_path() . '/EDI/Staples/' . $filename, ",");
			
			$error = array();
			$order_ids = array();
			
			set_time_limit(0);
      
			foreach ($data as $line)  {
				
				if ($line[0] != 'order_date') {
					
					if (count($line) != 60 && count($line) != 62) {
						 $error[] = 'Incorrect number of fields in file: ' . count($line);
						 break;
					}
					
          foreach($line as $key=>$value){
            $line[$key]=str_replace(['"','='],'',$value);
          }
          
          $store = Store::where('store_name', $line[1])->first();
          
          if (!$store) {
						 $error[] = 'Store name from file not found in 5p: ' . $line[1];
						 continue;
					}
          
          $previous_order = Order::where('orders.is_deleted', '0')
                            ->where('orders.order_id', $line[4])
                            ->where('orders.store_id', $store->store_id)
                            ->first();
          
          if ( !$previous_order ) {
            $order_5p = $this->insertOrder($line, $store);
            $order_ids[] = $order_5p;
            Log::info('Staples import: order ' . $line[4] . ' processed');
          } else {
            $order_5p = $previous_order->id;
          }
          
          Log::info('Staples import: Processing line ' . $line[4]);
					
					$previous_item = Item::where('items.edi_id', $line[11])
                            ->where('items.is_deleted', '0')
														->where('items.order_id', $line[4])
														->first();
					
					if ( $previous_item ) {
						 Log::info('Staples : Line Item already in Database ' . $line[4]); 
						 $error[] = 'Line Item already in Database ' . $line[4];
						 continue;
					} 
					
          $this->insertItem($line, $store, $order_5p);
          
          $this->setOrderTotals($order_5p);
				}
			}
			
        
			return ['errors' => $error, 'order_ids' => $order_ids];
		}
		
		private function insertOrder($data, $store) { 
			
			// -------------- Customers table data insertion started ----------------------//
			$customer = new Customer();
			
      $name = str_replace('#', ' ',  $data[29]);
      
			$customer->order_id = $data[4];
			$customer->ship_full_name = $data[29];
      $customer->ship_last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
      $customer->ship_first_name = trim( preg_replace('#'.$customer->ship_last_name.'#', '', $name) );
			$customer->ship_company_name = $data[27] != '' ? $data[27] : null;
      $customer->ship_address_1 = $data[30] != '' ? $data[30] : null;
      $customer->ship_address_2 = $data[31] != '' ? $data[31] : null; 
      $customer->ship_address_2 .= $data[32] != '' ? ' ' . $data[32] : ''; 
      $customer->ship_city = $data[33];
			$customer->ship_state = Helper::stateAbbreviation($data[34]);
			$customer->ship_zip = $data[36];
			$customer->ship_country = $data[35] != '' ? $data[35] : 'US';
			$customer->ship_phone = $data[38];
			
      $customer->bill_email = $data[37] != '' ? $data[37] : null;
			$customer->bill_company_name = $data[1];
      
			$customer->bill_phone = $data[49];
      $customer->ignore_validation = TRUE;
      
			// -------------- Customers table data insertion ended ----------------------//
			// -------------- Orders table data insertion started ----------------------//
			$order = new Order();
			$order->order_id = $data[4];
			$order->short_order = $data[4];
			$order->shipping_charge = '0';
			$order->tax_charge = '0'; 
			$order->order_date = date("Y-m-d H:i:s", strtotime($data[0]));
			$order->store_id = $store->store_id; 
			$order->store_name = '';
			$order->order_status = 4;
			$order->order_comments = $data[51];
			$order->ship_state = Helper::stateAbbreviation($data[34]);
      $order->ship_date = date("Y-m-d", strtotime($data[9]));
      
      if ($store->store_id == 'staples-04') {
        $order->carrier = 'MN'; 
  			$order->method = 'Purolator Label'; 
        $order->currency = 'CAD';
			}
      
			// -------------- Orders table data insertion ended ----------------------//
				
				$customer->save();
				
				try {
					$order->customer_id = $customer->id;
				} catch ( \Exception $exception ) {
					Log::error('Failed to insert customer id in Staples');
				}
				
				$order->save();
				
				try {
					$order_5p = $order->id;
				} catch ( \Exception $exception ) {
					$order_5p = '0';
					Log::error('Failed to get 5p order id in Staples');
				}

				Order::note('Order imported from CSV', $order->id, $order->order_id);
				
				return $order->id;
				
		}
    
    public function setOrderTotals ($order_5p) {
      if ($order_5p != null) {
        $order = Order::find($order_5p);
    		$order->item_count = count($order->items);
        $total = 0;
        foreach ($order->items as $item) {
          $total += $item->item_quantity * $item->item_unit_price;
        }
        $order->total = sprintf("%01.2f", $total);
        $order->save();
      }
    }
    
    private function insertItem ($data, $store, $order_5p) {
      
      $product = Helper::findProduct($data[13]);
      
      $item = new Item();
      
      if ($product != false) {
        $item->item_code = $product->product_model;
        $item->item_id = $product->id_catalog;
        $item->item_thumb = $product->product_thumb;
        $item->item_url = $product->product_url;
      } else {
        $item->item_code = $data[13];
        Log::error('Staples Product not found ' . $data[13] . ' in order ' . $data[4]);
      }
      
      $item->order_id = $data[4];
      $item->store_id = $store->store_id; 
      $item->item_description = $data[16]; 
      $item->item_quantity = $data[17];
      $item->item_unit_price = $data[20]; 
      $item->data_parse_type = 'CSV';
      $item->child_sku = $data[13];
      $item->item_option = '{}';
      $item->edi_id = $data[11];
      $item->order_5p = $order_5p;
      $item->save();
    }
    
		public function orderConfirmation($store, $order) {
			
		} 
	
		public function shipmentNotification($store, $shipments) {
		
    }
    
    public function exportShipments($store, $shipments) {
      
     Log::info('Staples shipment csv started');

		 $lines = array();
       
       $line = array();
       
       $line[] = 'po_number';
       $line[] = 'po_line_number';
       $line[] = 'vendor_sku';
       $line[] = 'serial_number';
       $line[] = 'invoice_number';
       $line[] = 'tracking_id';
       $line[] = 'carrier_id';
       $line[] = 'ship_quantity';
       $line[] = 'bol_number';
       $line[] = 'addl_charge_amount1';
       $line[] = 'addl_charge_type1';
       $line[] = 'addl_charge_currency1';
       $line[] = 'addl_charge_amount2';
       $line[] = 'addl_charge_type2';
       $line[] = 'addl_charge_currency2';
       
       $lines[] = $line;
     
			foreach ($shipments as $shipment) {

				foreach ($shipment->items as $item) {
					
					$line = array();
					
          $line[] = $item->order_id;
          $line[] = $item->edi_id;
          $line[] = $item->child_sku;
          $line[] = '';
          $line[] = $shipment->unique_order_id;
          $line[] = $shipment->tracking_number;
          if ($store != 'staples-04') {
            $line[] = 'UPSM';
          } else {
            $line[] = 'PRLA';
          }
          $line[] = $item->item_quantity;
          $line[] = '';
          $line[] = '';
          $line[] = '';
          $line[] = '';
          $line[] = '';
          $line[] = '';
          $line[] = '';
					
					$lines[] = $line;
				}
			}
			
			if (count($lines) > 0) {
				$filename = $store . '_SHIP_' . date('ymd_His') . '.csv'; 
				$path = storage_path() . '/EDI/Staples/'; 
        try {
  				$csv = new CSV;
  				$pathToFile = $csv->createFile($lines, $path, null, $filename, ',');
          // copy($path . $filename, storage_path() . '/EDI/Staples/' . $filename);
        } catch (\Exception $e) {
          Log::error('Error Creating Staples CSV - ' . $e->getMessage());
          return;
        }
			}
			
			Log::info('Staples shipment csv upload created');
      
			return $path . $filename;
		}
		
		public function getInput($store) {
			
		} 
		 
		public function backorderNotification($store, $item) {
			
		}
		
		public function shipLabel($store, $unique_order_id, $order_id) {
			
		}
}
