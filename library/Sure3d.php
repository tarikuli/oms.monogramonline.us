<?php

namespace Monogram;

use App\Batch;
use App\Item;
use Exception;
use Illuminate\Support\Facades\Log;
use Imagick;

class Sure3d
{
    protected $download_dir = '/media/graphics/Sure3d/';
    const thumb_dir = '/public_html/assets/images/Sure3d/thumbs/';
    const thumb_web = '/assets/images/Sure3d/thumbs/';
    protected $main = '/media/graphics/MAIN/';

    protected $files = array();

    public function __construct ()
    {
        if(env("GRAPHICS_ENV") == "devgraphics"){
            $this->download_dir = '/media/devgraphics/Sure3d/';
            $this->main = '/media/devgraphics/MAIN/';

        }
    }


    public static function getEnv(){
        if(env("GRAPHICS_ENV") == "graphics"){
            return "oms";
        }else{
            return "devoms";
        }
    }

    public function setLink($item, $link = null)
    {

        $item = Item::find($item);

        $options = json_decode($item->item_option, true);

        if ($link != null) {

            $options['Custom_EPS_download_link'] = $link;

            $item->item_option = json_encode($options);

        } else if ($link == null && isset($options['Custom_EPS_download_link'])) {

            $link = $options['Custom_EPS_download_link'];

        } else if ($link == null) {

            return false;

        }

        $item->sure3d = $link;
        $item->save();
        $url = $this->getImage($item);
        return $url;
    }

    public function getImage($item, $find = 0)
    {
        if (!file_exists($this->download_dir)) {
            mkdir($this->download_dir);
        }

        if (!($item instanceof Item)) {
            $item = Item::find($item);
        }

        if (!$item) {
            Log::error('Sure3d getImage: Item not found');
            return null;
        }


        if ($item->sure3d == null) {
            $options = json_decode($item->item_option, true);
            if (isset($options['Custom_EPS_download_link'])) {
                $item->sure3d = $options['Custom_EPS_download_link'];
                $item->save();
            } else {
                return null;
            }
        }

        $url = $item->sure3d;

        $ext = substr($url, strrpos($url, '.'));
        $filename = $this->download_dir . $item->batch_number . '-' .$item->order->short_order . '-' . $item->id . $ext;

        ##########################
        $options = json_decode($item->item_option, true);
        $images = [];
        foreach ($options as $label => $imageUrl) {
//            echo "<pre>" . $label . "   -   " . $imageUrl . "</pre>";
            if ($label != "Custom_EPS_download_link") {
                $label = ucwords(strtolower($label));
                $images[$label] = $imageUrl;
                $newFilesName[$label] = $this->download_dir . $item->batch_number . '-' .$item->order->short_order . '-' . $item->id . '-' . $label . $ext;
            }

        }

        ##########################

        ##################-----------------##################
        foreach ($images as $label => $imageUrl) {
            $filename = $newFilesName[$label];
            $url = $imageUrl;
            // $filename = "/media/graphics/Sure3d/1216980-4.jpg";
            if (file_exists($filename)) {
                unlink($filename);
            }
            if (file_exists($filename) && $find == 0) {
                try {
                    rename($filename, $this->download_dir . $item->order->short_order . ' - ' . $item->id . '-OLD-' . date("His") . $ext);
                } catch (Exception $e) {
                    Log::error('Sure3d getImage: failed renaming old file ' . $filename . ' - ' . $e->getMessage());
                    $filename = $this->download_dir . $item->order->short_order . ' - ' . $item->id . '-' . date("His") . $ext;
                }
            } else if (file_exists($filename) && $find == 1) {
                Log::error('Sure3d getImage: file Exist ' . $filename . '  url =' . $url);
                return $url;
            }

            $count = 0;
            set_time_limit(0);
            ini_set('memory_limit', '512M');

            do {
                touch($filename);

                try {

                    $fp = fopen($filename, 'w+');

                    if ($fp === false) {
                        throw new Exception('Could not open: ' . $filename);
                    }
//                    Log::error('**Batch Export : jSure3d image from  ' . $url);

                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_FILE, $fp);
//                curl_setopt($ch, CURLOPT_TIMEOUT, 0);
                    curl_setopt($ch, CURLOPT_USERAGENT,
                        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
//                curl_setopt($ch, CURLOPT_WRITEFUNCTION, "curl_callback");
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 1000);      // some large value to allow curl to run for a long time


                    curl_exec($ch);

                    if (curl_errno($ch)) {
                        throw new Exception(curl_error($ch));
                    }

                    $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                    curl_close($ch);
                    fclose($fp);

                    if ($statusCode == 200) {
                        $saved = true;
//                        Log::info('getImage  getImage created from url= ' . $url . '  to = ' . $filename);
                    } else {
                        throw new Exception("Status Code: " . $statusCode);;
                    }

                } catch (Exception $e) {
                    $saved = false;
                    $count++;
                    Log::error('Sure3d getImage: ' . $e->getMessage());
                    sleep(2);
                }

            } while (!$saved && $count < 10);

            if (!$saved) {
                Log::error('Sure3d getImage: Download Failed - ' . $url);
                return null;
            }

            if (strtolower($ext) == '.dxf') {
//                $epsUrl = str_replace('.dxf', '.eps', str_replace('/DXF/', '/EPS/', $url));
//                $tmp = $this->getImage($item);
            } else {
                try {
                    $this->createThumb($item, $ext);
                } catch (Exception $e) {
                    Log::error('Sure3d createThumb: Exception creating thumbnail ' . $filename . ' - ' . $e->getMessage());
                }
            }
        }
        ##################-----------------##################
        return $url;

    }

    public function createThumb($item, $ext = '.eps')
    {

//        Log::info('Create thumb ' . $ext);
        if (!file_exists(base_path() . Sure3d::thumb_dir)) {
            mkdir(base_path() . Sure3d::thumb_dir);
        }

        if (!($item instanceof Item)) {
            $item = Item::with('order')->find($item);
        }

        $file = $item->order->short_order . '-' . $item->id . $ext;

        ##########################

        $options = json_decode($item->item_option, true);
        $newFilesName = array();
        $thumbsFileName = array();

        foreach ($options as $label => $imageUrl) {
//            echo "<pre>" . $label . "   -   " . $imageUrl . "</pre>";

            if ($label != "Custom_EPS_download_link") {
                $label = ucwords(strtolower($label));

                $images[$label] = $imageUrl;
                $file = $item->batch_number . '-' .$item->order->short_order . '-' . $item->id . '-' . $label . $ext;
                $newFilesName[$label] = $this->download_dir . $file;
                $thumbsFileName[$label] = $file;
//                $image_path = $imageUrl;
            }

        }

        ##########################

//        $image_path = $this->download_dir . $file;


        foreach ($newFilesName as $label => $image_path) {
            if (!file_exists($image_path)) {
                Log::error('Sure3d getImage: Image does not exist - ' . $image_path);
//            return;
                continue;

            }

            // $image = file_get_contents($image_path);

            try {
                $section = strtolower($item->parameter_option->route->stations->first()->section_info->section_name);
            } catch (Exception $e) {
                $section = '';
            }

            if ($section == 'sublimation' || $section == 'applique') {
                $flop = 1;
            } else {
                $flop = 0;
            }

//            $thumb_dir = base_path() . Sure3d::thumb_dir . substr($file, 0, strpos($file, '.')) . '.jpg';
            $thumb_dir = base_path() . Sure3d::thumb_dir .$thumbsFileName[$label];

            try {
//            $image_path = /media/graphics/Sure3d/1216988-5-RevImage.jpg -> $thumb_dir = /var/www/oms/public_html/assets/images/Sure3d/thumbs/1216988-5-RevImage.jpg
                Log::info("createThumb image_path = " . $image_path . ' - thumb_dir = ' . $thumb_dir." url = ".sprintf("http://".$this::getEnv().".monogramonline.us/assets/images/Sure3d/thumbs/%s", str_replace("Revimage","Image",$thumbsFileName[$label])));
                ImageHelper::createThumb($image_path, $flop, $thumb_dir, 700);
                #$item->item_thumb = sprintf(url('/')."/assets/images/Sure3d/thumbs/%s", str_replace("Revimage","Image",$thumbsFileName[$label]));
                $item->item_thumb = sprintf("http://".$this::getEnv().".monogramonline.us/assets/images/Sure3d/thumbs/%s", str_replace("Revimage","Image",$thumbsFileName[$label]));

                $item->save();
            } catch (Exception $e) {
                Log::error('Sure3d createThumb: ' . $e->getMessage());
            }

        }
        return;
    }

    public static function getThumb($item)
    {
        if ($item->sure3d == null) {
            return false;
        }

        $jpg = $item->batch_number. '-' .$item->order->short_order . '-' . $item->id . '.jpg';

        $thumb_path = base_path() . Sure3d::thumb_dir . $jpg;


        if (!file_exists($thumb_path)) {
            return false;
        }

        try {
            $size = getimagesize($thumb_path);

        } catch (Exception $e) {

            Log::error('Sure3d: Thumbnail file not found ' . $thumb_path);
            return false;
        }

        return [Sure3d::thumb_web . $jpg, $size[0], $size[1]];
    }

    public static function getThumbs($item)
    {
        if ($item->sure3d == null) {
            Log::error('Sure3d: 1. Thumbnail file not found ==== order_id ' . $item->order_id);
            return false;
        }

        ##########################
        $options = json_decode($item->item_option, true);
        $newFilesName = array();
        $thumbsFileName = array();
        $ext = ".jpg";
        #########oldBatchNumber###########
        $oldBatchNumber = $item->batch_number;
        if((substr($oldBatchNumber, 0, 1) == 'X')){
            $oldBatchNumber = explode('-',$oldBatchNumber);
            $oldBatchNumber = $oldBatchNumber[1];
        }
        if((substr($oldBatchNumber, 0, 1) == 'R')){
            $oldBatchNumber = explode('-',$oldBatchNumber);
            $oldBatchNumber = $oldBatchNumber[1];
        }
        #########oldBatchNumber###########
        foreach ($options as $label => $imageUrl) {
            if ($label != "Custom_EPS_download_link") {
                $label = ucwords(strtolower($label));
                $images[$label] = $imageUrl;
                #$file = $item->batch_number. '-' .$item->order->short_order . '-' . $item->id . '-' . $label . $ext;
                $file = $oldBatchNumber. '-' .$item->order->short_order . '-' . $item->id . '-' . $label . $ext;
                $newFilesName[$label] = base_path() . Sure3d::thumb_dir. $file;
                $thumbsFileName[$label] = $file;
            }
        }
        ##########################

        foreach ($newFilesName as $label => $thumb_path) {
//            $jpg = $item->order->short_order . '-' . $item->id . '.jpg';
            $thumFileName = $thumbsFileName[$label];


            if (!file_exists($thumb_path)) {
                Log::error('Sure3d: 2. Thumbnail file not found ==== ' . $thumb_path);
                return false;
            }

            try {
                $size = getimagesize($thumb_path);
            } catch (Exception $e) {
                Log::error('Sure3d: Thumbnail file not found ' . $thumb_path);
//                return false;
            }
            $thumImages[$label] = [url('/').Sure3d::thumb_web . $thumFileName, $size[0], $size[1]];
        }
//        return [Sure3d::thumb_web . $jpg, $size[0], $size[1]];

        return $thumImages;

    }

    public function exportBatch($batch)
    {

        if (!($batch instanceof Batch)) {
            $batch = Batch::with('items')->where('batch_number', $batch)->first();
        }

        set_time_limit(0);

        $graphics = array();

        $graphic_count = 0;

        foreach ($batch->items as $item) {

            if ($item->sure3d != NULL) {
                $graphic_count++;
                // ***1*** Got ot each Item line and download image files using url
                $this->getImage($item, 1);
                ##########################
                $options = json_decode($item->item_option, true);
                foreach ($options as $label => $imageUrl) {
                    if ($label != "Custom_EPS_download_link") {
                        $label = ucwords(strtolower($label));
                        $graphics[] = $item->batch_number . '-' .$item->order->short_order . '-' . $item->id . '-' . $label . substr($item->sure3d, strrpos($item->sure3d, '.'));
                    }
                }
                ##########################

//                for ($x = 0; $x < $item->item_quantity; $x++) {
//                    $graphics[] = $item->order->short_order . '-' . $item->id . substr($item->sure3d, strrpos($item->sure3d, '.'));
//                }
            }
        }

//        Log::info('Batch Export : Sure3d graphics: ' . print_r($graphics, true));
//        if (count($graphics) > 0 && $graphic_count < count($batch->items)) {
//            Batch::note($batch->batch_number, $batch->station_id, '9', 'Sure3d Error: Mixed Sure3d Batch - not all ids set');
//            Log::error('Sure3d moveBatch Error: Mixed Sure3d Batch - not all ids set - Batch: ' . $batch->batch_number);
//            return false;
//        }

        if (count($graphics) > 0) {
            // Check is any existing Batch Directory exist in /media/graphics/MAIN/2*
            $list = glob($this->main . $batch->batch_number . "*");

            if (count($list) > 0) {

                Batch::note($batch->batch_number, $batch->station_id, '9', 'Sure3d Error: Batch already in MAIN');
                Log::error('Sure3d moveBatch Error: Batch already in MAIN - Batch: ' . $batch->batch_number);
                return false;

            }

            $dir = $this->main . $batch->batch_number . $batch->route->csv_extension;

//      if (strtolower($batch->route->graphic_dir) == '' && count($graphics) > 1) {
//        ini_set('memory_limit','256M');
//        $this->layout($graphics, $batch->batch_number, $dir);
//      } else {
            mkdir($dir);
            touch($dir . '/lock');
//            Array([0] => '1216987-1.jpg',   [1] => '1216987-2.jpg')
            foreach ($graphics as $key => $sure3d) {
                // Sure3d file copy from : /media/graphics/Sure3d/11319-1216988-6-Image.jpg to /media/graphics/MAIN/11/11-0.jpg
                if (file_exists($this->download_dir . $sure3d) && strtolower($batch->route->graphic_dir) == '') {
//                    Log::info('Batch Export : Sure3d file copy from : ' . $this->download_dir . $sure3d . " to " . $dir . '/' . $batch->batch_number . '-' . $key . substr($sure3d, strrpos($sure3d, '.')));
                    copy($this->download_dir . $sure3d, $dir . '/' . $batch->batch_number . '-' . $key . substr($sure3d, strrpos($sure3d, '.')));
                } else if (file_exists($this->download_dir . $sure3d)) {
//                    Log::info('Batch Export : Sure3d file copy from2 : ' . $this->download_dir . $sure3d . " to " . $dir . '/' . $sure3d);
                    copy($this->download_dir . $sure3d, $dir . '/' . $sure3d);
                } else {
                    Log::error('Sure3d: Graphic not found ' . $sure3d);
                    Batch::note($batch->batch_number, $batch->station_id, '9', 'Sure3d: Graphic not found ' . $sure3d);
                }
            }
            unlink($dir . '/lock');
        }
//    }

        Batch::note($batch->batch_number, $batch->station_id, '9', 'Sure3d Graphic Created');
        return true;
    }

    public function layout($graphics, $batch_number, $dir)
    {
        return;
        Log::info('Layout');
        $pages = array();
        $pages[] = array();
        $page_height = 0;
        $big = '';

        foreach ($graphics as $graphic) {
            Log::info($graphic);
            $size = ImageHelper::getImageSize($this->download_dir . $graphic); //getImageGeometry?

            // if ($size['height'] > $size['width'] && $size['height'] < 1650) {
            //   $this->rotateEps($this->download_dir . $graphic);
            //   $size = $this->getEpsSize($this->download_dir . $graphic);
            // }

            if ($size['width'] > 23 && !strpos(strtolower($dir), 'big')) {
                $big = '-BIG';
            }

            if ($page_height + $size['height'] + .75 > 200) {
                $pages[] = array();
                $page_height = 0;
            }

            $page_height += $size['height'] + .75;

            $pages[count($pages) - 1][] = $graphic;
        }

        mkdir($dir);
        touch($dir . '/lock');

        foreach ($pages as $page => $graphics) {

            $image = new Imagick();

            foreach ($graphics as $graphic) {
                try {
                    echo "$graphic\n";
                    $image->readImage($this->download_dir . $graphic);
                    $image->newImage(50, 50, 'white');
                } catch (Exception $e) {
                    Log::error('Sure3d: Error laying out file ' . $graphic . ' - ' . $e->getMessage());
                }
            }
            echo "1\n";
            $image->resetIterator();
            echo "2\n";
            $combined = $image->appendImages(true);
            echo "3\n";

            $page = $page + 1;

            if (count($pages) == 1) {
                $fp = $dir . '/' . $batch_number . $big . '.eps';
            } else {
                $fp = $dir . '/' . $batch_number . $big . '-page' . $page . 'of' . count($pages) . '.eps';
            }

            $combined->writeImage($fp);
        }

        unlink($dir . '/lock');
        return;
    }

    private function rotateEps($fileName)
    {

        $image = new Imagick();
        $image->readImage($fileName);
        $image->rotateImage('white', 90);
        $image->writeImage($fileName);
        return true;

    }

    public function download()
    {

        $items = Item::with('order')
            ->where('item_status', 1)
            ->whereNotNull('sure3d')
            ->where('item_thumb', 'LIKE', '%localhost%')
            ->where('is_deleted', '0')
            ->get();


        foreach ($items as $item) {
            $file = $this->download_dir . $item->batch_number . '-' .$item->order->short_order . '-' . $item->id . substr($item->sure3d, strrpos($item->sure3d, '.'));

            if (!file_exists($file)) {
                try {
                    $item->sure3d = $this->getImage($item);
                } catch (Exception $e) {
                    Log::error('Sure3d download: Exception getting image ' . $item->sure3d . ' - ' . $e->getMessage());
                }
            }
        }
    }

}