<!doctype html> 
<html lang = "en">
<head>
	<meta charset = "UTF-8">
	<title>Batch preview</title>
	<meta name = "viewport" content = "width=device-width, initial-scale=1">
	<link type = "text/css" rel = "stylesheet" href = "/assets/css/bootstrap.min.css">
	<link type = "text/css" rel = "stylesheet" href="/assets/css/pikaday.min.css">
	<script type = "text/javascript" src = "/assets/js/jquery.min.js"></script>
	<script type = "text/javascript" src = "/assets/js/bootstrap.min.js"></script>
	<script type = "text/javascript" src = "/assets/js/moment.min.js"></script>
	<script type = "text/javascript" src = "/assets/js/pikaday.min.js"></script>
	<style>
		table {
			table-layout: fixed;
			font-size: 11px;
		}

		td {
			width: auto;
		}

		img {
			width: 50px;
			height: 50px;
		}

		/*----------------------------------------------------------------------------------*/
		.switch {
			position: relative;
			display: inline-block;
			width: 60px;
			height: 34px;
		}

		/* Hide default HTML checkbox */
		.switch input {
			opacity: 0;
			width: 0;
			height: 0;
		}

		/* The slider */
		.slider {
			position: absolute;
			cursor: pointer;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			background-color: #ccc;
			-webkit-transition: .4s;
			transition: .4s;
		}

		.slider:before {
			position: absolute;
			content: "";
			height: 26px;
			width: 26px;
			left: 4px;
			bottom: 4px;
			background-color: white;
			-webkit-transition: .4s;
			transition: .4s;
		}

		input:checked + .slider {
			background-color: #2196F3;
		}

		input:focus + .slider {
			box-shadow: 0 0 1px #2196F3;
		}

		input:checked + .slider:before {
			-webkit-transform: translateX(26px);
			-ms-transform: translateX(26px);
			transform: translateX(26px);
		}

		/* Rounded sliders */
		.slider.round {
			border-radius: 34px;
		}

		.slider.round:before {
			border-radius: 50%;
		}
		.switch {
			height: 24px;
			display: block;
			position: relative;
			cursor: pointer;
		}
		.switch input {
			display: none;
		}
		.switch input + span {
			padding-left: 50px;
			min-height: 24px;
			line-height: 24px;
			display: block;
			color: #99A3BA;
			position: relative;
			vertical-align: middle;
			white-space: nowrap;
			transition: color .3s ease;
		}
		.switch input + span:before, .switch input + span:after {
			content: '';
			display: block;
			position: absolute;
			border-radius: 12px;
		}
		.switch input + span:before {
			top: 0;
			left: 0;
			width: 42px;
			height: 24px;
			background: #E4ECFA;
			transition: all .3s ease;
		}
		.switch input + span:after {
			width: 18px;
			height: 18px;
			background: #fff;
			top: 3px;
			left: 3px;
			box-shadow: 0 1px 3px rgba(18, 22, 33, 0.1);
			transition: all .45s ease;
		}
		.switch input + span em {
			width: 8px;
			height: 7px;
			background: #99A3BA;
			position: absolute;
			left: 8px;
			bottom: 7px;
			border-radius: 2px;
			display: block;
			z-index: 1;
			transition: all .45s ease;
		}
		.switch input + span em:before {
			content: '';
			width: 2px;
			height: 2px;
			border-radius: 1px;
			background: #fff;
			position: absolute;
			display: block;
			left: 50%;
			top: 50%;
			margin: -1px 0 0 -1px;
		}
		.switch input + span em:after {
			content: '';
			display: block;
			border-top-left-radius: 4px;
			border-top-right-radius: 4px;
			border: 1px solid #99A3BA;
			border-bottom: 0;
			width: 6px;
			height: 4px;
			left: 1px;
			bottom: 6px;
			position: absolute;
			z-index: 1;
			-webkit-transform-origin: 0 100%;
			transform-origin: 0 100%;
			transition: all .45s ease;
			-webkit-transform: rotate(-35deg) translate(0, 1px);
			transform: rotate(-35deg) translate(0, 1px);
		}
		.switch input + span strong {
			font-weight: normal;
			position: relative;
			display: block;
			top: 1px;
		}
		.switch input + span strong:before, .switch input + span strong:after {
			font-size: 14px;
			font-weight: 500;
			display: block;
			font-family: 'Mukta Malar', Arial;
			-webkit-backface-visibility: hidden;
		}
		.switch input + span strong:before {
			content: 'Auto Batch On';
			transition: all .3s ease .2s;
		}
		.switch input + span strong:after {
			content: 'Auto Batch Off';
			opacity: 0;
			visibility: hidden;
			position: absolute;
			left: 0;
			top: 0;
			color: #5628EE;
			transition: all .3s ease;
			-webkit-transform: translate(2px, 0);
			transform: translate(2px, 0);
		}
		.switch input:checked + span:before {
			background: rgba(86, 40, 238, 0.35);
		}
		.switch input:checked + span:after {
			background: #fff;
			-webkit-transform: translate(18px, 0);
			transform: translate(18px, 0);
		}
		.switch input:checked + span em {
			-webkit-transform: translate(18px, 0);
			transform: translate(18px, 0);
			background: #5628EE;
		}
		.switch input:checked + span em:after {
			border-color: #5628EE;
			-webkit-transform: rotate(0deg) translate(0, 0);
			transform: rotate(0deg) translate(0, 0);
		}
		.switch input:checked + span strong:before {
			opacity: 0;
			visibility: hidden;
			transition: all .3s ease;
			-webkit-transform: translate(-2px, 0);
			transform: translate(-2px, 0);
		}
		.switch input:checked + span strong:after {
			opacity: 1;
			visibility: visible;
			-webkit-transform: translate(0, 0);
			transform: translate(0, 0);
			transition: all .3s ease .2s;
		}

		/*-------------------------------------------------------------------------------------------------*/
	</style>
</head>
<body>
	@include('includes.header_menu')
	<div class = "container">
		<ol class = "breadcrumb">
			<li><a href = "{{url('/')}}">Home</a></li>
			<li class = "active">Preview Batch</li>
		</ol>

		@include('includes.error_div')
		@include('includes.success_div')
		<div class = "col-xs-12">
			<div class="row">
				{!! Form::open(['url' => url('preview_batch'), 'method' => 'get']) !!}
				{!! Form::hidden('backorder', isset($backorder) ? $backorder : '' , ['id' => 'backorder']) !!}
				<div class = "form-group col-xs-2">
					<label for = "search_for_first">Search for</label>
					{!! Form::text('search_for_first', $request->get('search_for_first'), ['id'=>'search_for_first', 'class' => 'form-control', 'placeholder' => 'Comma delimited']) !!}
				</div>
				<div class = "form-group col-xs-2">
					<label for = "search_in_first">Search in</label>
					{!! Form::select('search_in_first', $search_in, $request->get('search_in_first'), ['id'=>'search_in_first', 'class' => 'form-control']) !!}
				</div>
				<div class = "form-group col-xs-2">
					<label for = "store">Store</label>
					{!! Form::select('store', $stores, $request->get('store'), ['id'=>'store', 'class' => 'form-control']) !!}
				</div>
				<div class = "form-group col-xs-2">
					<label for = "Section">Section</label>
					{!! Form::select('section', $sections, $request->get('section'), ['id'=>'section', 'class' => 'form-control']) !!}
				</div>
				<div class = "form-group col-xs-2">
					<label for = "start_date">Order Start date</label>
					<div class = 'input-group date' id = 'start_date_picker'>
						{!! Form::text('start_date', $request->get('start_date'), ['id'=>'start_datepicker', 'class' => 'form-control', 'placeholder' => 'Enter start date', 'autocomplete' => 'off']) !!}
						<span class = "input-group-addon"><span class = "glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
				<div class = "form-group col-xs-2">
					<label for = "end_date">Order End date</label>
					<div class = 'input-group date' id = 'end_date_picker'>
						{!! Form::text('end_date', $request->get('end_date'), ['id'=>'end_datepicker', 'class' => 'form-control', 'placeholder' => 'Enter end date', 'autocomplete' => 'off']) !!}
						<span class = "input-group-addon"><span class = "glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class = "form-group col-xs-8"></div>
				<div class = "form-group col-xs-3">

					@if (!$locked)
						<label class="switch">
							<input type="checkbox" id="lock" onclick="locks()">
							<span>
            			<em></em>
            			<strong></strong>
        				</span>
						</label>
					@else
						<label class="switch">
							<input type="checkbox" id="lock" onclick="locks()" checked>
							<span>
            			<em></em>
            			<strong></strong>
        				</span>
						</label>
					@endif

				</div>
				<div class = "form-group col-xs-1">
						{!! Form::submit('Search', ['class' => 'btn btn-success']) !!}
				</div>
			</div>
				{!! Form::close() !!}
		</div>
		
		@if(count($batch_routes) > 0)
		
			<div><br><br><br></div>
				
			<div class="row">
				<div class = "form-group col-xs-7">
					<ul class="nav nav-pills nav-xs">
					  <li role="instock" @if(!$backorder) class="active" @endif ><a href="/preview_batch">In Stock</a></li>
					  <li role="backorder" @if($backorder) class="active" @endif ><a href="/preview_batch?backorder=1">Back Orders</a></li>
					</ul>
				</div>

				{!! Form::open(['url' => url('preview_batch'), 'method' => 'post']) !!}
				
				{!! Form::hidden('backorder', $backorder) !!}
				<div class = "form-group col-xs-2">
						{!! Form::checkbox('select-deselect', 1, false, ['id' => 'select-deselect', 'style' => 'margin-top:10px;']) !!} Select / Deselect all
				</div>
				<div class = "form-group col-xs-1">
{{--					@if (!$locked)--}}
						{!! Form::submit('Create batch', ['id' => 'create-batch','class' => 'btn btn-success create_batch', 'style' => 'display:none']) !!}
{{--					@else--}}
						{!! Form::button('Auto Batch Locked', ['id' =>'warning','class' => 'btn btn-warning', 'style' => 'display:none']) !!}
{{--					@endif--}}
				</div>
			</div>

			<div class = "row">
				<div class = "col-xs-12">
					<table class = "table">
						<tr>
							<th>Batch #</th>
							<th>S.L #</th>
							<th>Batch S.L #</th>
							<th></th>
							<th>Item ID<br>Order #</th>
							<th>Order date<br>Store</th>
							<th>SKU</th>
							<th>Quantity</th>
						</tr>
					</table>
				</div>
				
				@foreach($batch_routes as $batch_route)
					
						<div class = "col-xs-12"> 
							<table class = "table">
								<tr data-id = "{{$batch_route['id']}}">
									<td>{{ $count }}</td>
									<td colspan="2">Route: {{ $batch_route['batch_code'] }} = {{ $batch_route['batch_route_name'] }}</td>
									<td>{!! Form::checkbox('select-deselect', 1, false, ['id' => 'group-select']) !!}</td>
									<td colspan="3" > Next station >>> {{ $batch_route['next_station'] }} )</td>
									<td></td>
								</tr>
								@setvar($row_serial = 1)
								
								@foreach ($batch_route['items'] as $item)
								
									<tr>
										<td><img src = "{{$item->item_thumb}}" /></td>
										<td>{{$serial++}}</td>
										<td>{{$row_serial++}}</td>
										<td>{!! Form::checkbox('batches[]', sprintf("%s|%s|%s|%s|%s", $count, $batch_route['id'], $item->item_table_id, $item->batch, $item->store_id) ,false, ['class' => 'checkable']) !!}</td>
										<td>
												***{{ $item->item_table_id }}
												<br>
				   							<a href = "{!! url(sprintf('orders/details/%s', $item->order_5p)) !!}"
											   target = "_blank">{{ $item->short_order }}
											</a>
										</td>
										<td>
												{{substr($item->order_date, 0, 10)}}
												<br>
												{{ $item->store_name }}
										</td>
										<td>
											<a href = "{{ url(sprintf("logistics/sku_list?search_for_first=%s&search_in_first=child_sku", $item->child_sku)) }}"
											   target = "_blank">{{$item->item_code}}</a>
										</td>
										<td>{{$item->item_quantity}}</td>
									</tr>
								@endforeach
								
								<tr>
									<td colspan=7></td>
									<td>
										<span class = "item_selected">{{ $row_serial - 1 }}</span> of <span
												class = "item_total">{{ $batch_route['batch_max_units'] }}</span> Max
									</td>
								</tr>
								@setvar(++$count)
							</table>
						</div>
						
				@endforeach
				
			</div>
			<div class="row">
				<div class = "form-group col-xs-7"></div>
				<div class = "form-group col-xs-2">
						{!! Form::checkbox('select-deselect', 1, false, ['id' => 'select-deselect', 'style' => 'margin-top:10px;']) !!} Select / Deselect all
				</div>
				<div class = "form-group col-xs-1">
{{--					@if (!$locked)--}}
							{!! Form::submit('Create batch', ['id' => 'create-batch','class' => 'btn btn-success create_batch', 'style' => 'display:none']) !!}
{{--					@else--}}
						{!! Form::button('Auto Batch Locked', ['id' =>'warning','class' => 'btn btn-warning', 'style' => 'display:none']) !!}
{{--					@endif--}}
				</div>
			</div>
			{!! Form::close() !!}
		@else
			<div class = "col-xs-12">
				<div class = "alert alert-warning">
					No batches to create.
				</div>
			</div>
		@endif
	</div>

	<script type = "text/javascript">
		var checkBox = document.getElementById("lock");
		var batchButton = document.getElementById("create-batch");
		var warning = document.getElementById("warning");
		$(document).ready(function(){
			setTimeout( function() { 
    		$(".create_batch").hide();
  		}  , 180000 );
			if (checkBox.checked == true) {
				batchButton.style.display = "none";
				warning.style.display = "block";
			}else {
				batchButton.style.display = "block";
				warning.style.display = "none";
			}
		});
		
		$(function(){
        $(".create_batch").on('click',function() {
            $(".create_batch").hide();
        }); 
    });

		var picker = new Pikaday(
		{
				field: document.getElementById('start_datepicker'),
				format : "YYYY-MM-DD",
				minDate: new Date('2016-06-01'),
				maxDate: new Date(),
				yearRange: [2000,2030]      
		});

		var picker = new Pikaday(
		{
				field: document.getElementById('end_datepicker'),
				format : "YYYY-MM-DD",
				minDate: new Date('2016-06-01'),
				maxDate: new Date(),
				yearRange: [2000,2030]      
		});
		
		var state = false;
		
		$("input#select-deselect").click(function (event)
		{
			state = !state;
			$("input[type='checkbox']").not($(this)).prop('checked', state);
			$("table").each(function ()
			{
				updateTableInfo($(this));
			});
		});
		
		$("input#group-select").on("click", function (event)
		{
			var table = $(this).closest('table');
			var state = $(this).prop('checked');
			table.find('tr').not(':first').not(':last').each(function ()
			{
				$(this).find('input:checkbox').prop('checked', state);
			});

			updateTableInfo(table);
		});
		
		$("input.checkable").not('input#select-deselect, input.group-select').on('click', function (event)
		{
			var table = $(this).closest('table');
			var item_selected = getSelectedItemCount(table);
			var item_total = table.find('tr').not(':first').not(':last').length;
			//$(table).find('span.item_selected').text(item_selected);
			updateTableInfo(table);
			$(table).find('tr').eq(0).find('input:checkbox').prop('checked', item_selected == item_total);
		});

		function updateTableInfo (table)
		{
			$(table).find('span.item_selected').text(getSelectedItemCount(table));
		}

		function getSelectedItemCount (table)
		{
			var total_selected = 0;
			table.find('tr').not(':first').not(':last').each(function ()
			{
				if ( $(this).find('input:checkbox').prop('checked') == true ) {
					++total_selected;
				}
			});
			return total_selected;
		}

		function locks() {
			// Get the checkbox

			// If the checkbox is checked, send data
			if (checkBox.checked == true){

				$.ajax({
					type: 'get',
					url: '{{ route('formSubmit1') }}',
					data: {
						"flag": "1",
					},
					context: this,
					success: function (response) {

					},
					failure: function (response) {

					}
				});
			batchButton.style.display = "none";
			warning.style.display = "block";
			} else {
				$.ajax({
					type: 'get',
					url: '{{ url("preview_batch/lock") }}',
					data: {
						"flag": "0"
					},
					context: this,
					success: function (response) {

					},
					failure: function (response) {

					}
				});
				batchButton.style.display = "block";
				warning.style.display = "none";
			}
		}
	</script>
</body>
</html>