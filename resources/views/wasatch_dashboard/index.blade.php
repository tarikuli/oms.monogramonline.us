<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Wasatch Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="/assets/css/bootstrap.min.css">

    <script type="text/javascript" src="/assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
    <style>
        .panel-group .panel {
            border-radius: 0;
            box-shadow: none;
            border-color: #EEEEEE;
        }

        .panel-default > .panel-heading {
            padding: 0;
            border-radius: 0;
            color: #212121;
            background-color: #FAFAFA;
            border-color: #EEEEEE;
        }

        .panel-title {
            font-size: 14px;
        }

        .panel-title > a {
            display: block;
            padding: 15px;
            text-decoration: none;
        }

        .more-less {
            float: right;
            color: #212121;
        }

        .panel-default > .panel-heading + .panel-collapse > .panel-body {
            border-top-color: #EEEEEE;
        }

    </style>
</head>
<body>
@include('includes.header_menu')
<div class="container" style="width:100%;">
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}">Home</a></li>
        <li class="active">Wasatch Dashboard (Complete Batch will move to Production every 10 min )</li>
    </ol>
    @include('includes.error_div')
    @include('includes.success_div')

    <div class="row">
        <div class="col-lg-6">
            @setvar( $totalJobs = 0)
            @setvar( $printerNumber = 1)
            @foreach($printQue as $printerName => $batchQueue)
                <div class="panel-group" id="accordionPrint" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingPrint{{$printerNumber}}">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordionPrint"
                                   href="#collapsePrint{{$printerNumber}}" aria-expanded="true"
                                   aria-controls="collapsePrint{{$printerNumber}}">
                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                    <strong>{{ $printerName }} -> Pending Prints:
                                    </strong><span style="color:green; font-size: large; " id="printqueueCount{{$printerNumber}}">

                                    @if(isset($printQue[$printerName]))
                                            {{count($printQue[$printerName])}}
                                            @setvar( $totalJobs += count($printQue[$printerName]))
                                        @else
                                            0
                                    @endif

                                </span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapsePrint{{$printerNumber}}" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingPrint{{$printerNumber}}">
                            <div class="panel-body">
                                <table class="table table-bordered table-hover" id="printqueue{{$printerNumber}}">

                                    <thead>
                                    <tr>
                                        <th colspan="6">PRINTQUEUE</th>
                                    </tr>
                                    <tr>
                                        <th>Batch#</th>
                                        <th>Copies</th>
                                        <th>Index</th>
                                        <th>Notes</th>
                                        <th>Output</th>
                                        <th>Source File</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($batchQueue as $batchNumber => $itemsArray)
                                        {{--                    {{ dd( $items, $batchQueue) }}--}}
                                        @foreach($itemsArray as $items)

                                            @if(isset($jpgPdfCount[$batchNumber]) && $jpgPdfCount[$batchNumber]['jpg_match'] == false)
                                                <tr style="background-color: red;">
                                            @elseif(isset($jpgPdfCount[$batchNumber]) && $jpgPdfCount[$batchNumber]['pdf_match'] == false)
                                                <tr style="background-color: yellow;">
                                            @else
                                                <tr>
                                            @endif
                                                <td>
                                                    {{ $batchNumber }}
                                                </td>
                                                <td>
                                                    @if(isset($items["copies"]))
                                                        {{ $items['copies'] }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(isset($items["index"]))
                                                        {{ $items['index'] }}
                                                    @endif

                                                </td>
                                                <td>
                                                    @if(isset($items["notes"]))
                                                        {{ $items['notes'] }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(isset($items["output"]))
                                                        {{ $items['output'] }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(isset($items["sourcefile"]))
                                                        {{ $items['sourcefile'] }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr style="background-color: dimgrey;">
                                            <td colspan="6"></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @setvar( $printerNumber ++)
                    @endforeach
                    {{  "Total = ".$totalJobs }}
                </div>
        </div>
    </div>


    <script type="text/javascript">

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });


        function toggleIcon(e) {
            $(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('glyphicon-plus glyphicon-minus');
        }

        $('.panel-group').on('hidden.bs.collapse', toggleIcon);
        $('.panel-group').on('shown.bs.collapse', toggleIcon);
    </script>
</body>
</html>