<!doctype html>
<html lang = "en">
<head>
	<meta charset = "UTF-8">
	<title>Print Sublimation</title>
	<meta name = "viewport" content = "width=device-width, initial-scale=1">
	<link type = "text/css" rel = "stylesheet" href = "/assets/css/bootstrap.min.css">
	<link type = "text/css" rel = "stylesheet" href = "/assets/css/bootstrap-multiselect.css">
	<link type = "text/css" rel = "stylesheet" href = "/assets/css/pikaday.min.css">
	
	<script type = "text/javascript" src = "/assets/js/jquery.min.js"></script>
	<script type = "text/javascript" src = "/assets/js/bootstrap.min.js"></script>
	<script type = "text/javascript" src = "/assets/js/bootstrap-multiselect.js"></script>	
	<script type = "text/javascript" src = "/assets/js/moment.min.js"></script>
	<script type = "text/javascript" src = "/assets/js/pikaday.min.js"></script>
	<style>
	
		input[type=number]{
		  width: 50px;
		} 
		
		#wrapper {
		  margin-right: 50px;
			margin-left: 50px;
		}
		
		#content {
		  float: left;
		  width: 1000px;
		}
	
		#sidebar2 {
		  float: right;
		  width: 400px;
		}
		
		#cleared {
		  clear: both;
		}

		/*---------------------------rejectLocksSwitch--------------------------------------*/
		.rejectLocksSwitch {
			position: relative;
			display: inline-block;
			width: 60px;
			height: 34px;
		}

		/* Hide default HTML checkbox */
		.rejectLocksSwitch input {
			opacity: 0;
			width: 0;
			height: 0;
		}

		/* The slider */
		.slider {
			position: absolute;
			cursor: pointer;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			background-color: #ccc;
			-webkit-transition: .4s;
			transition: .4s;
		}

		.slider:before {
			position: absolute;
			content: "";
			height: 26px;
			width: 26px;
			left: 4px;
			bottom: 4px;
			background-color: white;
			-webkit-transition: .4s;
			transition: .4s;
		}

		input:checked + .slider {
			background-color: #2196F3;
		}

		input:focus + .slider {
			box-shadow: 0 0 1px #2196F3;
		}

		input:checked + .slider:before {
			-webkit-transform: translateX(26px);
			-ms-transform: translateX(26px);
			transform: translateX(26px);
		}

		/* Rounded sliders */
		.slider.round {
			border-radius: 34px;
		}

		.slider.round:before {
			border-radius: 50%;
		}

		.rejectLocksSwitch {
			height: 24px;
			display: block;
			position: relative;
			cursor: pointer;
		}

		.rejectLocksSwitch input {
			display: none;
		}

		.rejectLocksSwitch input + span {
			padding-left: 50px;
			min-height: 24px;
			line-height: 24px;
			display: block;
			color: #99A3BA;
			position: relative;
			vertical-align: middle;
			white-space: nowrap;
			transition: color .3s ease;
		}

		.rejectLocksSwitch input + span:before, .rejectLocksSwitch input + span:after {
			content: '';
			display: block;
			position: absolute;
			border-radius: 12px;
		}

		.rejectLocksSwitch input + span:before {
			top: 0;
			left: 0;
			width: 42px;
			height: 24px;
			background: #E4ECFA;
			transition: all .3s ease;
		}

		.rejectLocksSwitch input + span:after {
			width: 18px;
			height: 18px;
			background: #fff;
			top: 3px;
			left: 3px;
			box-shadow: 0 1px 3px rgba(18, 22, 33, 0.1);
			transition: all .45s ease;
		}

		.rejectLocksSwitch input + span em {
			width: 8px;
			height: 7px;
			background: #99A3BA;
			position: absolute;
			left: 8px;
			bottom: 7px;
			border-radius: 2px;
			display: block;
			z-index: 1;
			transition: all .45s ease;
		}

		.rejectLocksSwitch input + span em:before {
			content: '';
			width: 2px;
			height: 2px;
			border-radius: 1px;
			background: #fff;
			position: absolute;
			display: block;
			left: 50%;
			top: 50%;
			margin: -1px 0 0 -1px;
		}

		.rejectLocksSwitch input + span em:after {
			content: '';
			display: block;
			border-top-left-radius: 4px;
			border-top-right-radius: 4px;
			border: 1px solid #99A3BA;
			border-bottom: 0;
			width: 6px;
			height: 4px;
			left: 1px;
			bottom: 6px;
			position: absolute;
			z-index: 1;
			-webkit-transform-origin: 0 100%;
			transform-origin: 0 100%;
			transition: all .45s ease;
			-webkit-transform: rotate(-35deg) translate(0, 1px);
			transform: rotate(-35deg) translate(0, 1px);
		}

		.rejectLocksSwitch input + span strong {
			font-weight: normal;
			position: relative;
			display: block;
			top: 1px;
		}

		.rejectLocksSwitch input + span strong:before, .rejectLocksSwitch input + span strong:after {
			font-size: 14px;
			font-weight: 500;
			display: block;
			font-family: 'Mukta Malar', Arial;
			-webkit-backface-visibility: hidden;
		}

		.rejectLocksSwitch input + span strong:before {
			content: 'Reject Batch Print On';
			transition: all .3s ease .2s;
		}

		.rejectLocksSwitch input + span strong:after {
			content: 'Reject Batch Print Off';
			opacity: 0;
			visibility: hidden;
			position: absolute;
			left: 0;
			top: 0;
			color: #5628EE;
			transition: all .3s ease;
			-webkit-transform: translate(2px, 0);
			transform: translate(2px, 0);
		}

		.rejectLocksSwitch input:checked + span:before {
			background: rgba(86, 40, 238, 0.35);
		}

		.rejectLocksSwitch input:checked + span:after {
			background: #fff;
			-webkit-transform: translate(18px, 0);
			transform: translate(18px, 0);
		}

		.rejectLocksSwitch input:checked + span em {
			-webkit-transform: translate(18px, 0);
			transform: translate(18px, 0);
			background: #5628EE;
		}

		.rejectLocksSwitch input:checked + span em:after {
			border-color: #5628EE;
			-webkit-transform: rotate(0deg) translate(0, 0);
			transform: rotate(0deg) translate(0, 0);
		}

		.rejectLocksSwitch input:checked + span strong:before {
			opacity: 0;
			visibility: hidden;
			transition: all .3s ease;
			-webkit-transform: translate(-2px, 0);
			transform: translate(-2px, 0);
		}

		.rejectLocksSwitch input:checked + span strong:after {
			opacity: 1;
			visibility: visible;
			-webkit-transform: translate(0, 0);
			transform: translate(0, 0);
			transition: all .3s ease .2s;
		}
		/*---------------------------rejectLocksSwitch--------------------------------------*/
		/*----------------------------------------------------------------------------------*/
		.switch {
			position: relative;
			display: inline-block;
			width: 60px;
			height: 34px;
		}

		/* Hide default HTML checkbox */
		.switch input {
			opacity: 0;
			width: 0;
			height: 0;
		}

		/* The slider */
		.slider {
			position: absolute;
			cursor: pointer;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			background-color: #ccc;
			-webkit-transition: .4s;
			transition: .4s;
		}

		.slider:before {
			position: absolute;
			content: "";
			height: 26px;
			width: 26px;
			left: 4px;
			bottom: 4px;
			background-color: white;
			-webkit-transition: .4s;
			transition: .4s;
		}

		input:checked + .slider {
			background-color: #2196F3;
		}

		input:focus + .slider {
			box-shadow: 0 0 1px #2196F3;
		}

		input:checked + .slider:before {
			-webkit-transform: translateX(26px);
			-ms-transform: translateX(26px);
			transform: translateX(26px);
		}

		/* Rounded sliders */
		.slider.round {
			border-radius: 34px;
		}

		.slider.round:before {
			border-radius: 50%;
		}

		.switch {
			height: 24px;
			display: block;
			position: relative;
			cursor: pointer;
		}

		.switch input {
			display: none;
		}

		.switch input + span {
			padding-left: 50px;
			min-height: 24px;
			line-height: 24px;
			display: block;
			color: #99A3BA;
			position: relative;
			vertical-align: middle;
			white-space: nowrap;
			transition: color .3s ease;
		}

		.switch input + span:before, .switch input + span:after {
			content: '';
			display: block;
			position: absolute;
			border-radius: 12px;
		}

		.switch input + span:before {
			top: 0;
			left: 0;
			width: 42px;
			height: 24px;
			background: #E4ECFA;
			transition: all .3s ease;
		}

		.switch input + span:after {
			width: 18px;
			height: 18px;
			background: #fff;
			top: 3px;
			left: 3px;
			box-shadow: 0 1px 3px rgba(18, 22, 33, 0.1);
			transition: all .45s ease;
		}

		.switch input + span em {
			width: 8px;
			height: 7px;
			background: #99A3BA;
			position: absolute;
			left: 8px;
			bottom: 7px;
			border-radius: 2px;
			display: block;
			z-index: 1;
			transition: all .45s ease;
		}

		.switch input + span em:before {
			content: '';
			width: 2px;
			height: 2px;
			border-radius: 1px;
			background: #fff;
			position: absolute;
			display: block;
			left: 50%;
			top: 50%;
			margin: -1px 0 0 -1px;
		}

		.switch input + span em:after {
			content: '';
			display: block;
			border-top-left-radius: 4px;
			border-top-right-radius: 4px;
			border: 1px solid #99A3BA;
			border-bottom: 0;
			width: 6px;
			height: 4px;
			left: 1px;
			bottom: 6px;
			position: absolute;
			z-index: 1;
			-webkit-transform-origin: 0 100%;
			transform-origin: 0 100%;
			transition: all .45s ease;
			-webkit-transform: rotate(-35deg) translate(0, 1px);
			transform: rotate(-35deg) translate(0, 1px);
		}

		.switch input + span strong {
			font-weight: normal;
			position: relative;
			display: block;
			top: 1px;
		}

		.switch input + span strong:before, .switch input + span strong:after {
			font-size: 14px;
			font-weight: 500;
			display: block;
			font-family: 'Mukta Malar', Arial;
			-webkit-backface-visibility: hidden;
		}

		.switch input + span strong:before {
			content: 'Auto Print On';
			transition: all .3s ease .2s;
		}

		.switch input + span strong:after {
			content: 'Auto Print Off';
			opacity: 0;
			visibility: hidden;
			position: absolute;
			left: 0;
			top: 0;
			color: #5628EE;
			transition: all .3s ease;
			-webkit-transform: translate(2px, 0);
			transform: translate(2px, 0);
		}

		.switch input:checked + span:before {
			background: rgba(86, 40, 238, 0.35);
		}

		.switch input:checked + span:after {
			background: #fff;
			-webkit-transform: translate(18px, 0);
			transform: translate(18px, 0);
		}

		.switch input:checked + span em {
			-webkit-transform: translate(18px, 0);
			transform: translate(18px, 0);
			background: #5628EE;
		}

		.switch input:checked + span em:after {
			border-color: #5628EE;
			-webkit-transform: rotate(0deg) translate(0, 0);
			transform: rotate(0deg) translate(0, 0);
		}

		.switch input:checked + span strong:before {
			opacity: 0;
			visibility: hidden;
			transition: all .3s ease;
			-webkit-transform: translate(-2px, 0);
			transform: translate(-2px, 0);
		}

		.switch input:checked + span strong:after {
			opacity: 1;
			visibility: visible;
			-webkit-transform: translate(0, 0);
			transform: translate(0, 0);
			transition: all .3s ease .2s;
		}
		/*-------------------------------------------------------------------------------------------------*/
	</style>
	
</head>
<body>
	@include('includes.header_menu')
		<div class = "container" style="width:95%;">
			<ol class = "breadcrumb">
				<li><a href = "{{url('/')}}">Home</a></li>
				<li class = "active"><a href="{{ url('/graphics/print_sublimation') }}">Print Sublimation</a></li>
			</ol>
			@include('includes.error_div')
			@include('includes.success_div')
			
			<div class = "wrapper">
				<div class = "col-xs-12">

					<h4 style="display: none">Send Graphics to Printers
					<div class="pull-right">
						{!! Form::open(['url' => 'graphics/print_all', 'method' => 'POST', 'id' => 'print_all']) !!}
							<div class="form-group col-xs-10">
									@foreach ($batches as $batch)
										{!! Form::hidden('print_batches[]', $batch->batch_number) !!}
									@endforeach
									{!! Form::select('printer', $printers, null, 
														['id' => 'printer', 'class' => 'form-control', 'onclick' => "return false;", 'placeholder' => 'Select Printer' ]) !!}
							</div>
							<div class = "form-group col-xs-2">
										@setvar($msg = 'Are you sure you want to print these files?')
										{!! Form::submit('Print All' , ['class' => 'btn btn-sm btn-warning', 
																			'onclick' => 'return confirm("' . $msg .'")']) !!}
							</div>
						{!! Form::close() !!}
					</div>
					</h4>
			  </div>
				<div class = "col-xs-12">
					<div class="form-group col-xs-2">
					@if (!$locked)
						<label class="switch">
							<input type="checkbox" id="lock" onclick="locks()">
							<span>
            			<em></em>
            			<strong></strong>
        				</span>
						</label>
					@else
						<label class="switch">
							<input type="checkbox" id="lock" onclick="locks()" checked>
							<span>
            			<em></em>
            			<strong></strong>
        				</span>
						</label>
					@endif
					</div>

					<div class="form-group col-xs-2">
					@if (!$rejectLocks)
						<label class="rejectLocksSwitch">
							<input type="checkbox" id="rejectLocks" onclick="rejectLocks()">
							<span>
            			<em></em>
            			<strong></strong>
        				</span>
						</label>
					@else
						<label class="rejectLocksSwitch">
							<input type="checkbox" id="rejectLocks" onclick="rejectLocks()" checked>
							<span>
            			<em></em>
            			<strong></strong>
        				</span>
						</label>
					@endif
					</div>

					<div class = "form-group col-xs-4  col-md-2">
						<div class = 'input-group date' id = 'from_date_picker'>
							{!! Form::text('from_date_auto', $from_date_auto ?? '', ['id'=>'from_date_auto', 'class' => 'form-control', 'placeholder' => 'from', 'onclick' => 'return false;','autocomplete' => 'off']) !!}
							<span class = "input-group-addon"><span class = "glyphicon glyphicon-calendar"></span></span>
						</div>
					</div>

					<div class = "form-group col-xs-4  col-md-2">
						<div class = 'input-group date' id = 'to_date_picker'>
							{!! Form::text('to_date_auto', $to_date_auto ?? '', ['id'=>'to_date_auto', 'class' => 'form-control', 'placeholder' => 'To', 'onclick' => 'return false;','autocomplete' => 'off']) !!}
							<span class = "input-group-addon"><span class = "glyphicon glyphicon-calendar"></span></span>
						</div>
					</div>

				</div>
				<div class = "col-xs-12">
					<div class = "panel panel-default">
						<div class = "panel-body">
					    {!! Form::open(['method' => 'get', 'url' => 'graphics/print_sublimation', 'name' => 'filter']) !!}
							<div class = "form-group col-xs-6 col-md-2">
								{!! Form::text('select_batch', $select_batch, ['id'=>'select_batch', 'class' => 'form-control', 
																			'onchange' => 'return false;', 'placeholder' => 'Batch Number']) !!}
							</div>
							<div class = "form-group col-xs-3 col-md-1">
					      {!! Form::select('type', ['soft' => 'Soft', 'hard' => 'Hard', 'other' => 'Other'], isset($type) ? $type : '',  
																				['class' => 'form-control', 'placeholder' => 'Type', 'onclick' => 'return false;']) !!}
					    </div>
							<div class = "form-group col-xs-9 col-md-1">
					      {!! Form::select('production_station_id[]', $stations, isset($production_station_id) ? $production_station_id : '',  
																				['class' => 'form-control', 'id' => 'production_station_id', 'multiple' => 'multiple', 
																				'onclick' => 'return false;']) !!}
					    </div>
							<div class = "form-group col-xs-6 col-md-1">
								@if (count($stores) > 1)
					      	{!! Form::select('store_id[]', $stores, $store_id,  ['id' => 'store_id', 'multiple' => 'multiple', 'class' => 'form-control']) !!}
					    	@endif
							</div>
							<div class = "form-group col-xs-6 col-md-2">
								<div class = 'input-group date' id = 'from_date_picker'>
									{!! Form::text('from_date', $from_date ?? '', ['id'=>'from_datepicker', 'class' => 'form-control', 'placeholder' => 'from', 'onclick' => 'return false;','autocomplete' => 'off']) !!}
									<span class = "input-group-addon"><span class = "glyphicon glyphicon-calendar"></span></span>
								</div>
							</div>
							<div class = "form-group col-xs-6 col-md-2">
								<div class = 'input-group date' id = 'to_date_picker'>
									 {!! Form::text('to_date', $to_date ?? '', ['id'=>'to_datepicker', 'class' => 'form-control', 'placeholder' => 'To', 'onclick' => 'return false;']) !!}
								 <span class = "input-group-addon"><span class = "glyphicon glyphicon-calendar"></span></span>
								</div>
							</div>

							<div class = "form-group col-xs-6 col-md-1">
					      		{!! Form::submit('Show Summaries' , ['name' => 'summaries_btn','class' => 'btn btn-default']) !!}
					    	</div>

							<div class = "form-group col-xs-6 col-md-1">
								{!! Form::submit('Show Details' , ['name' => 'details_btn','class' => 'btn btn-default', 'style' => 'margin-left: 15px;']) !!}
							</div>
					  {!! Form::close() !!}
						</div>	
					</div>
				</div>
			    
			  <div id="content">
			      @if(isset ($batches) && count($batches) > 0)
							<h4>{{ count($batches) }} batches found</h4>
			        <table class="table">
			         <tbody>
			          @foreach($batches as $batch)
										@if ($batch->to_printer_date != NULL) 
											<tr bgcolor="#ffffe6">
										@else
											<tr>
										@endif
			              <td width="400">
												<a href = "{{ url(sprintf('batches/details/%s', $batch->batch_number)) }}" target="_blank">
																<strong>{{ $batch->batch_number }}</strong></a>
											@if (count($batch->items) > 1)
													({{ count($batch->items) }} Items)
											@endif
											<strong style="color: red;">{{ $stores[$batch->store_id] ?? null }}</strong>
			                
											<br>
											
			                @if (strpos(strtolower($batch->route->csv_extension), 'soft'))
			                  <br>
			                  <div class="button-box print-group" onclick="return false;">
			                    <div class="col-lg-4">
			                      {!! Form::open(['id' => 'move_form_' . $batch->batch_number . rand(10,99)]) !!}
			                      {!! Form::hidden('batch_number', $batch->batch_number) !!}
			                      {!! Form::hidden('printer', 'SOFT-1') !!}
			                      {!! Form::button('Send to SOFT-1' , ['id'=>'move_' . $batch->batch_number, 'class' => 'btn btn-xs btn-primary']) !!}
			                      {!! Form::close() !!}
			                    </div>
			                    <div class="col-lg-4">
			                      {!! Form::open(['id' => 'move_form_' . $batch->batch_number . rand(10,99)]) !!}
			                      {!! Form::hidden('batch_number', $batch->batch_number) !!}
			                      {!! Form::hidden('printer', 'SOFT-2') !!}
			                      {!! Form::button('Send to SOFT-2' , ['id'=>'move_' . $batch->batch_number, 'class' => 'btn btn-xs btn-info']) !!}
			                      {!! Form::close() !!}
			                    </div>
													<div class="col-lg-4">
			                      {!! Form::open(['id' => 'move_form_' . $batch->batch_number . rand(10,99)]) !!}
			                      {!! Form::hidden('batch_number', $batch->batch_number) !!}
			                      {!! Form::hidden('printer', 'SOFT-3') !!}
			                      {!! Form::button('Send to SOFT-3' , ['id'=>'move_' . $batch->batch_number, 'class' => 'btn btn-xs btn-primary']) !!}
			                      {!! Form::close() !!}
			                    </div>
			                  </div>
			                @elseif (strpos(strtolower($batch->route->csv_extension), 'hard'))
			                  <br>
			                  <div class="button-box  print-group" onclick="return false;">
			                    <div class="col-lg-4">
			                      {!! Form::open(['id' => 'move_form_' . $batch->batch_number . rand(10,99)]) !!}
			                      {!! Form::hidden('batch_number', $batch->batch_number) !!}
			                      {!! Form::hidden('printer', 'HARD-1') !!}
			                      {!! Form::button('Send to HARD-1' , ['id'=>'move_' . $batch->batch_number, 'class' => 'btn btn-xs btn-danger']) !!}
			                      {!! Form::close() !!}
			                    </div>
			                    <div class="col-lg-4">
			                      {!! Form::open(['id' => 'move_form_' . $batch->batch_number . rand(10,99)]) !!}
			                      {!! Form::hidden('batch_number', $batch->batch_number) !!}
			                      {!! Form::hidden('printer', 'HARD-2') !!}
			                      {!! Form::button('Send to HARD-2' , ['id'=>'move_' . $batch->batch_number, 'class' => 'btn btn-xs btn-success']) !!}
			                      {!! Form::close() !!}
			                    </div>
													<div class="col-lg-4">
			                      {!! Form::open(['id' => 'move_form_' . $batch->batch_number . rand(10,99)]) !!}
			                      {!! Form::hidden('batch_number', $batch->batch_number) !!}
			                      {!! Form::hidden('printer', 'HARD-3') !!}
			                      {!! Form::button('Send to HARD-3' , ['id'=>'move_' . $batch->batch_number, 'class' => 'btn btn-xs btn-danger']) !!}
			                      {!! Form::close() !!}
			                    </div>
			                  </div>
			                @else
								<div class="print-group" onclick="return false;">
				                  <div class = "col-xs-12">
				                    {!! Form::open(['id' => 'move_form_' . $batch->batch_number . rand(10,99)]) !!}
				                    {!! Form::hidden('batch_number', $batch->batch_number) !!}
				                    <div class = "form-group col-xs-10">
				                      {!! Form::select('printer', $printers, $batch->route->printer,
																			['id' => 'printer_select_' . $batch->batch_number, 'class' => 'form-control', 'onclick' => "return false;", 'placeholder' => 'Select Printer']) !!}
				                    <br>
										@if (isset($batchNumberWasatchDashboard[$batch->batch_number]))
											Batch# {!! $batch->batch_number !!} printing by Auto
										@endif
									</div>
				                    <div class = "form-group col-xs-2">
				                      {!! Form::button('Send' , ['id'=>'move_' . $batch->batch_number, 'class' => 'btn btn-sm btn-default', 'style' => 'margin-top: 3px;']) !!}
				                    </div>
				                  {!! Form::close() !!}
				                  </div>	
								</div>
			                @endif
			                
												<div class="print-message" style="display:none;" onclick="return false;">
													<div class = "col-xs-12">
														<div class = "form-group col-xs-10">
													 		<strong style="color: red;">Please Wait...</strong>
														</div>
												 </div>
												</div>
												
											
			              </td>
			              <td width="100">
			                @if ($batch->items->first()->child_sku)
			                  <span data-toggle = "tooltip" data-placement = "top" title = "{{ $batch->items->first()->child_sku }}">
			                    <a href = "{{ url(sprintf('batches/details/%s', $batch->batch_number)) }}" target="_blank">
													<img src = "{{ $batch->items->first()->item_thumb }}" width = "70" height = "70" /></a>
			                  </span>
			                @endif
			              </td>
			              <td width="400">
											@if($batch->type  == 'P')
												<strong style="color: red;">IN PRODUCTION:</strong> 
												<br>
											@elseif($batch->type == 'Q')
												<strong style="color: red;">IN QC:</strong>
												<br>
											@endif
											@if ($batch->production_station)
												Give to: {{ $batch->production_station->station_description }}
												<br>
											@else
												PRODUCTION STATION NOT FOUND: {{ $batch->production_station_id }}
												<br>
											@endif
											@if ($batch->status != 'active')
			                  Batch Status: <strong style="color: red;">{{ $batch->status }}</strong>
											@endif
											<br>
											First Order Date: {{ substr($batch->min_order_date, 0, 10) }}
				             </td>
										 <td width="200">
											 <table cellpadding="5">
												 <tr>
													 <td align="right">Scale:</td>
													 <td>
														 {!! Form::number('scale', $batch->items->first()->parameter_option->scale, ['id' => 'scale_' . $batch->batch_number, 'onclick' => 'return false;', 'readonly']) !!} %
														 <br>

														 <a href = "{{ url(sprintf('logistics/sku_list#%s', $batch->items->first()->parameter_option->unique_row_value)) }}" target="_blank">
															 <strong>{{ $batch->items->first()->parameter_option->graphic_sku  }}</strong></a>

													 </td>
												 </tr>
												 <!-- <tr>
													 <td align="right">Min Size:</td>
													 <td>{!! Form::number('minsize', null, ['id' => 'minsize_' . $batch->batch_number, 'onclick' => 'return false;']) !!} "</td>
												 </tr> -->
												 <!-- <tr>
													 <td align="right">Mirror:</td>
													 <td>{!! Form::checkbox('mirror', 1, 0, ['id' => 'mirror_' . $batch->batch_number]) !!}</td>
												 </tr> -->
											 </table>
										 </td>
				            </tr>
			          @endforeach
			        </table>
			      @elseif (isset($summary) and count($summary) > 0)
			        <table class="table">
								<tr>
									<th>Production Station</th>
									<th>First order</th>
									<th width=200 style="text-align:right;">Batches</th>
								</tr>
								
								@foreach ($summary as $row)
									<tr>
										<td>{{ $row->production_station->station_description }}</td>
										<td>{{ substr( $row->date, 0, 10 ) }}</td>
										<td align="right">
											<a href="{{ url(sprintf('/batches/list?status=movable&graphic_found=1&section=1&station=6&production_station=%s', $row->production_station_id)) }}"
												target="_blank">{{ $row->count }}</a>
										</td>
									</tr>
								@endforeach
								
								<tr> 
									<th></th> 
									<th style="text-align:right;">Total:</th> 
									<th width=200 style="text-align:right;"> 
										{{ $summary->sum('count') }} 
									</th> 
								</tr> 
							</table>
						@else
							<div class = "alert alert-warning">Nothing Found</div>
			      @endif
			  </div>
			  
			  <div id="sidebar2">
					@if (!is_array($queues))
						{{ $queues }}
					@else
						@foreach($queues as $unit => $queue)
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" href="#collapse{!! substr($unit, -1) !!}">
											 {!! str_replace('_', ' ', $unit) !!} ({{ $queue['TOTAL'] }})
										</a>
										<br><br>
										{!! Form::select('config_' . substr($unit, -1), $stations, isset($config[substr($unit, -1)]) ? $config[substr($unit, -1)] : null, 
											 						['class' => 'select_config form-control', 'id' => substr($unit, -1), 'placeholder' => 'Manual Print']) !!}
									</h4>
								</div>
								<div id="collapse{!! substr($unit, -1) !!}" class="panel-collapse collapse">
									<div class="panel-body">
										<strong>STAGED XML</strong>
										<br>
										@if (count($queue['STAGED_XML']) > 0)
													@foreach ($queue['STAGED_XML'] as $file)
														{{ $file }} <br>
													@endforeach
										@else
											Nothing<br>
										@endif
										<br>
										<strong>HOT FOLDER</strong>
										<br>
										@if (count($queue['HOT_FOLDER']) > 0)
													@foreach ($queue['HOT_FOLDER'] as $file)
														{{ $file }} <br>
													@endforeach
										@else
											Nothing<br>
										@endif
										<br>
										<strong>RIP QUEUE</strong>
										<br>
										@if (count($queue['RIP_QUEUE']) > 0)
													@foreach ($queue['RIP_QUEUE'] as $file)
														{{ $file }} <br>
													@endforeach
										@else
											Nothing<br>
										@endif
										<br>
										<strong>PRINT QUEUE</strong>
										<br>
										@if (count($queue['PRINT_QUEUE']) > 0)
													@foreach ($queue['PRINT_QUEUE'] as $file)
														{{ $file }} <br>
													@endforeach
										@else
											Nothing
										@endif
									</div>
								</div>
							</div>
						@endforeach
					@endif
			  </div>
			  
			  <div id="cleared"></div>
			  
			</div>
		</div>

			<script type='text/javascript'>
				var checkBox = document.getElementById("lock");
				
				$('#store_id').multiselect({
																			includeSelectAllOption:true,
																			nonSelectedText:'Store',
																			numberDisplayed: 1,
																		});
				$('#production_station_id').multiselect({
																			includeSelectAllOption:true,
																			nonSelectedText:'Station',
																			numberDisplayed: 1,
																		});


				
				$(document).ready(function(){
					setTimeout( function() { 
						$(".print-group").hide();
					}  , 120000 );



					$("#from_date_auto").focusout(function(){
						var from_date_auto = new Date($("#from_date_auto").val());
						var to_date_auto = new Date($("#to_date_auto").val());
						if(from_date_auto > to_date_auto){
							alert("Sorry From Date should be less then To Date");
							$(this).val("");
							$(this).focus();
							return false;
						}

						$.ajax({
							type: 'get',
							url: '{{ url("graphics/lock") }}',
							cache: false,
							data: {
								"flag": "from_date_auto",
								"fileNameWithOutPath": $(this).val(),
								"fromDateAutoVal": $(this).val(),
								"randomVal": Math.random(),
							},
							context: this,
							success: function (response) {

							},
							failure: function (response) {

							}
						});
					});

					$("#to_date_auto").focusout(function(){
						var from_date_auto = new Date($("#from_date_auto").val());
						var to_date_auto = new Date($("#to_date_auto").val());
						if(from_date_auto > to_date_auto){
							alert("Sorry To Date should be bigger then Form Date");
							$(this).val("");
							$(this).focus();
							return false;
						}

						// $(this).css("background-color", "#ffd51f");

						$.ajax({
							type: 'get',
							url: '{{ url("graphics/lock") }}',
							cache: false,
							data: {
								"flag": "to_date_auto",
								"fileNameWithOutPath": $(this).val(),
								"toDateAutoVal": $(this).val(),
								"randomVal": Math.random(),
							},
							context: this,
							success: function (response) {

							},
							failure: function (response) {

							}
						});
					});
				});
				
				var picker = new Pikaday(
				{
						field: document.getElementById('to_datepicker'),
						format : "YYYY-MM-DD",
						minDate: new Date('2016-06-01'),    
				});

				var picker = new Pikaday(
				{
					field: document.getElementById('from_datepicker'),
					format : "YYYY-MM-DD",
					minDate: new Date('2016-06-01'),
				});


				var picker = new Pikaday(
						{
							field: document.getElementById('from_date_auto'),
							format : "YYYY-MM-DD",
							minDate: new Date('2016-06-01'),
						});

				var picker = new Pikaday(
						{
							field: document.getElementById('to_date_auto'),
							format : "YYYY-MM-DD",
							minDate: new Date('2016-06-01'),
						});

	      $(function () {

					$('button[id^="move_"]').on('click', function (e) {
						
						form_name = "#" + $(this).closest('form').attr('id');
	          e.preventDefault();
						
						if ($( form_name ).parent().parent().attr('class').indexOf('print-group') !== -1)  {
							$( form_name ).parent().parent().hide();
							
							$( form_name ).closest("tr").find('.print-message').show();
						}
						
						var batch_number = $( form_name ).find('input[name="batch_number"]').val(); 
						var scale = $("#scale_" + batch_number).val();
						// var minsize = $("#minsize_" + batch_number).val();
						// var mirror = $("#mirror_" + batch_number).val();
						
						$( form_name ).append("<input type='hidden' name='scale' value='"+ scale +"'>");
						// $( form_name ).append("<input type='hidden' name='minsize' value='"+ minsize +"'>");
						// $( form_name ).append("<input type='hidden' name='mirror' value='"+ mirror +"'>");
						
	          $.ajax({
	            type: 'post',
	            url: '{{ url("graphics/move_to_print") }}',
	            data: $( form_name ).serialize(),
							context: this,
	            success: function (response) {
								$( this ).closest("tr").find('.print-message').html(response);
	            },
							failure: function (response) {
								$( this ).closest("tr").find('.print-message').html('Failure');
	            }
	          });

	        });
					
					$('select[name^="config_"]').on('change', function (e) {
						
						e.preventDefault();
						var number = $(this).attr('id');
						var station = $(this).val();
						
						$.ajax({
							type: 'get',
							url: '{{ url("graphics/printer_config") }}',
							data: 'number=' + number + '&station=' + station,
							success: function (response) {
								if (response != 'success') {
									alert(response);
								}
							},
							failure: function (response) {
								alert(response);
							}
						});

					});
					
	      });

				function locks() {
					// console.log($("#lock").is(':checked') );
					// debugger;
					if ($("#lock").prop("checked")  == true) {
						$.ajax({
							type: 'get',
							url: '{{ route('jobFormSubmit') }}',
							cache: false,
							data: {
								"flag": "1",
								"fileNameWithOutPath": "autoPrintHard",
								"randomVal": Math.random(),
							},
							context: this,
							success: function (response) {

							},
							failure: function (response) {

							}
						});

					} else {
						$.ajax({
							type: 'get',
							url: '{{ url("graphics/lock") }}',
							cache: false,
							data: {
								"flag": "0",
								"fileNameWithOutPath": "autoPrintHard",
								"randomVal": Math.random(),
							},
							context: this,
							success: function (response) {

							},
							failure: function (response) {

							}
						});
					}
				}


				function rejectLocks() {
					// console.log($("#rejectLocks").is(':checked') );
					// debugger;
					if ($("#rejectLocks").prop("checked")  == true) {
							$.ajax({
								type: 'get',
								url: '{{ route('jobFormSubmit') }}',
								cache: false,
								data: {
									"flag": "1",
									"fileNameWithOutPath": "rejectLocks",
									"randomVal": Math.random(),
								},
								context: this,
								success: function (response) {

								},
								failure: function (response) {

								}
							});

						} else {
							$.ajax({
								type: 'get',
								url: '{{ url("graphics/lock") }}',
								cache: false,
								data: {
									"flag": "0",
									"fileNameWithOutPath": "rejectLocks",
									"randomVal": Math.random(),
								},
								context: this,
								success: function (response) {

								},
								failure: function (response) {

								}
							});
						}
					}
	    </script>
			
</body>
</html>